
local api = {} 

-- API обертка для вызовов из usr.lua
utility = require("lua/utility")
get = require("lua/get")
config = require("lua/config")
-------------------------------------------------------------------------------
-- Global variables for this file --
local integr_value = 0
local curr_time = os.time()
local timer_t = {}
local internet_flag = false
local t_internet_end = 21474836400 -- Sat, 06 Jul 2650 08:20:00 GMT
local pulse_relay_table = {}
local api_unit_test_flag = false
local comparator_t = {}
local antilegionella_session_time_end = 0

-------------------------------------------------------------------------------
local function send_sms(level, type, message) 
--level=0 информационный уровень, событие не является аварийным
--level=1 авария, событие отображается сервером как аварийное
--type -> not used yet, TBD
  local phone_t = {}
  local phone_string
  -- if 3G modem enabled, and phones are stored then let send copy via SMS
  if config.isPatchTrheadOn == true and config.enable_3g == true then
  	if level == 0 then 
  		phone_string = utility.param.phones.s49 -- information
  	else
  		phone_string = utility.param.phones.s9 -- alarm
  	end
  	if utility.param.phones ~= nil and phone_string ~= nil then 
  		-- extract phone list from s9 to table phone_t
  		-- http://stackoverflow.com/questions/19262761/lua-need-to-split-at-comma
        for phone in string.gmatch(phone_string, '([^,]+)') do 
        table.insert(phone_t, phone) 
		end	
  	end
  end
  
  message = message:sub(1,140)
  -- sudo gammu sendsms TEXT +79108771111  -unicode -text "Halooo ..русский"
  local com, res
  for k, phone in pairs(phone_t)do
    -- The Issue: LIP Library may remove "+" at the first char of phone number.
    -- Let check and if phone has not "+" let add it simply
    if string.find(phone, "+") == nil then phone = "+"..phone end
    -- Note: option "text" works well from Bash but failed on russian here. Let use "textutf8" only.
    com = 'sudo gammu sendsms TEXT '..phone..' -unicode -textutf8 '..'\"'..message..'\" &'
    res = os.execute(com)
  end  
end
-------------------------------------------------------------------------------
local function invert_boiler_status(boiler_status, inversion)
  if inversion then
    return 1-boiler_status
  else
    return boiler_status
  end
end
-------------------------------------------------------------------------------
function send_byte_tx1(byte)
  call_c_from_lua_uart1_byte_tx(byte)
end
-------------------------------------------------------------------------------
function send_byte_tx2(byte)
  call_c_from_lua_uart2_byte_tx(byte)
end
-------------------------------------------------------------------------------
function get_digit_input(index)
-- на входе аргумент "index", он должен быть от 1 до 3 соответственно номеру входа.
-- на выходе "1", если на входе высокий уровень и "0" если на входе низкий уровень.
  assert(((index<=3) and (index>=1)), "****** get_digit_input() WRONG argument. Must be <=3 and >=1: "..tostring(index))
  local digit = call_c_from_lua_get_digit_inp() -- NOTE: C Code has INVERSION !!! 
  
  local inversion_t = {7,6,5,4,3,2,1,0}
  local digit_i = inversion_t[digit+1]
  th_tab.digit_input = digit_i
  
  -- Case: Input 220VAC, therefore counter = 50. It means that ret=1
  -- 50Hz frequncy may vary, let froom 48Hz to 52Hz, it's enough.
  local freq = get_counter(index)
  local ret = 0
  if freq >= 48 and freq <= 52 then 
  	ret = 1 
  else
  	ret = bit32.extract(digit_i, index-1)
  end
  return ret
end
-------------------------------------------------------------------------------
function get_counter(index)
-- на входе аргумент "index", он должен быть от 1 до 3 соответственно номеру входа.
-- на выходе число импульсов за секунду, в диапазоне от 0 до 255. Если число > 255, то 
--  значение не корректно. На самом деле, получится остаток от деления на 256.
  assert(((index<=3) and (index>=1)), "****** get_counter() WRONG argument. Must be <=3 and >=1: "..tostring(index))
  
  local counter = call_c_from_lua_get_inp_counter(index)
  return counter
end
-------------------------------------------------------------------------------
function get_analog_input(index)
-- на входе аргумент "index", он должен быть от 1 до 3 соответственно номеру аналогового входа.
-- на выходе значение напряжения в милливольтах.
  local adc_data = call_c_from_lua_get_analog_inp(index) -- return 0...4095
  -- let apply zero shifting from config
  -- analog = analog - config.adc_shift
  --if analog < 0 then analog = 0 end
  
  -- Vref=3.3V, Inputd R/R divider = 1/11. Therefore Inp_range=3.3*11=36.3V
  -- Let ADC=4095, then 4095*K=36.3V, -> K=36300/4095=8.864=3300*11/4095
  local analog = math.floor (adc_data * config.adc_vref * 11 / 4095 )
  th_tab.analog_input[index] = analog
  return analog
end
-------------------------------------------------------------------------------
function get_relay(index)
-- на входе аргумент "index", он должен быть от 1 до 6 соответственно номеру реле.
-- на выходе "1", если реле включено и "0" если реле выключено
-- пока не учитывает импульсный режим
-- b0-relay1; b1-relay2 and so on
  assert(((index<=6) and (index>=1)), "****** get_relay() WRONG argument. Must be <=6 and >=1: "..tostring(index))
  local ret = th_tab.relay_status[index]
--  return bit32.extract (ret, index-1)
  return ret
end
-------------------------------------------------------------------------------
function set_relay(index, status)
-- на входе первый аргумент "index", он должен быть от 1 до 6 соответственно номеру реле.
-- второй аргумент "status", он должен принимать значения 0 или 1. 0-реле выключено, 1-реле включено
    assert(((index<=6) and (index>=1)), "****** set_relay() WRONG argument. Must be <=6 and >=1: "..tostring(index))
    assert(((status==0) or (status==1)), "****** set_relay() WRONG argument. Must be 0 or 1: "..tostring(status))
    th_tab.relay_status[index] = status
    local relay_byte = 0
    for ind, relay_stat in ipairs(th_tab.relay_status)do
      if relay_stat == 1 then 
        relay_byte = bit32.replace(relay_byte, 1, ind-1)
      else
        relay_byte = bit32.replace(relay_byte, 0, ind-1)
      end
    end    
  if api_unit_test_flag == false then
    call_c_from_lua_set_relay(relay_byte)
  end
  return relay_byte
end

-------------------------------------------------------------------------------
function ini_pulse_relays(pulse_relay_table)
  for i=1, 4 do -- pass through 4 relays (1/2/3/4) 
    pulse_relay_table[i] = {}
    pulse_relay_table[i][1] = false
    pulse_relay_table[i][2] = 0
  end
  return pulse_relay_table
end
-------------------------------------------------------------------------------
function set_pulse_relays(ind, width, profile, table)
  local relay = set_relay(ind, 0)
  local relay_par = 0
  --[[первый аргумент - номер реле (1...6)
  второй аргумент - номер длительность импульса (0,t1,t2,t3) - число 0/1/2/3
  третий аргумент - номер профайла (0/1/2/3), смотри таблицу ниже:
   {1, 2, 8, 30}, /*profile 0 - pause_before=1, t1=2s, t2=8s, t3=30s */
   {1, 1, 4, 16}, /*profile 1 - pause_before=1, t1=1s, t2=4s, t3=16s */
   {1, 2, 6, 20}, /*profile 2 - pause_before=1, t1=2s, t2=6s, t3=20s */
   {1, 1, 3, 12}  /*profile 3 - pause_before=1, t1=1s, t2=3s, t3=12s */   
  четвертый аргумент - таблица, в которой задаются номера реле в импульсном режиме и задается длительность импульса
  Формат таблицы relay_pulse_table = {<relay-1>,<relay-2>,<relay-3>,<relay-4>}
     где <relay-N> - параметры - длительность импульса и режим для реле с номером N (1/2/3/4)
     Формат поля длительности <relay-N>: таблица {Mode, Len}
     Флаг Mode - режим. Если Mode=true - это импульсный режим, Mode=false - обычный режим;
     Mode Len
     [1]  [2]
     0    0 - обычный режим
     0    1 - обычный режим
     0    2 - обычный режим
     0    3 - обычный режим
     1    0 - нет импульса
     1    1 - t1
     1    2 - t2
     1    3 - t3
   * pulse length:
   * len=00: relay_b=0 relay_par_b<pulse length>=0 => off
   * len=01: relay_b=1 relay_par_b<pulse length>=0 => t1
   * len=10: relay_b=0 relay_par_b<pulse length>=1 => t2
   * len=11: relay_b=1 relay_par_b<pulse length>=1 => t3      ]]--
  table[ind][1] = true -- set PULSE MODE
  table[ind][2] = width -- set WIDTH
  -- bit32.replace (n, v, field [, width]) 
  -- Returns a copy of n with the bits field to field + width - 1 replaced by the value v. 
  relay = bit32.replace(relay, profile, 6, 2) 
  for i , data in ipairs(table) do
    if (data[1] == true) then -- pulse mode TRUE
      relay_par = bit32.replace(relay_par, 1, i-1)
      if width==0 then
        relay_par = bit32.replace(relay_par, 0, i-1+4)
        relay = bit32.replace(relay, 0, i-1)
      elseif  width == 1 then
        relay_par = bit32.replace(relay_par, 0, i-1+4)
        relay = bit32.replace(relay, 1, i-1)
      elseif width == 2 then
        relay_par = bit32.replace(relay_par, 1, i-1+4)
        relay = bit32.replace(relay, 0, i-1)
      elseif width == 3 then
        relay_par = bit32.replace(relay_par, 1, i-1+4)
        relay = bit32.replace(relay, 1, i-1)
      end
    end
  end   
  if api_unit_test_flag == true then
--    print ("relay")
--    toBits(relay, 8)
--    print ("relay_par")
--    toBits(relay_par, 8)
  else
    call_c_from_lua_set_pulse_relay(relay, relay_par)
  end
end
-------------------------------------------------------------------------------
function get_analog_output(index)
-- на входе аргумент "index", он должен быть от 1 до 6 соответственно номеру выхода.
-- на выходе значение выходного напряжения в милливольтах. Оно должно принимать значения от 0 до 10000. 
--print(debug.traceback())
  assert(((index<=6) and (index>=1)), "****** get_analog_output() WRONG argument. Must be <=6 and >=1: "..tostring(index))
  return th_tab.analog_output[index]
end
-------------------------------------------------------------------------------
function set_analog_output(index, millivolts)
-- на входе первый аргумент "index", он должен быть от 1 до 6 соответственно номеру выхода.
-- второй аргумент "millivolts", он должен принимать значения от 0 до 10000. 
-- Если значение вне этого диапазона, то оно приводится к крайнему значение 0 или 10000 
--print(debug.traceback())
    assert(((index<=6) and (index>=1)), "****** set_analog_output() WRONG argument. Must be <=6 and >=1: "..tostring(index))
    if(millivolts < 0)then millivolts = 0 end
    if(millivolts > 10000)then millivolts = 10000 end
    th_tab.analog_output[index] = millivolts
    call_c_from_lua_set_analog_output(index, millivolts)
end
-------------------------------------------------------------------------------
function relay_on_off(state_on_off, relay_ind, analogAsRelay, inversion)
    -- включить, если state_on_off = 1
    -- выключить, если state_on_off = 0 
--    print('relay_on_off', state_on_off, relay_ind, analogAsRelay, inversion)
    if(inversion == false) then
        if(analogAsRelay == false) then
            set_relay(relay_ind, state_on_off)
        else
            set_analog_output(relay_ind-6, 10000*state_on_off)
        end
    else
        if(analogAsRelay == false) then
            set_relay(relay_ind, 1 - state_on_off)
        else
            set_analog_output(relay_ind-6, 10000 * (1-state_on_off) )
        end
    end
end              
-------------------------------------------------------------------------------
function analog_or_relay_comparator(tset_c, tsens_main_c, tsens_second_c, thyst_c, relay_index, inversion, analogasrelay)
-- то же, что функция relay_comparator() но для аналового выхода как реле. Код скопирован с изменениями
-- первый аргумент tset_c - заданная температура в градусах Цельсия;
-- втором аргумент tsens_c - фактическая температура в градусах Цельсия;
-- третий аргумент tsens_second_c - фактическая температура в градусах Цельсия у резервного датчика;
-- четвертный аргумент thyst_c - величина гистерезиса в градусах Цельсия;
-- пятый аргумент inversion - может принимать два значения, true или false
-- шестой аргумент analogasrelay - true/false если true - аналоговый выход используется как релейный
-- Результат работы - включение/выключение реле с номером relay_index или подача 0/10В на аналоговый выход
-------------------------------------------------------------------------------
  
  local boiler_status_on = 0
  if analogasrelay == true then
--    relay_index = relay_index - 6
    if get_analog_output(relay_index-6) >= 10000 then
    
      boiler_status_on = 1
    else 
      boiler_status_on = 0
    end
  else -- relay mode
    boiler_status_on = get_relay(relay_index)
  end
  invert_boiler_status(boiler_status_on, inversion)
  
  if analogasrelay == true then
    assert(((relay_index<=12) and (relay_index>=7)), "****** set_relay() WRONG argument <relay_inidex(AnalogAsRelay)>. Must be <=6 and >=1: "..tostring(relay_index))
  else
    assert(((relay_index<=6) and (relay_index>=1)), "****** set_relay() WRONG argument <relay_inidex>. Must be <=6 and >=1: "..tostring(relay_index))
  end
  assert(((inversion == true) or (inversion == false)), "****** set_relay() WRONG argument <inversion>. Must be true or false: "..tostring(inversion))
  local tsens_c
  
  if timer_t["RelayComp898"] == nil then
    print("\t\t Timer RelayComp898 - new ")
    timer_t["RelayComp898"] = {}
    timer_t["RelayComp898"].flag = 0
    timer_t["RelayComp898"].time = 0
  end
  
  local period_seconds = 600
  
  if tsens_main_c ~= nil then
      tsens_c = tsens_main_c
      timer_t["RelayComp898"].time = 0 -- need for change mode from Fault Mode to normal mode
  elseif tsens_second_c ~= nil then
      tsens_c = tsens_second_c
      timer_t["RelayComp898"].time = 0
  else
  
    boiler_status_on = 1 -- fault mode means boiler status ON because cicrular pump must turn on too.
  
    -- Warning - Fault Mode, let turn on/off relay with 10 minutes period
    local time = os.time() -- current time   
 --   print("FAULT MODE", time, timer_t["RelayComp898"].flag, timer_t["RelayComp898"].time)
    if (time > timer_t["RelayComp898"].time) then
      if timer_t["RelayComp898"].flag == 0 then
        timer_t["RelayComp898"].flag = 1
        timer_t["RelayComp898"].time = time + period_seconds
        set_relay(relay_index, 1)
      else
        timer_t["RelayComp898"].flag = 0
        timer_t["RelayComp898"].time = time + period_seconds
        set_relay(relay_index, 0)
      end
    end
    return boiler_status_on -- both sensor failed, so let give up
  end
  if(tsens_c > (tset_c + thyst_c))then
    boiler_status_on = 0
  end
  if(tsens_c < (tset_c - thyst_c))then
    boiler_status_on = 1 
  end

  if analogasrelay == true then
    if(inversion == false)then
      if boiler_status_on == 1 then set_analog_output(relay_index-6, 10000)
      else set_analog_output(relay_index-6, 0) end
    else -- inversion!
      if(boiler_status_on == 0)then set_analog_output(relay_index-6, 10000)
      else set_analog_output(relay_index-6, 0) end
    end  
  else -- relay mode
    if(inversion == false)then
      set_relay(relay_index, boiler_status_on)
    else
      if(boiler_status_on == 0)then set_relay(relay_index, 1)
      else set_relay(relay_index, 0) end
    end  
  end
  return boiler_status_on
end
-------------------------------------------------------------------------------
function analog_or_relay_comparator_with_delay(tset_c, tsens_main_c, tsens_second_c, thyst_c, relay_index, inversion, analogasrelay, u_id, delay)
-- то же, что функция relay_comparator() но для аналового выхода как реле. Код скопирован с изменениями
-- первый аргумент tset_c - заданная температура в градусах Цельсия;
-- втором аргумент tsens_c - фактическая температура в градусах Цельсия;
-- третий аргумент tsens_second_c - фактическая температура в градусах Цельсия у резервного датчика;
-- четвертный аргумент thyst_c - величина гистерезиса в градусах Цельсия;
-- пятый аргумент inversion - может принимать два значения, true или false
-- шестой аргумент analogasrelay - true/false если true - аналоговый выход используется как релейный
-- седьмой аргумент - u_id - уникальный индекс (не должен повторяться!!)
-- восьмой аргумен - delay - задержка ("выбег") после выключения
-- Результат работы - включение/выключение реле с номером relay_index или подача 0/10В на аналоговый выход
-------------------------------------------------------------------------------
  local boiler_stat
  local boiler_status_on = 0

  if analogasrelay == true then
--    relay_index = relay_index - 6
    if get_analog_output(relay_index-6) >= 10000 then
      boiler_status_on = 1
    else 
      boiler_status_on = 0
    end
  else -- relay mode
    boiler_status_on = get_relay(relay_index)
  end
  boiler_status_on = invert_boiler_status(boiler_status_on, inversion)

  if analogasrelay == true then
    assert(((relay_index<=12) and (relay_index>=7)), "****** set_relay() WRONG argument <relay_inidex(AnalogAsRelay)>. Must be <=6 and >=1: "..tostring(relay_index))
  else
    assert(((relay_index<=6) and (relay_index>=1)), "****** set_relay() WRONG argument <relay_inidex>. Must be <=6 and >=1: "..tostring(relay_index))
  end
  assert(((inversion == true) or (inversion == false)), "****** set_relay() WRONG argument <inversion>. Must be true or false: "..tostring(inversion))
  local tsens_c
  
  if timer_t["RelayComp898"] == nil then
    print("\t\t Timer RelayComp898 - new ")
    timer_t["RelayComp898"] = {}
    timer_t["RelayComp898"].flag = 0
    timer_t["RelayComp898"].time = 0
  end
  
  local period_seconds = 600
  
  if tsens_main_c ~= nil then
      tsens_c = tsens_main_c
      timer_t["RelayComp898"].time = 0 -- need for change mode from Fault Mode to normal mode
  elseif tsens_second_c ~= nil then
      tsens_c = tsens_second_c
      timer_t["RelayComp898"].time = 0
  else
  
    boiler_status_on = 1 -- fault mode means boiler status ON because cicrular pump must turn on too.
  
    -- Warning - Fault Mode, let turn on/off relay with 10 minutes period
    local time = os.time() -- current time   
    if (time > timer_t["RelayComp898"].time) then
      if timer_t["RelayComp898"].flag == 0 then
        timer_t["RelayComp898"].flag = 1
        timer_t["RelayComp898"].time = time + period_seconds
        relay_on_off(1, relay_index, analogasrelay, inversion) --  
      else
        timer_t["RelayComp898"].flag = 0
        timer_t["RelayComp898"].time = time + period_seconds
        relay_on_off(0, relay_index, analogasrelay, inversion) --  
      end
    end
    return boiler_status_on -- both sensor failed, so let give up
  end

  if(tsens_c > (tset_c + thyst_c))then
    boiler_status_on = 0
  end
  if(tsens_c < (tset_c - thyst_c))then
    boiler_status_on = 1 
  end
--------------
-- Turn OFF DELAY
  if delay > 0 then
      boiler_stat = timer_toff("TurnOFF_DelaySource_"..tostring(u_id), boiler_status_on, delay)
      relay_on_off(boiler_stat, relay_index, analogasrelay, inversion) 
  else
      relay_on_off(boiler_status_on, relay_index, analogasrelay, inversion) 
  end -- 
--------------
  return boiler_status_on
end
-------------------------------------------------------------------------------
function relay_comparator(tset_c, tsens_main_c, tsens_second_c, thyst_c, relay_index, inversion)
-- первый аргумент tset_c - заданная температура в градусах Цельсия;
-- втором аргумент tsens_c - фактическая температура в градусах Цельсия;
-- третий аргумент tsens_second_c - фактическая температура в градусах Цельсия у резервного датчика;
-- четвертный аргумент thyst_c - величина гистерезиса в градусах Цельсия;
-- пятый аргумент inversion - может принимать два значения, true или false
-- Результат работы - включение/выключение реле с номером relay_index. 
-------------------------------------------------------------------------------
  local boiler_status_on = get_relay(relay_index)
  invert_boiler_status(boiler_status_on, inversion)
  assert(((relay_index<=6) and (relay_index>=1)), "****** set_relay() WRONG argument <relay_inidex>. Must be <=6 and >=1: "..tostring(relay_index))
  assert(((inversion == true) or (inversion == false)), "****** set_relay() WRONG argument <inversion>. Must be true or false: "..tostring(inversion))
  local tsens_c
  
  if timer_t["RelayComp898"] == nil then
    print("\t\t Timer RelayComp898 - new ")
    timer_t["RelayComp898"] = {}
    timer_t["RelayComp898"].flag = 0
    timer_t["RelayComp898"].time = 0
  end
  
  local period_seconds = 600
  
  if tsens_main_c ~= nil then
      tsens_c = tsens_main_c
      timer_t["RelayComp898"].time = 0 -- need for change mode from Fault Mode to normal mode
  elseif tsens_second_c ~= nil then
      tsens_c = tsens_second_c
      timer_t["RelayComp898"].time = 0
  else
    -- Warning - Fault Mode, let turn on/off relay with 10 minutes period
    local time = os.time() -- current time   
 --   print("FAULT MODE", time, timer_t["RelayComp898"].flag, timer_t["RelayComp898"].time)
    if (time > timer_t["RelayComp898"].time) then
      if timer_t["RelayComp898"].flag == 0 then
        timer_t["RelayComp898"].flag = 1
        timer_t["RelayComp898"].time = time + period_seconds
        set_relay(relay_index, 1)
      else
        timer_t["RelayComp898"].flag = 0
        timer_t["RelayComp898"].time = time + period_seconds
        set_relay(relay_index, 0)
      end
    end
    return -- both sensor failed, so let give up
  end
  if(tsens_c > (tset_c + thyst_c))then
    boiler_status_on = 0
  end
  if(tsens_c < (tset_c - thyst_c))then
    boiler_status_on = 1 
  end

  if(inversion == false)then
    set_relay(relay_index, boiler_status_on)
  else
    if(boiler_status_on == 0)then set_relay(relay_index, 1)
    else set_relay(relay_index, 0) end
  end  
end
-------------------------------------------------------------------------------
function send_event(level, type, message)
--[[ Событие, которое отображает сервер
level=0 информационный уровень, событие не является аварийным
level=1 авария, событие отображается сервером как аварийное
type - тип события, содержание пока не определено. Диапазон 0..255.
Внимание, при нечетном значении поля type сервер применяет кодировку CP1251, иначе - кодировку UTF-8, для латинского текста - ASCII
  message - текстовая строка, длиной не более 200 байт. Так как русские буквы чаще всего кодируются
  двумя байтами, то длина строки не должна превышать 100 символов. Для латинских символов допустимая
  длина 200. Строка длинее 200 байт обрезается.
--]]

  message = message:sub(1,200)
  call_c_from_lua_event(level, type, message)
  send_sms(level, type, message) 
end
-------------------------------------------------------------------------------
function send_fault_event(id, sensor_num, msg_string)
--[[ отправка события на сервер 
id:
EVENT_ID_MAIN_POWER_FAULT   10 - пропадание питания (sensor_num и msg_string не требуются)
EVENT_ID_MAIN_POWER_OK      11 - появление питания (sensor_num и msg_string не требуются)
EVENT_ID_SENSOR_ALARM_HIGH  71 - порог максимальной температуры датчика sensor_num
EVENT_ID_SENSOR_ALARM_LOW   72 - порог минимальной температуры датчика sensor_num
EVENT_ID_SENSOR_ALARM_FAULT 73 - датчик sensor_num неисправен
EVENT_ID_BoilerInput FAULT_START        74 (sensor_num и msg_string не требуются)
EVENT_ID_BoilerInput_FAULT_END          75 (sensor_num и msg_string не требуются)
EVENT_ID
sensor_num - номер отказавшего датчика температуры, 1...N
msg_string - текст сообщения для отказавшего датчика sensor_num

Если это событие без номера датчика и текста, то можно вызывать так, например:
send_fault_event(10) -- пропадание питания
--]]
  if sensor_num == nil or msg_string == nil then
    call_c_from_lua_fault_event(id)  
  else
    call_c_from_lua_fault_event(id, sensor_num, msg_string)    
  end
end
-------------------------------------------------------------------------------
function pi_control(tset_c, tsens_main_c, tsens_second_c, mode, prop_k, integr_k, integr_max_val)
--[[ Функция реализует пропорционально-интегральное управление. 
tset_c - заданная температура, град. Цельсия; 
tsens_main_c - фактическая температура основного датчика, град. Цельсия;
tsens_second_c - фактическая температура резервного датчика, град. Цельсия;
mode - режим. Пока не определенная величина. Возможно, что потребуется модификация алгоритма, тогда режим может выбирать опцию;
prop_k - коэффициент усиления пропорционального канала;
integr_k - коэффициент усиления интегрального канала.
Возвращаемая величина ret - результат вычисления ПИ алгоритма.
Алгоримт работы - при каждом вызове вычисляется возвращаемая величина по формуле:
ret = (tsens_c - tset_c)* prop_k + SUM[(tsens_c - tset_c)* integr_k * delta_t]
где SUM[] - функция, вычисляющая сумму текущего аргумета со всеми аргументами предыдущих вызовов. 
То есть накопительный сумматор или, иными словами, интегратор. При запуске начальное значение суммы равно нулю. 
Для того, чтобы интегратор не был зависим от периода вызова, интегратор имеет множитель delta_t, равный
времени между двумя вызовами, в секундах.
ToDo: Нет ограничения допустимого диапазона переменных. 
--]]
  local tsens_c
  local sign
  
  if tsens_main_c ~= nil then
      tsens_c = tsens_main_c
  elseif tsens_second_c ~= nil then
      tsens_c = tsens_second_c
  else 
    -- both sensor failed, let give up
    return 0
  end
  
  local t = os.time()
  local delta_t = t - curr_time

  curr_time = t  
  local delta_c = tset_c - tsens_c
  integr_value = integr_value + (delta_c * integr_k * delta_t)
  
  if math.abs(integr_value) > integr_max_val then
    sign = utility.sign(integr_value) -- (-1 or 1)
    if sign > 0 then 
      integr_value = integr_max_val
    else
      integr_value = -integr_max_val
    end  
  end
  
  local ret = integr_value + (delta_c * prop_k) + 5000 -- имеется в виду 5000 мВ
  if ret < 0 then ret = 0 end
  if ret > 10000 then ret = 10000 end
--  print("\t\t>>>>>PI-0", tset_c, tsens_main_c,  delta_t) 
--  print("\t\t>>>>>PI-1", (delta_c * prop_k), (delta_c * integr_k * delta_t)) 
--  print("\t\t>>>>>PI-2", ret, integr_value, delta_t, tsens_c)
  return ret
end
-------------------------------------------------------------------------------
function internet_status()
  -- Return value: 1- ok; 0- failed;
  return call_c_from_lua_internet_status()
end
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
function unit_test()
  utility.param = lip.load("param/param.ini")
  th_tab.f_ini()
  
  set_relay(1, 1)
  print(get_relay(1), get_relay(2), get_relay(3), get_relay(4), get_relay(5), get_relay(6))
  set_relay(2, 1)
  print(get_relay(1), get_relay(2), get_relay(3), get_relay(4), get_relay(5), get_relay(6))
  set_relay(6, 1)
  print(get_relay(1), get_relay(2), get_relay(3), get_relay(4), get_relay(5), get_relay(6))
  set_relay(1, 0)
  print(get_relay(1), get_relay(2), get_relay(3), get_relay(4), get_relay(5), get_relay(6))
  print(get_digit_input(1),get_digit_input(2),get_digit_input(3))
  print(get_analog_input(1),get_analog_input(2),get_analog_input(3))
  
  set_analog_output(1, 300)
  print(get_analog_output(1),get_analog_output(2),get_analog_output(3),get_analog_output(4),get_analog_output(5),get_analog_output(6))
  set_analog_output(2, 500)
  print(get_analog_output(1),get_analog_output(2),get_analog_output(3),get_analog_output(4),get_analog_output(5),get_analog_output(6))
  set_analog_output(6, 700)
  print(get_analog_output(1),get_analog_output(2),get_analog_output(3),get_analog_output(4),get_analog_output(5),get_analog_output(6))
  set_analog_output(1, 0)
  print(get_analog_output(1),get_analog_output(2),get_analog_output(3),get_analog_output(4),get_analog_output(5),get_analog_output(6))
--  get_relay(0) 
end
-------------------------------------------------------------------------------
function pi_unit_test()
  time_old = 0
  while(true) do
    if((os.clock() - (time_old + 3)) > 1) then
      time_old = os.clock()
      local res = pi_control(10, 9, nil, nil, 5, 2)
      print("TIME IS: ",os.clock(), res)
    end
  end
end

-------------------------------------------------------------------------------
function sleep(n)
  os.execute("sleep " .. tonumber(n))
end
------------------------------------------------------------------------
function timer_ton(name, vin, timeout)
--[[ Таймер для задержки включения. Должен запускаться в основном цикле программы пользователя
аргумент name - произвольное имя таймера, текст латинскими буквами без пробелов и в кавычках
аргумент vin - переменная программы, которая обрабатывается таймером. Ее фронт, т.е. переход из 0 в 1, отслеживается и по нему запускается таймер.
аргумент timeout - временной интервал в секундах
Во время взведенного таймера, т.е. по переходу vin из 0 в 1, на выходе 0
Вне взведенного таймера на выходе то же, что и на входе, т.е. выход равен vin
Временная диаграмма, видна задержка переднего фронта:
vin     ____________|-------------|_______________
output  _________________|--------|_______________
Пример использования. Вводим локальную переменную relay_1, получаем состояние реле "1" и формируем переменную vout, 
которая копирует реле "1", но начало включения задержено на 10 секунд:
local relay_1 = get_relay(1)
vout = timer_on("Timer_Relay_1", relay_1, 10) 
--]]
  if timer_t[name] == nil then
    print("\t\t Timer ON new - name= ", name)
    timer_t[name] = {}
    timer_t[name].flag = 0
    timer_t[name].time = 0
    timer_t[name].v_old = 0
  end

--  print("------------->",timer_t[name].flag, timer_t[name].v_old, timer_t[name].time)
  local time = os.time() -- current time

  if timer_t[name].v_old ~= nil then
    if  (vin == 1) and (timer_t[name].v_old == 0) then
--      print("set new timer", time, time+timeout)
      timer_t[name].flag = 1
      timer_t[name].time = time + timeout
    end
  end
  ------------------
  timer_t[name].v_old = vin
  
if timer_t[name].flag ==  1 then
    if time >= timer_t[name].time then
    timer_t[name].flag = 0  
    end
  end
  ----------------
--  print (">>", timer_t[name].flag, vin)

  if timer_t[name].flag == 1 then
    return 0
  else
    return vin
  end
end

------------------------------------------------------------------------
function timer_ton_pulse(name, vin, timeout)
--[[ Таймер для задержки включения и генерации импульса. Должен запускаться в основном цикле программы пользователя
аргумент name - произвольное имя таймера, текст латинскими буквами без пробелов и в кавычках
аргумент vin - переменная программы, которая обрабатывается таймером. Ее фронт, т.е. переход из 0 в 1, отслеживается и по нему запускается таймер.
аргумент timeout - временной интервал в секундах
Во время взведенного таймера, т.е. по переходу vin из 0 в 1, на выходе 0
При срабатывани таймера на выходе 1 ровно на один вызов. При следующих вызовах на выходе опять 0
Временная диаграмма, видна задержка переднего фронта и выходной импульс
vin     ____________|-------------|_______________
output  _________________|-|______________________
Пример использования. Нужен генератор события, после его появления отправляем событие на сервер 
if timer_on("Timer_Event_1", event, 10) == 1 then send_event() end   
--]]
  local res = 0
  if timer_t[name] == nil then
    print("\t\t Timer On-Pulse new - name= ", name)
    timer_t[name] = {}
    timer_t[name].flag = 0
    timer_t[name].time = 0
    timer_t[name].v_old = 0
  end

--  print("------------->",timer_t[name].flag, timer_t[name].v_old, timer_t[name].time)
  local time = os.time() -- current time

  if timer_t[name].v_old ~= nil then
    if  (vin == 1) and (timer_t[name].v_old == 0) then
--print("set new timer", time, time+timeout)
      timer_t[name].flag = 1
      timer_t[name].time = time + timeout
    end
  end
  ------------------
  timer_t[name].v_old = vin
  
  if timer_t[name].flag ==  1 then
    if time >= timer_t[name].time then
    timer_t[name].flag = 0  
    res = 1
    end
  end
  return res
end

------------------------------------------------------------------------
--[[ То же, что для функции timer_on(), но разница в том, что эта функция задерживает выключение по спаду
входной переменной vin.
Таймер срабатывает по спаду из 1 в 0 переменной vin
Временная диаграмма, видна задержка заднего фронта:
vin     ____________|-------------|_______________
output  ____________|---------------------|_______
Пример использования. Вводим локальную переменную relay_1, получаем состояние реле "1" и формируем переменную vout, 
которая копирует реле "1", но выключение задержено на 10 секунд:
local relay_1 = get_relay(1)
vout = timer_on("Timer_Relay_1", relay_1, 10) 
--]]
function timer_toff(name, vin, timeout)
  if timer_t[name] == nil then
    print("\t\t Timer OFF new - name= ", name)
    timer_t[name] = {}
    timer_t[name].flag = 0
    timer_t[name].time = 0
    timer_t[name].v_old = 0
  end

--  print("------------->",timer_t[name].flag, timer_t[name].v_old, timer_t[name].time)
  local time = os.time() -- current time

  if timer_t[name].v_old ~= nil then
    if  (vin == 0) and (timer_t[name].v_old == 1) then
--      print("set new timer", time, time+timeout)
      timer_t[name].flag = 1
--      print(debug.traceback("timer_toff trace:"))
      timer_t[name].time = time + timeout
    end
  end
  ------------------
  timer_t[name].v_old = vin

if timer_t[name].flag ==  1 then
    if time >= timer_t[name].time then
    timer_t[name].flag = 0  
    end
  end
  ----------------
--  print (">>", timer_t[name].flag, vin)

  if timer_t[name].flag == 1 then
    return 1
  else
    return vin
  end
end

--------------------------------------------------------------------------
function timer_pulse(name, vin, timeout)
  if timer_t[name] == nil then
    print("\t\t Timer PULSE new - name= ", name)
    timer_t[name] = {}
    timer_t[name].flag = 0
    timer_t[name].time = 0
    timer_t[name].v_old = 0
  end

--  print("------------->",timer_t[name].flag, timer_t[name].v_old, timer_t[name].time)
  local time = os.time() -- current time

  if timer_t[name].v_old ~= nil then
    if  (vin == 1) and (timer_t[name].v_old == 0) then
--      print("set new timer", time, time+timeout)
      timer_t[name].flag = 1
      timer_t[name].time = time + timeout
    end
  end
  ------------------
  timer_t[name].v_old = vin

if timer_t[name].flag ==  1 then
    if time >= timer_t[name].time then
    timer_t[name].flag = 0  
    end
  end
  ----------------
--  print (">>", timer_t[name].flag, vin)

  if timer_t[name].flag == 1 then
    return 1
  else
    return 0
  end
end
----------------------------------------------------------------
function timer_period(name, period, reset)
  --[[генерирует периодический статус true/false.
  Период задается аргументом timeout.
  Возвращаемое значение true на одном тике и false на всех остальных тиках периода, 
  как на рисунке:
  ______|-|________________|-|_______________
  Фаза выходного значения асинхронна, хотя, ее можно сбросить, если установить аргумент reset=true
  В остальных случаях reset должна быть установленя в false.
  ]]--
  local state = false
  local time
  if (timer_t[name] == nil) or (reset == true) then
    print("\t\t Timer PERIODICAL new - name= ", name)
    timer_t[name] = {}
    timer_t[name].flag = 0
--print(debug.traceback("timer_period:"))
    
    timer_t[name].time = os.time() + period
  end  
  time = os.time() -- current time

  if time >= timer_t[name].time then
    timer_t[name].time = time + period
    state = true
  else
    state = false  
  end

  return state
end
----------------------------------------------------------------
function internet_check_and_restart(timeout, restart_flag)
--[[ Функция следит, если Интернета нет в течение "timeout" интервала, то вызывает перезапуск подключения к сети
Аргумент 1 - timeout - время ожидания до перезапуска
Аргумент 2 - флаг необходимости рестарта. Пока это поле не используется
Выходное значение:
0- перезапуск не нужен;
1- сделан перезапуск;
Примечание: выходное значение можно использовать для того, чтобы проверять, помог ли перезапуск. Если перезапуск не помог, 
то можно, например, сделать аппаратный сброс. 
--]]
  local out_t = 0

  local internet_failed = 1 - call_c_from_lua_internet_status()
--  print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> t_internet_end", t_internet_end, internet_failed, internet_flag)
  if(internet_failed == 0) then
    internet_flag = false
    t_internet_end = 21474836400 -- Sat, 06 Jul 2650 08:20:00 GMT
    return 0
  end 

  if (internet_flag == true) then
    if(os.time() > t_internet_end) then
      print("---- Waring: Lua internet_check_and_restart() call , Date is ", os.date())
      local handle = io.popen("sudo /etc/init.d/networking restart")
      handle:close()
      internet_flag = false
      t_internet_end = 21474836400 -- Sat, 06 Jul 2650 08:20:00 GMT
    end 
    return 1
  else
    internet_flag = true
    t_internet_end = os.time() + timeout
    out_t = 1
  end
  return out_t
end
----------------------------------------------------------------
function infinite_counter(index)
--[[ Аргумент - индекс дискретного входа от 1 до 3
Выходное значение - состояние бесконечного (накопительного) счетчика на этом входе.
Счетчик обнлуляется только при аппаратном сбросе (включении). 
Сервер может обнаружить обнуление счетчика и учесть, чтобы не потерять накопленное значение.
Требуется базовое ПО релиза SVN>=133
--]]
  return call_c_from_lua_infinite_counter(index)
end
----------------------------------------------------------------
function antilegionella_ini()
  --[[
  Функция вызывается при инициализации. Для настройки ее параметров использует 
  переменные из файла config.lua
  Сохраняет в файле param/param.ini в секции "ANTILEGIONELLA" время следующей сессии антилегионелла
  ]]--
  local gvs_uuid
  api.antilegionella_session_on = false         

  -- check is antilegionella ON?
  local res = false
  for key,value in pairs(usr.channel_tab['objects_channels']) do
      for ch_i, ch_c in ipairs(value['channelsConsume']) do
          if ch_c['GVS']  and ch_c['checkBox_antilegionella'] then
              gvs_uuid = ch_c['uuid']
              utility.param.antilegionella.gvs_uuid = gvs_uuid -- we do not need this uuid, but it used as flag only
          end
      end
  end
  if gvs_uuid ~= nil then
    local current_time = os.time() -- "Epoch" format, seconds
    local next_session_time = current_time + (config.antilegionella_period_days * 24 * 3600)
    utility.param.antilegionella.next_session = next_session_time
  else 
      utility.param.antilegionella.next_session = nil
  end 
  utility.update_param()
end
----------------------------------------------------------------
----------------------------------------------------------------
function antilegionella_check()
  --[[
  Функция запускается периодически, раз в минуту. Для настройки ее параметров использует 
  переменные из файла config.lua
  Читает изфайла param/param.ini в секции "ANTILEGIONELLA" время следующей сессии анитилегионелла
  config.antilegionella_temperature = 65 -- температура нагрева для функции антилегионелла
  config.antilegionella_period_days = 14 -- период нагрева фукции антилегионелла
  config.antilegionella_time_minutes = 30 -- время прогрева фукции антилегионелла
  config.antilegionella_start_time_hour = 1 -- в какое время стартует фукция - время в диапазоне 0...24 час.
  
  ]]--
  local current_time = os.time() -- "Epoch" format, seconds
  local next_session_time = utility.param.antilegionella.next_session
  if next_session_time ~= nil then
    if current_time > next_session_time then
      -- we waitig upto next night's hour for session start
      local temp = os.date("!*t", os.time()) -- "!t" -> UTC time;  "*t"-> local time
      if config.antilegionella_start_time_hour == (temp.hour + utility.param.term_control.timezone) then
            if api.antilegionella_session_on == false then
              api.antilegionella_session_on = true
              antilegionella_session_time_end = current_time + (config.antilegionella_time_minutes * 60)      
            else
              if current_time > antilegionella_session_time_end then
                api.antilegionella_session_on = false         
                antilegionella_ini()
              end
            end
      end -- if config.antilegionella_start_time_hour == temp.hour
    end -- if current_time > next_session_time
  end -- if next_session_time ~= nil ...
end
----------------------------------------------------------------
function unit_test_timer()--[[

  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 1, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)
  print (timer_pulse("n", 0, 3)) sleep(1)


  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 1, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  print (timer_ton("n", 0, 3)) sleep(1)
  
  print("OFF")  

  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 1, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
  print (timer_toff("m", 0, 3)) sleep(1)
]]--
end
------------------
function unit_test_pulse_relay()
  api_unit_test_flag = true
  utility.param = lip.load("param/param.ini")
  
  th_tab.f_ini()
  
  set_relay(3, 1)
  
-- function set_pulse_relays(ind, width, profile, table)
  pulse_relay_table = ini_pulse_relays(pulse_relay_table)
  
  print "set_pulse_relays(1, 0, 0, pulse_relay_table)"
  set_pulse_relays(1, 0, 0, pulse_relay_table)

  print "set_pulse_relays(1, 1, 0, pulse_relay_table)"
  set_pulse_relays(1, 1, 0, pulse_relay_table)

  print "set_pulse_relays(1, 2, 0, pulse_relay_table)"
  set_pulse_relays(1, 2, 0, pulse_relay_table)

  print "set_pulse_relays(1, 3, 0, pulse_relay_table)"
  set_pulse_relays(1, 3, 0, pulse_relay_table)

	local x = os.clock()
	local s = 0
	while true do 
	    for i=1,100000 do set_pulse_relays(1, 0, 0, pulse_relay_table)  end
	--  for i=1,100000 do s = s + i end
	  print(string.format("elapsed time: %.2f\n", os.clock()))
	end
end


--------------------
local function unit_test_send_sms()
  api_unit_test_flag = true
  utility.param = lip.load("param/param.ini")
  th_tab.f_ini()
--  utility.param.phones.s9 = "+7910877, +7910877"
--  utility.param.phones.s49 = "+7910877, +7910877"
  
  utility.param = lip.load("param/param.ini")
  print(utility.param.phones.s9)
  print(utility.param.phones.s49)
  
  config.enable_3g=true 
  print("---------------------")
  send_sms(1, 0, "Hi авария") 
  print("---------------------")
  send_sms(0, 0, "Hi информация") 
end  
-------------------------------------------------------------------------------
--unit_test()
--unit_test_pulse_relay()
-- unit_test_send_sms()

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
return api
