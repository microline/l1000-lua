local ot =  {}
utility = require("lua/utility")
config = require("lua/config")
api = require("lua/api")

-------------------------------------------------------------------------------
-- Locals:
local ot_faults_send = {}
local unit_test_flag = false -- used for unity test only!
local ot_pumpOTDelay, boiler_request_global, tset_c_global

-------------------------------------------------------------------------------
-- GLOBAL Values
ot_boiler_comm_failed = false
ot_packets_tx = {}
ot_total_packs_num = 0 
-- Format: KEY=OT_ID, VALUE=OT_BYTE_H, OT_BYTE_L
ot_packets_rx = {}
current_ot_ind = 0 -- cycling from 0 to ot_total_packs_num-1
ot_count = 0 -- increment every packet but reset after 1 minute %k packet. For Boiler<-->termostat exchange control
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
function ot_setup(tset_c, pumpOTDelay, boiler_requests)
	tset_c_global = tset_c
	ot_pumpOTDelay = pumpOTDelay
	boiler_request_global = boiler_requests
end
-------------------------------------------------------------------------------
--	Functions Calling Tree:
--	k_event.create_k_event() => ot_2boiler_ini() => ot_control_algorithm() -> every minute, calculate algorithm
--	table_controller() => ot_setup() -> every tick, prepare all data for algorithm
-------------------------------------------------------------------------------
function ot_control_algorithm()
	local ot_water_setpoint = config.ot_fault_setpoint -- in Celsius initial value
	local ot_ch_on = 1 -- let turn on boiler for default. 
	local ot_on = utility.param.p_setup.p82_on
	local p85_max_w = (utility.param.p_setup.p85_max_w/10)-273
	local p86_min_w = (utility.param.p_setup.p86_min_w/10)-273
	if tset_c_global == nil then return ot_water_setpoint, ot_ch_on end

	if ot_on == 1 then
		ot_water_setpoint = tset_c_global
		
	else -- no OT
		ot_water_setpoint = 0
	end -- if ot_on == 1 then...

	-- bit0: CH enable [ CH is disabled, CH is enabled] >>> from OT Control Algorithm & WEB Page
	local ch = bit32.band(utility.param.p_setup.p89_stat, 0x100)
	if  ch > 0 and ot_on == 1 and boiler_request_global == true then ot_ch_on = 1 else ot_ch_on = 0 end
	-- CH Flag Policy: if Pin>0 then CH=1(true) else <timeout-10min then CH=0>
	ot_ch_on = timer_toff("OpenTherm_CH_OFF_Delay", ot_ch_on, ot_pumpOTDelay)
	if g_log_level >= 1 then  -- low level
		print("OT Water Setpoint", ot_water_setpoint, "OT CH Flag", ot_ch_on)
	end			
	return ot_water_setpoint, ot_ch_on
end
-------------------------------------------------------------------------------
function is_dhw()
	ret = 0
	-- 1: DHW enable [ DHW is disabled, DHW is enabled] >> from WEB Page
	local ret = bit32.band(utility.param.p_setup.p89_stat, 0x200)
	if ret > 0 then ret = 1 end
	return ret
end
-------------------------------------------------------------------------------
function ot_boiler_communication_check()
  local ot_boiler_comm_failed   
  local ot_fault = {}
  
  if ot_count >  0 then -- For Boiler<-->thermostat exchange control
  	ot_count = 0	
	ot_boiler_comm_failed = false
	ot_fault["comm"] = 0
  else
	ot_boiler_comm_failed = true
	local uptime =  (os.difftime( os.time(), g_uptime.start) )
--print("uptime", uptime)
	if uptime > config.ot_comm_timeout then
  		-- ALARM - boiler<-->thermostat communication failed
		ot_fault["comm"] = 1
	end
  end
--print("ot_boiler_communication_check-1", ot_count, ot_fault["comm"])
   ot_add_fault(ot_fault, 0)
   
  return ot_boiler_comm_failed
end  
-------------------------------------------------------------------------------
function ot_add_fault(ot_faults, ot_fault_oem)
	local ot_faults_list = {"service", "lockout", "low_press", "flame", "air_press", "overtemp", "oem", "comm"}
	local ot_faults_prompts = {}
	ot_faults_prompts["service"] = "OT: Необходимо обслуживание."
	ot_faults_prompts["lockout"] = "OT: Требуется ручной сброс."
	ot_faults_prompts["low_press"] = "OT: Низкое давление воды."
	ot_faults_prompts["flame"] = "OT: Сбой газа или горелки."
	ot_faults_prompts["air_press"] = "OT: Сбой давления воздуха."
	ot_faults_prompts["overtemp"] = "OT: Перегрев."
	ot_faults_prompts["oem"] = "OT: код ошибки:"
	ot_faults_prompts["comm"] = "OT: Нет связи с котлом."
	local ret
	
	for k, v in pairs(ot_faults_list) do
		if ot_faults[v] == 1 then 
			if ot_faults_send[v] == nil then
				-- alarm did not send yet, let send it 
				ot_faults_send[v] = true
				ret = ot_faults_prompts[v].." OEM:"..tostring(ot_fault_oem)
				if unit_test_flag == false then 
					send_event(1, 0, ret)
				end
			end
		end
		if timer_toff("OT_Fault_"..v, ot_faults[v], config.ot_faults_timeout) == 0 then
			ot_faults_send[v] = nil
		end
--print("OT_Fault_"..v, ot_faults[v], ot_faults_send[v])		
	end
	return ret
end
-------------------------------------------------------------------------------
--[[ LOG packet for OT
Len		=	зависит от длины поля Data, если длина Data=Dn, то Len=2+4+Dn 
%L		-	сигнатура (2 байта ‘%’ ‘L’ = 0x25 0x4C)
Time		-    	дата время – 4 байта (в секундах от 01.01.1970 года)
Data		-	данные лога
CRC
]]--
function log_packet_ascii_bytes(data_table)
-- Argument: data_table - ASCII bytes represents packet as ASCII bytes of text  
-- Processing: call C code and pass new log packet
  local unixtime = utility.get_unixtime()
  local temp_string
  local ot_n_bytes = #data_table
  local header_len = 6 -- signature(2bytes) and unixtime(4bytes)

  if utility.param.p_setup.p82_on == 1 and utility.param.p_setup.p87_log == 1 then
	  -- Let do not add first byte - "LEN" - we will add it below
	  temp_string = table.concat({string.char(0x25,0x4C), unixtime,}) -- %L - this is "text log packet" 
	  local out_string = table.concat({string.char(header_len+ot_n_bytes), temp_string})
	  
--	  out_string = table.concat({temp_string, data_table})
	  for k, v in ipairs(data_table) do
	  	out_string = out_string..string.char(v) 
	  end
	  --print(">>>>> ot_log_packet_to_boiler test dump-1:",utility.hex_dump(out_string), '\not_n_bytes', ot_n_bytes, header_len)
	  --CRC вычисляется как сумма всех байт пакета вместе с длиной и сигнатурой
	  summ = 0
	  for i=1, header_len+1+ot_n_bytes  do -- add "1" - because Len calculated too.
	    summ = (summ + string.byte(out_string,i) ) %256
	--print(">>>>> ot_log_packet_to_boiler test dump-2:", string.byte(out_string,i))
	  end
	  out_string = table.concat({out_string, string.char(summ)})
      --print(utility.hex_dump (out_string))
      call_c_from_lua_packet(out_string)
  end
end
-------------------------------------------------------------------------------
function ot_log_packet_from_boiler(ot_id)
-- Argument - index at global array ot_packets_rx
-- Format:  ot_packets_rx[ot_id] = {ot_type, ot_byte_h, ot_byte_l}
-- Processing: call C code and pass new log packet
	local temp_string
	local packet3bytes = {}
	local data_table = { 0x62, 0x6f, 0x69, 0x6c, 0x65, 0x72, 0x3a, 0x20} -- ASCII bytes for string: "boiler:<space>"
	local number_str

	if utility.param.p_setup.p82_on == 1 and utility.param.p_setup.p87_log == 1 then
	    -- ot_packets_rx[ot_id] = {ot_type, ot_byte_h, ot_byte_l}
	    packet3bytes = ot_packets_rx[ot_id]
    
		-- need convert to ASCII TEXT with <BLANKS> between ASCII NUMBERS! 
		number_str = tostring(packet3bytes[1]) -- data type 
		for i = 1, #number_str do data_table[#data_table+1] = string.byte(number_str, i) end   
   		data_table[#data_table+1] = 0x20 -- add space between numbers
		number_str = tostring(ot_id)  -- ID
		for i = 1, #number_str do data_table[#data_table+1] = string.byte(number_str, i) end   
		for i = 2, 3 do -- data High and Low bytes
	   		data_table[#data_table+1] = 0x20 -- add space between numbers
			number_str = tostring(packet3bytes[i])
			for i = 1, #number_str do data_table[#data_table+1] = string.byte(number_str, i) end   
		end
		--utility.print_r ( data_table )  
	  	log_packet_ascii_bytes(data_table)  
	end
end
-------------------------------------------------------------------------------
local function ot_add_fault_unittest()
	unit_test_flag = true
	local ret
	local f = {}
	config.ot_faults_timeout = 2
--"service", "lockout", "low_press", "flame", "air_press", "overtemp", "oem"
	f["service"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 1 ret=ot_add_fault(f, 5) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 1 ret=ot_add_fault(f, 5) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 1 ret=ot_add_fault(f, 5) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 1 ret=ot_add_fault(f, 5) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["oem"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["lockout"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["lockout"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 0 f["lockout"] = 0 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 0 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 0 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["service"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["low_press"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["flame"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 0) print(ret) os.execute("sleep " .. tonumber(1))
	f["air_press"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 8) print(ret) os.execute("sleep " .. tonumber(1))
	f["overtemp"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 7) print(ret) os.execute("sleep " .. tonumber(1))
	f["oem"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 4) print(ret) os.execute("sleep " .. tonumber(1))
	f["oem"] = 1 f["lockout"] = 1 ret=ot_add_fault(f, 4) print(ret) os.execute("sleep " .. tonumber(1))

end
-------------------------------------------------------------------------------
local function ot_log_unit_test()
	utility.param = lip.load("param/param.ini")
	th_tab.f_ini()
	local i = 100
	-- Format:  ot_packets_rx[ot_id] = {ot_type, ot_byte_h, ot_byte_l}
	ot_packets_rx[i] = {99, 12, 13}
	ot_log_packet_from_boiler(i)
	
end
-------------------------------------------------------------------------------
local function pza_calculator_unittest()
	local water_temp
	water_temp = pza_calculator(0, 2, 20) --water_temp=	8
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 3, 20)--water_temp=	27.699
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 4, 20)--water_temp=	29.506
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 5, 20)--water_temp=	31.335
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 10, 20)--water_temp=	40.81 
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 15, 20)--water_temp=	50.835
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 25, 20)--water_temp=	72.535
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(0, 26, 20)--water_temp=	8
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-10, 15, 20)--water_temp=	64.185
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-15, 15, 20)--water_temp=	70.14
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-20, 15, 20)--water_temp=	75.615
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-25, 15, 20)--water_temp=	80.61
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-15, 15, 10)--water_temp=	47.75
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-15, 15, 30)--water_temp=	90
	print ("water_temp=", water_temp)	
	water_temp = pza_calculator(-15, 15, 30)--water_temp=	90
	print ("water_temp=", water_temp)	
end
-------------------------------------------------------------------------------
--Unit Test start line=> lua/ot.lua
--ot_add_fault_unittest()
--ot_log_unit_test()
--pza_calculator_unittest()
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
return ot






