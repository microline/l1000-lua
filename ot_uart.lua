local ot_uart =  {}

utility = require("lua/utility")
ot = require("lua/ot")

local uart1_rx_buff = {}
local uart2_rx_buff = {}
local unit_test_flag = false -- used for unity test only!
local ot_id0_hb_status=0; 
local ot_max_modulation=0; 
local ot_room_setpoint=0; 
local ot_room_temp=0; 
local control_setpoint_hb=0
local control_setpoint_lb = 0
local max_modulation_hb=0
local room_setpoint_hb=0
local room_setpoint_lb=0
local room_temp_hb=0
local room_temp_lb=0
local dhw_setpoint_hb=0
--[[
-------------------------------------------------------------------------------
-- GLOBAL Values, will be re-calculated below
ot_packets_tx = {}
ot_total_packs_num = 0 
-- Format: KEY=OT_ID, VALUE=OT_BYTE_H, OT_BYTE_L
ot_packets_rx = {}
current_ot_ind = 0 -- cycling from 0 to ot_total_packs_num-1
ot_count = 0 -- increment every packet but reset after 1 minute %k packet. For Boiler<-->termostat exchange control
-------------------------------------------------------------------------------
]]--
local function ot_2bytes(data)
	-- convert float value to two bytes for OT packet
	-- f8.8 signed fixed point value : 1 sign bit, 7 integer bit, 8 fractional bits (two’s compliment ie. the
	-- LSB of the 16bit binary number represents 1/256th of a unit).
	-- Example : A temperature of 21.5°C in f8.8 format is represented by the 2-byte value 1580 hex
	--(1580hex = 5504dec, dividing by 256 gives 21.5)
	-- A temperature of -5.25°C in f8.8 format is represented by the 2-byte value FAC0 hex
	-- (FAC0hex = - (10000hex-FACOhex) = - 0540hex = - 1344dec, dividing by 256 gives -5.25)
	-- return hb, lo (Higth byte, Low byte)
	local hb=0; local lb=0
	local byte
	if data >= 0 then
		byte = data * 256
		hb = bit32.rshift(byte, 8)
		lb = bit32.band(byte, 0xFF)
	else
		byte = math.abs(data * 256)
		byte = 0x10000 - byte
		hb = bit32.rshift(byte, 8)
		lb = bit32.band(byte, 0xFF)
	end
	return hb, lb
end
-------------------------------------------------------------------------------
function ot_2boiler_ini()
-- let call this ini function one time per minute
--[[
HB: Master status flag8 bit: description [ clear/0, set/1]
0: CH enable [ CH is disabled, CH is enabled] >>> from OT Control Algorithm & WEB Page
1: DHW enable [ DHW is disabled, DHW is enabled] >> from WEB Page
2: Cooling enable [ Cooling is disabled, Cooling is 
enabled]
3: OTC active [OTC not active, OTC is active]
4: CH2 enable [CH2 is disabled, CH2 is enabled]
5: reserved
6: reserved
7: reserved
LB: Slave status flag8 bit: description [ clear/0, set/1]
0: fault indication [ no fault, fault ]
1: CH mode [CH not active, CH active]
2: DHW mode [ DHW not active, DHW active]
3: Flame status [ flame off, flame on ]
4: Cooling status [ cooling mode not active, cooling
mode active ]
5: CH2 mode [CH2 not active, CH2 active]
6: diagnostic indication [no diagnostics, diagnostic event]
7: reserved
]]--
	
	-- first call failed because utility.param does not initializated yet. Let check
	if utility.param ~= nil then 
		local ot_water_setpoint, ot_ch_on, ot_dhw
		ot_water_setpoint, ot_ch_on = ot_control_algorithm()
		ot_dhw = is_dhw()
		ot_id0_hb_status = ot_ch_on + ot_dhw * 2
		
		control_setpoint_hb, control_setpoint_lb = ot_2bytes(ot_water_setpoint)
		
		ot_max_modulation = utility.param.p_setup.p88_max_m/256
		max_modulation_hb = ot_2bytes(ot_max_modulation)
	
		room_setpoint_hb, room_setpoint_lb = ot_2bytes(ot_room_setpoint)
		room_temp_hb, room_temp_lb = ot_2bytes(ot_room_temp)
		
		dhw_setpoint_hb = ot_2bytes(utility.param.p_setup.p90_dhw/256)
	end

	-- do not forget for Baxi U72 issue - id=127/128/129 !!	
	ot_packets_tx = {
	{0, 0, ot_id0_hb_status, 0},--0	Статус Бойлера WR
	{16,1, control_setpoint_hb, control_setpoint_lb}, --1 f8.8 Control setpoint: CH water temperature setpoint
	{16,2, 0, 4},			--2 HB=0, LB: Master MemberID code=4 (the same as for Baxi U72)
	{0, 3, 0, 0},			--3 конфигурация Бойлера
	{0, 5, 0, 0},			--4 Application-specific fault flags
	{0, 6, 0, 0},			--5 Remote boiler parameter transfer-enable & read/write flags
	{0, 9, 0, 0},			--6	Remote override room setpoint 
	{16,14, max_modulation_hb, 0}, --7	f8.8 Max Relative Modulation Level
	{16,16, room_setpoint_hb, room_setpoint_lb}, --8	f8.8 Room Setpoint
	{0, 17, 0, 0},			--9	Relative Modulation Level
	{0, 18, 0, 0},			--10 CH water pressure
	{0, 19, 0, 0},			--11 DHW flow rate
	{16,24, room_temp_hb, room_temp_lb}, --12 f8.8 Current sensed room temperature (°C)
	{0, 25, 0, 0},			--13 Boiler water temp
	{0, 26, 0, 0},			--14 DHW temperature
	{0, 27, 0, 0},			--15 Outside temperature
	{0, 28, 0, 0},			--16 Return water temperature f
	{0, 48, 0, 0},			--17 DHW setpoint upper & lower bounds for adjustment (°C)
	{0, 49, 0, 0},			--18 Max CH water setpoint upper & lower bounds for adjustment (°C)
	{0, 90, 0, 0},			--19  
	{0, 100, 0, 0},			--20 Remote override function
	{16,126, 1, 0x31},		--21 Baxi U72 fix: Master product version number and type
	{0, 127, 0, 0},			--22 Slave product version number and type
	{0, 128, 0, 0},			--23
	{16,129, 2, 0},			--24 Baxi U72 fix:
	{16,56, dhw_setpoint_hb, 0}, --25 f8.8 Setup Domestic hot water temperature
	}
	ot_total_packs_num = #ot_packets_tx -- GLOBAL Value

end
-------------------------------------------------------------------------------
local function ot_2boiler_tx(ind)
-- OT packet with index "ind" as argument sends to boiler
-- WARNING: index "ind" started from ZERO !!
  local data_table = {}
  local packet = {}
  local packet_4bytes = ot_packets_tx[ind + 1]
--[[ this code assume that OT Adapter does not add event bit but IT DOES !  
  local sum = 0
  for i , byte in ipairs(packet_4bytes) do
	local cnt = 0
  	while byte ~= 0 do
  		sum = sum + bit32.band(byte, 1)
  		byte = bit32.rshift(byte, 1)
  		cnt = cnt +1
  	end
  end
  -- The parity bit should be set or cleared such the total number of ‘1’ bits in the entire 32 bits of the message is even.
  if sum % 2 ~= 0 then
--	  packet_4bytes[1] = packet_4bytes[1] + 128
  end
--print("sum", sum, "first byte", packet_4bytes[1])
]]--
  local data_table = {35, 68, 52, 55, 52, 50, 61} -- "#D4742=" as ASCII hardcoded
  
  -- let assume that packet_4bytes is 4 digit 
  -- BUT we need convert to ASCII TEXT with <BLANKS> between ASCII NUMBERS! 
  for i = 1, 4 do
  	local number_str = tostring(packet_4bytes[i])
	for i = 1, #number_str do
    	local c = number_str:sub(i,i)
    	data_table[#data_table+1] = string.byte(number_str, i) -- return the ASCII value   
  	end
  	if i < 4 then 
  	   	data_table[#data_table+1] = 0x20 -- add space between numbers
  	end   
  end
--print('ot_2boiler_tx-1', packet_4bytes[1], packet_4bytes[2], packet_4bytes[3], packet_4bytes[4])
--utility.print_r ( data_table )  
  
  log_packet_ascii_bytes(data_table)
  
  packet = wrapped2textpacket(data_table)
  if unit_test_flag == false then
    for i , byte in ipairs(packet) do
      send_byte_tx1(byte)
    end
  else
    utility.print_r ( data_table )  
  end
end
-------------------------------------------------------------------------------
function ot_2boiler_next_tx()      
	-- every call this function send to boiler next OT packet. Repeat it in cycling mode.
	ot_2boiler_tx(current_ot_ind)
	if current_ot_ind == (ot_total_packs_num -1) then 	
		current_ot_ind = 0 
	else
		current_ot_ind = current_ot_ind + 1
	end
end
-------------------------------------------------------------------------------
function tableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end
-------------------------------------------------------------------------------
local function  crc_calc(packet, start_ind, last_ind)  
--[[Word SUM_CRC( Byte *Address, Byte Lenght)
{
  int N;
  unsigned short WCRC=0;
  for (N=1;N<=Lenght;N++,Address++)
   {
    WCRC+=(*Address)^255;
    WCRC+=((*Address)*256);
   }
  return WCRC;
} ]]--
  local crc_1 = 0
  local crc_2 = 0
  local crc = 0
  local byte
  for i=0, (last_ind - start_ind) do
    byte = packet[start_ind + i] 
    
    
    -- FOR DEBUG ONLY! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if byte == nil then 
    	print("CRC_ISSUE_1", start_ind, last_ind)
    	print(debug.traceback())
    	utility.print_r ( packet )  
    end
    
    
--    print("byte=", byte, crc, math.floor(crc / 256))
    crc = (crc + bit32.bxor (byte, 255))%(65536)
    crc = (crc + (byte * 256))%65536
  end
  crc_1 = crc % 256
  crc_2 = math.floor(crc / 256) 
--print("CRC1 CRC2", crc_1, crc_2)
--print(debug.traceback())
  return crc_1, crc_2
end
-------------------------------------------------------------------------------
local function packet_extractor(buff)
  local res = false
  local data_index = 0
  local length = 0
  local address_index = 0
  local crc_1 = 0
  local crc_2 = 0

--print(utility.print_r ( buff ) )  
  -- signature is following: 2, 'M', 'l', 2 - let find out it
  local buff_len = #buff
  for i=1, buff_len-3 do -- do not touch memory AFTER buff, so let stop before 'buff-3'
    if buff[i] == 2 then 
      if buff[i+1] == string.byte('M', 1) then 
        if buff[i+2] == string.byte('l', 1) then
          if buff[i+3] == 2 then address_index = i+4 break end
        end
      end      
    end -- if buff[i] == 2...
  end -- for i=1, buff...
--print(">>>2", buff_len, address_index)
  if address_index > 0 and buff_len >= address_index+2  then 
    length = buff[address_index + 2]
-- 2 M l 2 0 0 L 0x20 D1 D2 ... DN CRC1 CRC2
-- 1 2 3 4 5 6 7  8    9 10 ...8+N  9+N 10+N
--print(">>>3", length, buff_len, address_index+length + 2)
    if buff_len >= address_index+length + 4 then
      crc_1, crc_2 = crc_calc(buff, address_index, address_index+length+2)
--print(">>>4", crc_1, buff[address_index+length+3], crc_2, buff[address_index+length+4])
      if crc_1 == buff[address_index+length+3] and crc_2 == buff[address_index+length+4] then
        res = true
        data_index = address_index + 4
      end
    end
  end -- if address_index > 0...
--print(">>>5", res, data_index, length)
  return res, data_index, length-1 -- length decreased for One due to first blank symbol (0x20)
end
-------------------------------------------------------------------------------
local function ot_packet_processing(buff, data_index, length)
  -- OT packet parser 
  --s = string.char(N) -- return a string constructed from ASCII values
  local telegram = {}
  local token, ot_type, ot_id, ot_byte_h, ot_byte_l
  local res = false
  local ot_array = {}
  for i=1, length do
    local char = string.char(buff[data_index+i-1])
    telegram[#telegram+1] = char 
--print (">0",char)
  end
  local telegram_str = table.concat(telegram)
--print(">>>>6", string.sub(telegram_str, 1, 7))    
  if (string.sub(telegram_str, 1, 7) == "#D4742:") then
    -- extract data -> 4 bytes in ASCII with space delimiter
    local data_str = string.sub(telegram_str, 8)
--print(">>>>7", data_str)
    res = true
    local ind = 1
    for token in string.gmatch(data_str, "[^%s]+") do 
      local digit = tonumber(token)
      if digit == nil then res = false end        
      ot_array[#ot_array+1] = digit
    end
--print('ot_packet_processing-1')
--utility.print_r ( ot_array )  
    if #ot_array == 4 and res == true then
      ot_type = ot_array[1]
--[[TYPE: <b6 b5 b4> from <b7...b0>
4 - READ-ACK (Slave-to-Master)
5 - WRITE-ACK (Slave-to-Master)
6 - DATA-INVALID (Slave-to-Master)
7 - UNKNOWN-DATAID (Slave-to-Master) ]]--
      local ot_type_nimb = bit32.extract (ot_type, 4, 3 ) -- <b6 b5 b4>
      if ot_type_nimb == 4 or ot_type_nimb == 5 then
        ot_id = ot_array[2]
        ot_byte_h = ot_array[3]
        ot_byte_l = ot_array[4]
        -- OT packet arriving every second. They stored at memory and every minute
        -- packet into "k" packet and sends to server
        -- Lets store OT packet at table.
        -- Table has KEY - means OT ID and VALUE - means ByteH and ByteL
        ot_packets_rx[ot_id] = {ot_type, ot_byte_h, ot_byte_l}
        
        ot_log_packet_from_boiler(ot_id, ot_packets_rx)
        
--print(ot_id, ot_packets_rx[ot_id][1], ot_packets_rx[ot_id][2], ot_packets_rx[ot_id][3] )
        ot_count = ot_count + 1 -- increment every packet but reset after 1 minute %k packet. For Boiler<-->termostat exchange control
--print(">>>>>9", res, ot_type, ot_id, ot_byte_h, ot_byte_l)
      end -- if ot_type == 4 or...
    end
  end -- if (string.sub(telegram_str, 1, 7) == "#D4742:...
  return res
end
-------------------------------------------------------------------------------
function call_lua_from_c_uartrx(uart_num, data_len, buff)
  -- for test only:  
--  print("UART-RX", uart_num, data_len)
  
  local res = false
  local length = 0
  local data_index
  
  if uart_num == 1 then -- K-Line -->> OT packet parser
  
  -- WARNING: this is assume that we have SHORT packet, not more then 50 bytes !!!!!!!!!!!!!
  
    uart1_rx_buff = tableConcat(uart1_rx_buff,buff)
    
    -- Let find out packet signature at the buffer
    res, data_index, length = packet_extractor(uart1_rx_buff)
    if res == true then 
      res = ot_packet_processing(uart1_rx_buff, data_index, length)
      if g_log_level >= 2 then  
--        print ("OT Packet Received Result:", res)
      end
      uart1_rx_buff = {}
      end
    -- Let search packet from OT adapter. The length < 50, so if length>50 let delete it
    if #uart1_rx_buff > 50 then uart1_rx_buff = {} end
    
  end
  
end

-------------------------------------------------------------------------------
function wrapped2textpacket(data_table)
--[[
Function wrapps data to standard packet for K-Line and so on.
Format:
    0x02,'M', 'l' ,0x02 - сигнатура 4 байта
    Adr_dest - адрес приёмника 1 байт (0 – широковещат кадр)
    Adr_source - адрес источника 1 байт
    Len_data - длина данных 1 байт
    Data - данные Len_data байт
    CRC - 2 байта
Первый байт в блоке данных определяет тип сообщения. 
0x20 – текстовое сообщение (текст оканчивается \0 - кажется, не обязательно)
Argument - table contains 4 bytes
Output - table contains 4 + 10 = 14 bytes -> packet
]]--
  -- signature
  local packet = {}
  local data_len = #data_table
  packet[1] = 2
  packet[2] = 0x4D -- 'M'
  packet[3] = 0x6C -- 'l'
  packet[4] = 2
  packet[5] = 0 -- addr_dest 
  packet[6] = 0 -- addr_source
  packet[7] = data_len + 1 -- data length + space at packet[10]
  packet[8] = 0x20 -- text format type
  for i, d in ipairs(data_table) do
    packet[8+i] = data_table[i] -- note: i=1,2,3,..data_len+1
  end
  packet[9+data_len] = 0 -- CRC_1 (for explanation only, updated below)
  packet[9+data_len+1] = 0 -- CRC_2
  packet[9+data_len], packet[9+data_len+1] = crc_calc(packet, 5, 8+data_len)  
  return packet
end
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
function wrapped2textpacket_unittest()
  print("wrapped2textpacket test START")
  local data_table = {}
  local packet = {}
  local str = "#D4742=0 0 15 0"
  for i = 1, #str do
    local c = str:sub(i,i)
    data_table[#data_table+1] = string.byte(str, i) -- return the ASCII value   
  end
  packet = wrapped2textpacket(data_table)
  for i , byte in ipairs(packet) do
  	send_byte_tx1(byte)
  end
--[[
  local data_table = {35, 68, 52, 55, 52, 50, 61}
  local packet = {}
  local data = {16,2, 0, 4}			--2 HB=0, LB: Master MemberID code=4 (the same as for Baxi U72)
  for i = 1, 4 do
    data_table[#data_table+1] = data[i]   
  end
  print('wrapped2textpacket_unittest-2')
  utility.print_r ( data_table )  
  packet = wrapped2textpacket(data_table)
  for i , byte in ipairs(packet) do
  	send_byte_tx1(byte)
  end
  ]]--

  ----------
--  local inp_packet = {2, 77, 108, 2, 0, 0, 16, 32, 35, 68, 52, 55, 52, 50, 61, 48, 32, 48, 32, 49, 53, 32, 48, 242, 10}
  -- #D4742:64 0 15 11
  -- 
  ---[[
  local inp_packet = {2, 77, 108, 2, 0, 0, 18, 32, 35, 68, 52, 55, 52, 50, 58, 54, 52, 32, 48, 32, 49, 53, 32, 49, 49, 133, 119}
  call_lua_from_c_uartrx(1, 25, inp_packet)
  ---------
  --  bit32.extract (n, field [, width])
  -- Returns the unsigned number formed by the bits field to field + width - 1 from n. 
  -- Bits are numbered from 0 (least significant) to 31 (most significant). All accessed bits must be in the range [0, 31]. 
  print (bit32.extract (0x7F, 4, 3 ) )
  ot_packets_rx[10] = {0,3,4}
  print(ot_packets_rx[10][1])
  print(ot_packets_rx[10][2])
  print(ot_packets_rx[0][1], ot_packets_rx[0][2])
  print("--")
  for k, v in pairs(ot_packets_rx) do
    print(k, v[1], v[2])
  end
  print("wrapped2textpacket test END2")
end
-------------------------------------------------------------------------------
local function ot_2boiler_unittest()
	local ot_control_setpoint=0; 
	local ot_dhw_setpoint=0
	unit_test_flag = true
	ot_control_setpoint = -5.25 --21.5
	ot_max_modulation=100; 
	ot_room_setpoint=21.1; 
	ot_room_temp=23.9; 
	ot_dhw_setpoint=43
	ot_2boiler_ini()
--	utility.print_r ( ot_packets_tx )  
	print (control_setpoint_hb, control_setpoint_lb, max_modulation_hb, room_setpoint_hb, room_setpoint_lb, room_temp_hb, room_temp_lb, dhw_setpoint_hb)
	for i = 0, ot_total_packs_num-1 do
		print("packet id", i)
		ot_2boiler_tx(i)
	end
--[[
	]]--
end
-------------------------------------------------------------------------------
-- Do not comment this call - it's initialization! Need to calculate ot_total_packs_num variable here:
ot_2boiler_ini()
-------------------------------------------------------------------------------
-- UNIT TESTs:
--wrapped2textpacket_unittest()
--ot_2boiler_unittest()

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

return ot_uart