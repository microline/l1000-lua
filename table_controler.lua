local table_controler =  {}

--[[
 Этот файл содержит дополнение к коду api.lua для обработки контуров управления, 
 сгенерированных локальной утилитой настройки.
 
 ToDo:
 - антилегионелла;
 - ГВС приоритет для арбитра;
 - прокручивание сервомоторов летом;
 - каскад
 - ПЗА
 - Резервный режим - тестировать u_id для арбитра - как там срабатывает два таймера, для осн. и для резерва?
]]--
local utility = require("lua/utility")
local get = require("lua/get")
local api = require("lua/api")
local ot = require("lua/ot")
local config = require("lua/config")

local test_value = false -- Используется для тестирования События по состоянию переменной

-------------------------------------------------------------------------------
-- Global variables for this file 
local sensor_main_map_tab = {} 
local sensor_res_map_tab = {} 
local sensor_water_in_map_tab = {} 
local sensor_water_out_map_tab = {} 
local sensor_outside_map_tab = {} 

local controller_t = {}
local object_t = {}
local is_current_boiler_main = true
local cascade_number = 1 -- how many boilers currently in cascade. Changed from 1 to Max
local cascade_max
local cascade_t -- table, contains u_id's of boilers. Original list is created at the start then rotate each days
local cascade_t_ind -- index of current boiler in the cascade_t table
local delayCascad -- delay before Cascade is switched in seconds
local treshould_flags_max_list = {}
local treshould_flags_min_list = {}
local rotateBoilersPeriod -- rotaite period in seconds

-------------------------------------------------------------------------------
local function pza_calculator(outside_temp, n, tset_c)
	--[[
	Curve for tset_c=20C. Therefore need shift Curve for another value
	n - Curve's number [3..25]
	A: (n) ->                n*0.0005 + 0.0021
    B: (n) -> -n**2*0.0001 + n*0.095  - 0.0165
    C: (n) ->  n**2*0.011  + n*1.73   + 22.41
    water_temp => -A(n)*ot**2 - B(n)*ot + C(n)
    if water > 90 then water = 90
    if water < 8 then water = 8
	]]--
	local water_temp_min = 8
	local water_temp_max = 90
	local water_temp = water_temp_min
	if (outside_temp == nil) or (n == nil) or (tset_c == nil) then return water_temp_min end
	if (n < 3) or (n > 25) then return water_temp_min end

	local delta = tset_c - 20 -- shift it

	local t = outside_temp - delta
	local a = (n*0.0005) + 0.0021
	local b = (n*n*0.0001) + (n*0.095)  - 0.0165
	local c = (n*n*0.011)  + (n*1.73)   + 22.41
	
	water_temp = delta + c - (a*t*t) - (b*t)
	if water_temp > water_temp_max then water_temp = water_temp_max end
	if water_temp < water_temp_min then water_temp = water_temp_min end
	return water_temp
end
-------------------------------------------------------------------------------
function treshold_controller()
  local hyster = 1 -- means that decrease of events volume
  for key, minmax_set in pairs(th_tab.ttresh_arr_c) do
    if minmax_set ~= nil then
      if th_tab.tsens_arr_c[key] ~= nil and minmax_set['max'] ~= nil and minmax_set['min'] ~= nil then
        if  (th_tab.tsens_arr_c[key] > (minmax_set['max']+hyster) ) and (minmax_set['max'] > 0 ) then
          if  treshould_flags_max_list[key] == false then -- both nil and false will act as a false condition !!
            send_event(1, 0, 'Датчик '..th_tab.sens_status[key]["name"]..': температура выше порога! '..tostring(th_tab.tsens_arr_c[key])) --
            treshould_flags_max_list[key] = true
          end 
        else
          treshould_flags_max_list[key] = false
        end
        if  th_tab.tsens_arr_c[key] < (minmax_set['min'] - hyster) and (minmax_set['min'] > 0 )  then
          if  treshould_flags_min_list[key] == false then -- both nil and false will act as a false condition !!
            send_event(1, 0, 'Датчик '..th_tab.sens_status[key]["name"]..': температура ниже порога! '..tostring(th_tab.tsens_arr_c[key])) -- 
            treshould_flags_min_list[key] = true
          end 
        else
          treshould_flags_min_list[key] = false
        end -- if  th_tab.tsens_arr_c[key] <
      end -- if th_tab.tsens_arr_c[key] ~= nil
---------------      
    end -- if minmax_set ~= nil
  end -- for key, minmax_set in pairs(th
end 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
local function sensor_find(serial)
  if(utility.param.s14_s17_setup ~= nil) then
    for i, sens_serial in ipairs(th_tab.tsens_arr_ser)do --
      if serial == sens_serial then
        return i
      end
    end
  end  
  return nil
end
----------------------------------------------------------------
local function send_alarm(type, text)
--[[
type - тип события, возможные значения: 
"ALARM" -> авария; 
"INFO"- информация; 
"NONE"- событие не посылается
text - текст события
]]--
--print("**************** SEND TO SERVER", type, text)
  if type == "NONE" then
    -- nothing to do
  elseif type == "ALARM" then
    send_event(1, 0, text)
  elseif type == "INFO" then
    send_event(0, 0, text)
  else
    -- can not stay here..
    print("***** ERROR - wrong event type", type)
  end
end
----------------------------------------------------------------
function events_controller(ev_t)
  local conditional
  local cond_res
  local res
  local u_id
  local relay_ind
--  utility.print_r ( ev_t )  
  for key, value in pairs(ev_t) do
    for key, eventOut in pairs(value) do
      u_id = eventOut['u_id']
      relay_ind = eventOut['relayEventInd']
--      print ("EVENT NAME",eventOut['eventName'])
--      print ("u_id",eventOut['u_id'])
      cond_res = false
      local timeoutEvent = eventOut['timeoutEvent'] -- in seconds
      local alarmEventType = eventOut['alarmEventType']
      local infoEventType = eventOut['infoEventType']
      local noEventType = eventOut['noEventType']
      local curr_state =false -- current event state for calculator
      local alarm_type = nil
      if alarmEventType then
        alarm_type = "ALARM" 
      elseif infoEventType then
        alarm_type = "INFO" 
      elseif noEventType then
        alarm_type = "NONE" 
      else
        print("***** ERROR Wrong Event Type")
        return
      end

      if eventOut.inpLogicList ~= nil then
--        utility.print_r ( eventOut )
        -- 0 - AND
        -- 1 - OR
        conditional = eventOut.inpLogicList.conditional
        local condt_list = {}
        if eventOut.inpLogicList.inputCondList ~= nil then

          for key, evenIn in pairs(eventOut.inpLogicList.inputCondList) do
            if evenIn ~= nil then
                if evenIn.radioButtonName == 'analog' then
                  local analogCond = evenIn.analogCond -- conditional: 0->">"; 1->"<"
                  local mv = get_analog_input(evenIn.analogNum+1)
                  local treshould = evenIn.millivolts
                  res = false
                  if treshould ~= nil then
                    res = ((mv>treshould) and (analogCond == 0))or
                      ((mv<treshould) and (analogCond == 1))
                  end 
--                  print("\t\t<analog-1>", analogCond, mv, treshould, res, evenIn.analogNum)
                  table.insert(condt_list, res)
                end
                if evenIn.radioButtonName == 'discret' then
                  local discretCond = evenIn.discretCond
                  local discret = get_digit_input(evenIn.discretNum+1)
                  res = (discret==1 and discretCond==0) or (discret==0 and discretCond==1)
                  table.insert(condt_list, res)
                end
                if evenIn.radioButtonName == 'relay' then
                  local relay = get_relay(evenIn.relayNum+1)-- 1->"ON"; 0->"OFF"
                  local cond = evenIn.relayStatus --0->"HI"; 1->"LO"
                  res = (relay ~= cond)  -- because ON ~= HI
                  table.insert(condt_list, res)
                end
                if evenIn.radioButtonName == 'sensor' then
                  local temper = th_tab.tsens_arr_c[sensor_find(evenIn.sensorSer)]  
                  local sensorCond = evenIn.sensorCond
                  local tresh = evenIn.sensorTemp
                  res = false 
                  if temper ~= nill and tresh ~= nil then
                    res = (temper>tresh and sensorCond==0) or (temper<tresh and sensorCond==1)
                  end 
                  table.insert(condt_list, res)
                end
                if evenIn.radioButtonName == 'value' then
                  local valueName = _G[evenIn.valueName]
                  local valueState = evenIn.valueState
                  res = false
                  local valueStateBool
                  if valueState==0 then valueStateBool=true else valueStateBool=false end
                  if valueName ~= nil and valueStateBool ~= nil then
                    res = valueName==valueStateBool
                  end
                  table.insert(condt_list, res)
                end -- if evenIn.radioButtonName == 'value' then
            end -- if evenIn ~= nil then
          end -- for key, evenIn in pairs(eventOut.inpLogicList.inputCondList) do
          
          if conditional == 0 then -- AND
            cond_res = true
            for k, v in pairs(condt_list) do
              if v == false then 
                cond_res = false 
                break
              end 
            end
          else -- OR
            cond_res = false
            for k, v in pairs(condt_list) do
              if v == true then 
                cond_res = true 
                break
              end
            end
          end -- if conditional ==...
          local res_1
          
          if cond_res==true then res_1=1 else res_1=0 end 
          if timer_ton_pulse("Event_"..tostring(u_id), res_1, timeoutEvent) > 0 then
            send_alarm(alarm_type, eventOut['eventText']) 
          end
          
          if relay_ind > 0 then
            if (cond_res == true) then 
              set_relay(relay_ind, 1) 
            else 
              set_relay(relay_ind, 0) 
            end
          end
                  
        end -- if eventOut.inpLogicList.inputCondList ~= nil then
      end -- if eventOut.inpLogicList ~= nil then
    end -- for key, eventOut in pairs(value) do
  end -- for key, value in ev_t do
end
----------------------------------------------------------------
function table_controler.obj_chann_update()-- add to param.ini objects and channels sections
  for key,value in pairs(usr.channel_tab['objects_channels']) do
    local uuid = value['uuid']
    if uuid ~= nil then -- it's possible that one element is empyt and has not uuid, skip it
      if utility.param[uuid] == nil then
        utility.param[uuid] = {}
        utility.param[uuid].mode = 0 -- turn off
      end 
  
      for ch_i, ch_c in ipairs(value['channelsConsume']) do
        uuid = ch_c['uuid']
  
        if utility.param[uuid] == nil then
          utility.param[uuid] = {}
          utility.param[uuid].tset_comf = 2740
          utility.param[uuid].tset_eco = 2740
          utility.param[uuid].tset_off = 2740
          utility.param[uuid].tcalend_int_1 = "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_2 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_3 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_4 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_5 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_6 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_7 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_fract_1 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_2 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_3 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_4 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_5 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_6 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_7 = "T000000000000000000000000"
        end 
      
      end  -- for ch_i, ch_c in ipairs(value['channelsConsume']) do
      
      for ch_i, ch_s in ipairs(value['channelsSource']) do
        uuid = ch_s['uuid']
  
        if utility.param[uuid] == nil then
          utility.param[uuid] = {}
          utility.param[uuid].tset_comf = 2740
          utility.param[uuid].tset_eco = 2740
          utility.param[uuid].tset_off = 2740
          utility.param[uuid].tcalend_int_1 = "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_2 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_3 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_4 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_5 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_6 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_int_7 =  "T202020202020202020202020202020202020202020202020"
          utility.param[uuid].tcalend_fract_1 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_2 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_3 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_4 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_5 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_6 = "T000000000000000000000000"
          utility.param[uuid].tcalend_fract_7 = "T000000000000000000000000"
        end
      end -- for ch_i, ch_s in ipairs(value['channelsSource']) do
    end --if uuid ~= nil then
  end -- for key, value in pairs(usr.channel_tab[...
end 
----------------------------------------------------------------
local function sensor_mapping(map_table, u_id, sensor_serial)   
  local sens_ind = nil
  -- Let have mapping cash table between u_id and sernsor_index. 
  -- local map_table -> {sens_ind_1, sens_ind_2,..}
  if map_table[u_id] == nil then
    sens_ind = sensor_find(sensor_serial)
    if sens_ind ~= nil then -- search does not failed
      map_table[u_id] = sens_ind
    end
  else -- found cash 
    sens_ind = map_table[u_id] 
  end
  return sens_ind
end    
----------------------------------------------------------------
local function cascade_switch(uuid_str)
  --[[-Логика каскада. 
  Если Ттн системы>дельта, в течении времени Тзадержки, включить ведомый1, если он уже вкл, то вкл. ведомый2 и т.д.
  Если Ттн системы<дельта, в течении времени Тзадержки, откл ведомый2, если он уже откл, то откл. ведомый1 и т.д.
  Ротация котлов - сдвиг статуса (для равномерного износа котлов) 
  deltaTempCascad <> sens_water_out - sens_water_in
  Ттн = sens_water_out
  delayCascad = Тзадержки
  rotateBoilersPeriod - период ротации, сутки
  
  Calculate cascade_number - how many boiles is needed. 
  ]]--       
    local water_out, water_in
    local deltaTempCascadMin, deltaTempCascadMax
    local cascade_switch_status
     
    water_out = th_tab.tsens_arr_c[object_t[uuid_str].sens_water_out_ind]  
    water_in = th_tab.tsens_arr_c[object_t[uuid_str].sens_water_in_ind]
    deltaTempCascadMin = object_t[uuid_str].deltaTempCascadMin
    deltaTempCascadMax = object_t[uuid_str].deltaTempCascadMax

    if (water_out - water_in) > deltaTempCascadMax  then
        cascade_switch_status = timer_ton("Cascade_Switch_Timeout_Incr", 1, object_t[uuid_str].delayCascad)
        if cascade_switch_status == 1 then
            cascade_number = cascade_number + 1
            if cascade_number > cascade_max then
              cascade_number = cascade_max
            end
            cascade_switch_status = timer_ton("Cascade_Switch_Timeout_Incr", 0, object_t[uuid_str].delayCascad)
        end
    else
        cascade_switch_status = timer_ton("Cascade_Switch_Timeout_Incr", 0, object_t[uuid_str].delayCascad)
    end
    
    if (water_out - water_in) < deltaTempCascadMin  then
        cascade_switch_status = timer_ton("Cascade_Switch_Timeout_Decr", 1, object_t[uuid_str].delayCascad)
        if cascade_switch_status == 1 then
            cascade_number = cascade_number - 1
            if cascade_number < 1 then
              cascade_number = 1
            end
            cascade_switch_status = timer_ton("Cascade_Switch_Timeout_Decr", 0, object_t[uuid_str].delayCascad)
        end
    else
        cascade_switch_status = timer_ton("Cascade_Switch_Timeout_Decr", 0, object_t[uuid_str].delayCascad)
    end

    if  timer_period("Boiler_Rotate_Period", rotateBoilersPeriod, false) then
        cascade_t_ind = (cascade_t_ind % cascade_max) + 1    
    end
end        
----------------------------------------------------------------
local function boiler_arbiter(uuid_str, boiler_requests, u_id, isSourceMain)
  local res = false
  local water_out, water_in, water_out_min
  local reserve_switch_status = 0
  local main_switch_status = 0
  if boiler_requests == true then
      if object_t[uuid_str].reserveMode == true then
          --[[ Логика резерва. 
          Если Ттн подачи<Ттн min в течении времени Тзадержки, основной отключить, включить резерв1 и т.д.
          Если Ттн подачи>Ттн min в течении времени Тзадержки, основной вкл, резерв1 откл ]]--
          water_out = th_tab.tsens_arr_c[object_t[uuid_str].sens_water_out_ind]  
          water_in = th_tab.tsens_arr_c[object_t[uuid_str].sens_water_in_ind]
          water_out_min = object_t[uuid_str].minTempReserved
          
          if water_out < water_out_min then
              reserve_switch_status = timer_ton("Reserve_Switch_Timeout_"..tostring(u_id), 1, object_t[uuid_str].reserveDelay)
              main_switch_status = timer_ton("Main_Switch_Timeout_"..tostring(u_id), 0, object_t[uuid_str].reserveDelay)
          else
              reserve_switch_status = timer_ton("Reserve_Switch_Timeout_"..tostring(u_id), 0, object_t[uuid_str].reserveDelay)
              main_switch_status = timer_ton("Main_Switch_Timeout_"..tostring(u_id), 1, object_t[uuid_str].reserveDelay)
          end

          if reserve_switch_status == 1 then
              is_current_boiler_main = false
          end
          
          if main_switch_status == 1 then
              is_current_boiler_main = true
          end

          res = ( (is_current_boiler_main == true) and (isSourceMain == true) ) or
           ( (is_current_boiler_main == false) and (isSourceMain == false) )
      elseif object_t[uuid_str].cascadeMode == true then
          -- Now we have cascade number. Let assign proper boiler to duty.
          -- cascade table: cascade_t 
          for i=1, cascade_number do
              local cas_ind = ((cascade_t_ind + i-2) % cascade_max ) + 1
              if cascade_t[cas_ind] == u_id then
                  res = true
              end         
          end 
      else -- Simple mode below
          res = boiler_requests
      end -- if object_t[uuid_str].reserveMode == true then
  end -- if boiler_requests == true then...
  return res 
end
----------------------------------------------------------------
-------------------------------------------------------------------------------
local function get_schedule_channel_c(uuid)
--[[#S2:<INDEX> - INDEX:
 if 1...7 - недельное расписание целой части температуры. Температура в Цельсии, две цифры на час. Всего 24 часа по две цифры  - 48 цифр;
if 9...15 - недельное расписание дробной части, одна цифра на час. Всего 24 часа по одной цифре - 24 цифры.
tcalend_fract_1=T000000000000000000000000
tcalend_fract_2=T000000000000000000000000
tcalend_fract_3=T000000000000000000000000
tcalend_fract_4=T000000000000000000000000
tcalend_fract_5=T000000000000000000000000
tcalend_fract_6=T000000000000000000000000
tcalend_fract_7=T000000000000000000000000
tcalend_int_1=T202020202020202020202020202020202020202020202020
tcalend_int_2=T202020202020202020202020202020202020202020202020
tcalend_int_3=T202020202020202020202020202020202020202020202020
tcalend_int_4=T202020202020202020202020202020202020202020202020
tcalend_int_5=T202020202020202020202020202020202020202020202020
tcalend_int_6=T202020202020202020202020202020202020202020202020
tcalend_int_7=T202020202020202020202020202020202020202020202020
--]]
  local weekday = os.date("%w") -- 1...7
  local hour = os.date("%H") -- 0...23
  
  -- os.date from Raspberry Pi gets time with GMT+0 timezone, so
  -- Let take into consideration TimeZone utility.param.term_control.timezone
  local timezone = utility.param.term_control.timezone
  if (hour + timezone) > 23 then
    if weekday == 7 then
      weekday = 1
    else
      weekday = weekday + 1  
    end
  end
  hour = (hour + timezone) % 24

  local t_int = utility.param['uuid']["tcalend_int_"..tostring(weekday)]
  local t_fract = utility.param['uuid']["tcalend_fract_"..tostring(weekday)]

  local temp = t_int:sub(2*hour+2, 2*hour+2+1) + (t_fract:sub(hour+2, hour+2))/10
  return tonumber(temp)
end  
-------------------------------------------------------------------------------
-- local tset_dict = {}
local function calc_tset_c(uuid, mode)
  -- let store Diction with temperatures set.  First time we put actual value here. Next time we read this value only shortly.
  res = nil
  if uuid ~= nil then
    if (utility.param[uuid] ~= nil) and (mode ~= nil) then
    --utility.param[uuid].tset_comf
        if mode == 0 then -- turn off
          res = utility.param[uuid].tset_off
        elseif mode == 1 then -- comfort
          res = utility.param[uuid].tset_comf
        elseif mode == 2 then -- econom 
          res = utility.param[uuid].tset_eco
        elseif mode == 3 then -- schedule
          res = get_schedule_channel_c(uuid)
        else
          -- it's not possible to be here!!
        end
    end
  end
  
  -- convert Kelvin 0.1 degree to Celsium float
  res = (res-2730)/10
  return res
end

----------------------------------------------------------------
local function  warmFloorBlock(warmFloorBlockChannel, controller_t, boiler_requests)
  if warmFloorBlockChannel ~= nil and warmFloorBlockChannel > 0 then
      if controller_t[warmFloorBlockChannel] ~= nil then
        if controller_t[warmFloorBlockChannel].request == false then
            boiler_requests = false
        end
      end
  end
  return boiler_requests
end
----------------------------------------------------------------
function table_controller(t)
  -- Source's parameters:
  local mainSourceRelay, inversionSource, AnalogAsRelaySource, PZA, fixed_flag
  local byBoiler, requestAct, stillAct, mainSensorSerSource
  local resSensorSerSource, hysteresisSource, CircPumpSource, PumpTimeBefore
  local PumpTimeAfter, axilRelaySource, AnalogAsRelaySourcePump, isSourceMain, warmFloorBlockChannel
  local KpropSource, KintegrSource, pumpOTDelay, isOT
      
  -- Consume's parameters:
  local mainSensorSerDestin, mainSensorNameDestin, mainRelayDestin, CircusPumpDestin, axilRelayDestin
  local KintegrDesin, hysterDestin, inversionDestin, resSensorSerDestin, KpropDestin,PumpTimeBeforeDestin
  local gvs, TreePointMode, PumpTimeAfterDestin, analogAsRelayDestin, threeWayConsumeRelay
  local resSensorNameDestin, pwm_period, pwm_fulltime, koeff_s_c, MainTimeAfterDestin, pwm_fulltime_ratio
  local u_id
  local state, circ_status,  delta_c, channelConsumeNumber
  
  local boiler_requests = false
  local boiler_is_requsted
  local boiler_status_on = 0
  local analog_stat, sensor_main_ind, sensor_res_ind
  local object_uuid
  local tset_c
  local uuid
  local mode
  local boiler_status_on_gvs
  local tset_c_refConsumePZA = 20 -- nominal value, may be changed below
  local pzaSensorSerial, sensor_outside_temp
  
  for key,value in pairs(t['objects_channels']) do

    object_uuid = value['uuid']

    if utility.param[object_uuid] ~= nil then
      mode = utility.param[object_uuid].mode
    else
      break
    end
    if (object_t[object_uuid] == nil) then
      print("\t\t New Object - uuid= ", object_uuid)
      object_t[object_uuid] = {}

      object_t[object_uuid].minTempReserved = value['minTempReserved']
      object_t[object_uuid].reserveDelay = value['reserveDelay']
      object_t[object_uuid].sens_water_out_ind = sensor_find(value['sensorSerSystemOut'])
      object_t[object_uuid].sens_water_in_ind = sensor_find(value['sensorSerSystemIn'])
      object_t[object_uuid].deltaTempCascadMin = value['deltaTempCascadMin']
      object_t[object_uuid].deltaTempCascadMax = value['deltaTempCascadMax']
      object_t[object_uuid].delayCascad = value['delayCascad']
      object_t[object_uuid].rotateBoilersPeriod = value['rotateBoilersPeriod']
      object_t[object_uuid].reserveMode = value['radioButtonReserve']
      object_t[object_uuid].cascadeMode = value['radioButtonCascade']
      object_t[object_uuid].referenceConsumeChPZA = value['referenceConsumeChPZA']
      object_t[object_uuid].pzaSensorSerial = value['pzaSensorSerial']
          
      cascade_t = {}
      cascade_max = 0
      
      if object_t[object_uuid].cascadeMode == true then
        for ch_i, ch_s in ipairs(value['channelsSource']) do
            cascade_max = cascade_max + 1
            cascade_t[ch_i] = ch_s['u_id']
        end 
        -- Random needs because after PowerOn first element in the table will started. If PowerOn-Off too quickly then neeed randomize it.
        cascade_t_ind = math.random(cascade_max) --generates integer numbers between lower and upper.
        rotateBoilersPeriod = object_t[object_uuid].rotateBoilersPeriod * 86400 -- 3600*24 seconds in the day
        print ("Cascade: cascade_max=",cascade_max, "cascade_t_ind=", cascade_t_ind, "rotateBoilersPeriod=", rotateBoilersPeriod, object_t[object_uuid].rotateBoilersPeriod)        
      end -- if cascadeMode == true ...
    end -- if (object_t[object_uuid] == nil...
    
    if object_t[object_uuid].cascadeMode == true then
      cascade_switch(object_uuid) -- to calculate how many boilers is needed
    end
    
----------------------------------------------------------------------
------------------------ CONSUME -------------------------------------- 
----------------------------------------------------------------------
-- Let find out boiler request from Consumes
    boiler_requests = false
    for ch_i, ch_c in ipairs(value['channelsConsume']) do
--      print ("channelNameConsume", ch_c['channelNameConsume'])
      -- Note: relay index at table started fro Zero, but at this script started from One 
      mainSensorSerDestin = ch_c['mainSensorSerDestin']
      mainSensorNameDestin = ch_c['mainSensorNameDestin']
      mainRelayDestin = ch_c['mainRelayDestin']-- + 1
      CircusPumpDestin = ch_c['CircusPumpDestin']
      axilRelayDestin = ch_c['axilRelayDestin']-- + 1
      KintegrDesin = ch_c['KintegrDesin']
      hysterDestin = ch_c['hysterDestin']
      inversionDestin = ch_c['inversionDestin']
      resSensorSerDestin = ch_c['resSensorSerDestin']
      KpropDestin = ch_c['KpropDestin']
      PumpTimeBeforeDestin = ch_c['PumpTimeBeforeDestin']
      gvs = ch_c['GVS']
      TreePointMode = ch_c['TreePointMode']
      PumpTimeAfterDestin = ch_c['PumpTimeAfterDestin']
      analogAsRelayDestin = ch_c['analogAsRelayDestin']
      threeWayConsumeRelay = ch_c['threeWayConsumeRelay']-- + 1
      resSensorNameDestin = ch_c['resSensorNameDestin']
      pwm_period = ch_c['pwmPeriod']
      pwm_fulltime = ch_c['pwmFulltime']
      koeff_s_c = ch_c['koeff_s_c']
      warmFloorBlockChannel = ch_c['warmFloorBlockChannel']
      u_id = ch_c['u_id']
      uuid = ch_c['uuid']
      MainTimeAfterDestin = ch_c['MainTimeAfterDestin']
      pwm_fulltime_ratio = ch_c['pwmFulltime_ratio']
 
      tset_c = calc_tset_c(uuid, mode)
	  if u_id == object_t[object_uuid].referenceConsumeChPZA then
	  	tset_c_refConsumePZA = tset_c
	  end

      sensor_main_ind = sensor_mapping(sensor_main_map_tab, u_id, mainSensorSerDestin)   
      sensor_res_ind = sensor_mapping(sensor_res_map_tab, u_id, resSensorSerDestin)

      local sensor_main_temp, sensor_res_temp, sensor_duble_temp
      local isNoSensorID = (sensor_main_ind == nil) and (sensor_res_ind == nil)
      if (sensor_main_ind ~= nil) then 
        sensor_main_temp = th_tab.tsens_arr_c[sensor_main_ind]
      end
      if (sensor_res_ind ~= nil) then 
        sensor_res_temp = th_tab.tsens_arr_c[sensor_res_ind]
      end
      --      sensor_duble_temp -> temperature from main OR reserved sersors 
      if sensor_main_temp ~= nil then
          sensor_duble_temp = sensor_main_temp
      elseif sensor_res_temp ~= nil then
          sensor_duble_temp = sensor_res_temp
      end
      local isNoSensorData = (sensor_duble_temp == nil)
      -- let check that we have any sensors data:
      if isNoSensorData or isNoSensorID then 
        -- it's wrong scenario - possibly sensor serial numbers are wrong
        -- let skip all processing
      else

        if gvs and api.antilegionella_session_on then -- antilegionella processing
          tset_c = config.antilegionella_temperature -- re-write temperature set
        end
        
        if (controller_t[u_id] == nil) then
          print("\t\t Consume's Channel new - u_id= ", u_id)
          controller_t[u_id] = {}
          controller_t[u_id].sm_state = "IDLE"
          controller_t[u_id].pwm_timeout = 0
          controller_t[u_id].request = 0
          object_t[object_uuid][u_id] = {}
        end
  
  --------------------------------------        
        if (TreePointMode == false) then
          if (mainRelayDestin <= 6) then -- relay mode
              if (MainTimeAfterDestin == nil) or (MainTimeAfterDestin == 0) then
                boiler_status_on = analog_or_relay_comparator(tset_c, th_tab.tsens_arr_c[sensor_main_ind], 
                    th_tab.tsens_arr_c[sensor_res_ind], hysterDestin, mainRelayDestin, inversionDestin, false)      
              else
                boiler_status_on = analog_or_relay_comparator_with_delay(tset_c, th_tab.tsens_arr_c[sensor_main_ind], 
                    th_tab.tsens_arr_c[sensor_res_ind], hysterDestin, mainRelayDestin, inversionDestin, false, u_id, MainTimeAfterDestin)      
              end
  --[[для канала ТП надо создать дополнительный флаг - "блокировка каналом N". И далее 
    канал N - канал радиаторов, если Ткомн>Тцели(комн), то не только не будет запрашивать тепло для себя, для радиаторов, 
    но и будет блокировать запрос ТН канала ТП.              ]]--
              if boiler_status_on == 1 then 
                  boiler_requests = true  
                  controller_t[u_id].request = true
              else
                  controller_t[u_id].request = false
              end
              boiler_requests = warmFloorBlock(warmFloorBlockChannel, controller_t, boiler_requests)              

              if CircusPumpDestin == true then
                circ_status = timer_toff("CircusPumpOffDelay_"..tostring(u_id), boiler_status_on, PumpTimeAfterDestin)
                set_relay(axilRelayDestin, circ_status )
              end
  --------------------------------------        
          elseif (mainRelayDestin > 6 and analogAsRelayDestin == true) then -- analog as relay mode
              if (MainTimeAfterDestin == nil) or (MainTimeAfterDestin == 0) then
                boiler_status_on = analog_or_relay_comparator(tset_c, th_tab.tsens_arr_c[sensor_main_ind], 
                    th_tab.tsens_arr_c[sensor_res_ind], hysterDestin, mainRelayDestin, inversionDestin, true)      
              else
                boiler_status_on = analog_or_relay_comparator_with_delay(tset_c, th_tab.tsens_arr_c[sensor_main_ind], 
                    th_tab.tsens_arr_c[sensor_res_ind], hysterDestin, mainRelayDestin, inversionDestin, true, u_id, MainTimeAfterDestin)      
              end
              
              if boiler_status_on == 1 then 
                  boiler_requests = true  
                  controller_t[u_id].request = true
              else
                  controller_t[u_id].request = false
              end

              boiler_requests = warmFloorBlock(warmFloorBlockChannel, controller_t, boiler_requests)              
  
              if CircusPumpDestin == true then
                  circ_status = timer_toff("CircusPumpOffDelay_"..tostring(u_id), boiler_status_on, PumpTimeAfterDestin)
                  set_relay(axilRelayDestin, circ_status )
              end -- circus pump processing
  ----------------------------------------------------------------------------        
          elseif (mainRelayDestin > 6 and analogAsRelayDestin == false) then -- analog PI mode
  
            local analog_out = pi_control(tset_c, th_tab.tsens_arr_c[sensor_main_ind], th_tab.tsens_arr_c[sensor_res_ind], mode, KpropDestin, KintegrDesin, 5000)
            set_analog_output(mainRelayDestin-6, analog_out)

  --          if analog_out > 0 then boiler_requests = true  end -- I'm not sure that analog servo my requesting boiler
            boiler_requests = true              
            boiler_requests = warmFloorBlock(warmFloorBlockChannel, controller_t, boiler_requests)              
              
            -- ADD CIRCUS PUMP Code here !!!!!!!!                    
  ----------------------------------------------------------------------------        
          else -- wrong mode, but it's impossible...
            print("wrong mode, but it's impossible... -2")
          end -- ... mode
  ----------------------------------------------------------------------------        
        else -- 3-Way Servo Motor
          delta_c = sensor_duble_temp - tset_c
          if delta_c < 0 then 
            boiler_requests = true              
          end
          boiler_requests = warmFloorBlock(warmFloorBlockChannel, controller_t, boiler_requests)              

          state = controller_t[u_id].sm_state
          if timer_period("3_Way_Cntrl_"..tostring(u_id), pwm_period, false) == true then
            -- new PWM window started
            controller_t[u_id].sm_state = "START"
            state = "START"
  
            if math.abs(delta_c) > hysterDestin then
              controller_t[u_id].pwm_timeout = math.abs(koeff_s_c * delta_c * pwm_fulltime_ratio)
              if controller_t[u_id].pwm_timeout >= pwm_period then
                controller_t[u_id].pwm_timeout = pwm_period
              end
            end
            -- relay_way setup           
            if delta_c > 0 then
              if(inversionDestin == false) then
                set_relay(threeWayConsumeRelay, 1)
              else
                set_relay(threeWayConsumeRelay, 0)
              end
            else
              if(inversionDestin == false) then
                set_relay(threeWayConsumeRelay, 0)
              else
                set_relay(threeWayConsumeRelay, 1)
              end
            end            
          else -- not new period
            if state == "IDLE" then -- do nothing
  
            elseif state == "START" then 
              state = "PULSE" 
              timer_toff("PWM_"..tostring(u_id), 1, controller_t[u_id].pwm_timeout)
              -- relay turn on
              relay_on_off(1, mainRelayDestin, analogAsRelayDestin, false)
            elseif state == "PULSE" then 
              if timer_toff("PWM_"..tostring(u_id), 0, controller_t[u_id].pwm_timeout) == 0 then
                state = "FINISH"
                -- relay turn off
                relay_on_off(0, mainRelayDestin, analogAsRelayDestin, false)
              end -- if timer_toff
            elseif state == "FINISH" then state = "IDLE"
            else -- dumb option
            end -- if state == ...
          end -- if timer_period(...
          controller_t[u_id].sm_state = state
        end --  if (TreePointMode == false
  -------------      
        if gvs == false then -- clear on state for all none-gvs channels
            if boiler_status_on_gvs == true then  boiler_status_on = 0  end
        else  -- for gvs channel
            if boiler_status_on == 1 then 
                boiler_status_on_gvs =  true 
            else 
                boiler_status_on_gvs = false 
          end
        end

      end --if (sensor_main_ind == nil) and (sensor_res_ind == nil) both sensor are nil - skip processing
-------------      
    end -- Consume's Channels iterator
    ----------------------------------------------------------------------------    

----------------------------------------------------------------------
------------------------ SOURCE -------------------------------------- 
----------------------------------------------------------------------
    for ch_i, ch_s in ipairs(value['channelsSource']) do
--      print ("channelNameSource", ch_s['channelNameSource'])
      -- Note: relay index at table started fro Zero, but at this script started from One 
      mainSourceRelay = ch_s['mainRelaySource']-- + 1
      inversionSource = ch_s['inversionSource']
      AnalogAsRelaySource = ch_s['AnalogAsRelaySource'] 
      PZA = ch_s['PZA']
      fixed_flag = ch_s['fixed']
      byBoiler = ch_s['byBoiler']
      requestAct = ch_s['requestAct']
      stillAct = ch_s['stillAct']
      mainSensorSerSource = ch_s['mainSensorSerSource']
      resSensorSerSource = ch_s['resSensorSerSource']
      hysteresisSource = ch_s['hysteresisSource']
      CircPumpSource = ch_s['CircPumpSource']
      PumpTimeAfter = ch_s['pumpTimeAfterSource']
      axilRelaySource = ch_s['axilRelay']-- + 1
      AnalogAsRelaySourcePump = ch_s['AnalogAsRelaySourcePump']
      isSourceMain = ch_s['isSourceMain']
      KpropSource = ch_s['KpropSourceOT'] -- Note: it's not for OT, it's for 0/10V controler. "OT" is stale name, sorry
      KintegrSource = ch_s['KintegrSourceOT'] -- Note: it's not for OT, it's for 0/10V controler. "OT" is stale name, sorry
      pumpOTDelay = ch_s['pumpOTDelay']
      isOT = ch_s['isOT']
      uuid = ch_s['uuid']
      
      u_id = ch_s['u_id']

      -- it's not rom setup. It's boiler's water setup default. Just for case is something wrong. Let be the same for OT and non-OT
      tset_c = config.ot_fault_setpoint  
      
      if (fixed_flag == true) then
		-- let takes Twater-setup from Web
		tset_c = calc_tset_c(uuid, mode)
      end 

      if (byBoiler == true) then end -- do nothing

	  -- PZA Note: tset_c here =20. One of Consume's Channel is PZA reference channel. 
	  if (PZA == true) then  
          local sensor_outside_ind = sensor_mapping(sensor_outside_map_tab, object_uuid, object_t[object_uuid].pzaSensorSerial)   
      	  if (sensor_outside_ind ~= nil) then 
          	sensor_outside_temp = th_tab.tsens_arr_c[sensor_outside_ind]
      	  end
		  tset_c = pza_calculator(sensor_outside_temp, utility.param.pza.curve_num, tset_c_refConsumePZA)
--print(">>>-1", tset_c, sensor_outside_temp, utility.param.pza.curve_num, tset_c_refConsumePZA)		  
	  end -- most of deal do at ot.lua

      if isOT == true then 
          -- add OpenTherm code here. ot_control_algorithm() is called from k_event.lua but it need setpoint and sensor info

		  -- NOTE: options below can not be simultaneouslly. So we check them side by side:
		  
		  -- boiler_requests >> need take into consideration since 2016-09-13. If none Consume requsted let boiler turn off 
		  
          -- since 2016-09-14, SVN>=202, Lua>=10.00
          -- OT-Fixed code changed. Now Fixed means Twater-setup takes from Web, nor from Utility.
          
	  ot_setup(tset_c, pumpOTDelay, boiler_requests)

	  ----------------	      
      else -- non-OT below
	      sensor_main_ind = sensor_mapping(sensor_main_map_tab, u_id, mainSensorSerSource)   
	      sensor_res_ind = sensor_mapping(sensor_res_map_tab, u_id, resSensorSerSource)

	      local sensor_main_temp, sensor_res_temp, sensor_duble_temp
	      local isNoSensorID = (sensor_main_ind == nil) and (sensor_res_ind == nil)
	      if (sensor_main_ind ~= nil) then 
	        sensor_main_temp = th_tab.tsens_arr_c[sensor_main_ind]
	      end
	      if (sensor_res_ind ~= nil) then 
	        sensor_res_temp = th_tab.tsens_arr_c[sensor_res_ind]
	      end
	      --      sensor_duble_temp -> temperature from main OR reserved sersors 
	      if sensor_main_temp ~= nil then
	          sensor_duble_temp = sensor_main_temp
	      elseif sensor_res_temp ~= nil then
	          sensor_duble_temp = sensor_res_temp
	      end
	      local isNoSensorData = (sensor_duble_temp == nil)
	      -- let check that we have any sensors data:
	      if isNoSensorData or isNoSensorID then 
	        -- it's wrong scenario - possibly sensor serial numbers are wrong
	        -- let skip all processing
	      else
--	        tset_c = calc_tset_c(uuid, mode) -- fixed at Version SVN-228, Lua 12.05. This is calculated above
	  --[[ -----------------------------------
	        if (controller_t[u_id] == nil) then
	          print("\t\t Source's Channel new - u_id= ", u_id)
	          controller_t[u_id] = {}
	          controller_t[u_id].request = false
	        end  
	  ------------------------------ ]]--        th_tab.tset_c
			if g_log_level >= 1 then  -- low level
				print("requestAct", requestAct, tset_c, sensor_duble_temp)
			end		  
	        if(requestAct == true) then
	            -- we have request, but options: 
	            -- 1) one boiler, let request processing
	            -- 2) Reserved boilers, let deside what boiler on, what boiler off
	            -- 3) Cascade boilers, let deside about boilers
	            
	            --Input - request (true/false); Output - request for specific Source Channel
	  
	            boiler_is_requsted = boiler_arbiter(object_uuid, boiler_requests, u_id, isSourceMain)
				
	            if (boiler_is_requsted == true) then -- Let control
	  
	                if (fixed_flag == true) or (PZA == true) then
	                    --------------------------------------        
	                    if (mainSourceRelay <= 6) then -- relay mode
	                        boiler_status_on = analog_or_relay_comparator_with_delay(tset_c, th_tab.tsens_arr_c[sensor_main_ind], 
	                            th_tab.tsens_arr_c[sensor_res_ind], hysteresisSource, mainSourceRelay, inversionSource, false, u_id, 0)
	                        if CircPumpSource == true then
	                          circ_status = timer_toff("CircusPumpOffDelay_Source_"..tostring(u_id), boiler_status_on, PumpTimeAfter)
	                          relay_on_off(circ_status, axilRelaySource, AnalogAsRelaySourcePump, false) --  
	                        end
	                    --------------------------------------        
	                    elseif (mainSourceRelay > 6 and AnalogAsRelaySource == true) then -- analog as relay mode
	                        boiler_status_on = analog_or_relay_comparator_with_delay(tset_c, th_tab.tsens_arr_c[sensor_main_ind], 
	                            th_tab.tsens_arr_c[sensor_res_ind], hysteresisSource, mainSourceRelay, inversionSource, true, u_id, 0)
	                        if CircPumpSource == true then
	                            circ_status = timer_toff("CircusPumpOffDelay_Source_"..tostring(u_id), boiler_status_on, PumpTimeAfter)
	                            relay_on_off(circ_status, axilRelaySource, AnalogAsRelaySourcePump, false) --  
	                        end -- circus pump processing
	                    --------------------------------------        
	                    elseif (mainSourceRelay > 6 and AnalogAsRelaySource == false) then -- analog 0/10V mode
				            local analog_out = pi_control(tset_c, th_tab.tsens_arr_c[sensor_main_ind], th_tab.tsens_arr_c[sensor_res_ind], mode, KpropSource, KintegrSource, 5000)
							if g_log_level >= 1 then  -- low level
								print("analog_out", mainSourceRelay, analog_out)
							end		
	    	    		    set_analog_output(mainSourceRelay-6, analog_out)
	                    --------------------------------------        
	                    else -- wrong mode, but it's impossible...
	                      print("wrong mode, but it's impossible... -1")
	                    end -- ... mode: relay / analogAsRelay
	                --------------------------------------        
	                elseif(byBoiler == true) then
	                    relay_on_off(1, mainSourceRelay, AnalogAsRelaySource, inversionSource) -- Boiler Turn On 
	                else -- wrong case
	                        print("wrong mode, but it's impossible... -3")
	                end -- if(fixed_flag == true...
	            else --if (boiler_requests == false -> if nothing to do then let turn off Boiler. Take "inersion" into consideration !!
	                relay_on_off(0, mainSourceRelay, AnalogAsRelaySource, inversionSource) -- Boiler Turn Off 
	            end -- if (boiler_reques == true) 
	        else -- if (requestAct is false, then let turn on boiler contantly
	            relay_on_off(1, mainSourceRelay, AnalogAsRelaySource, inversionSource) -- Boiler Turn On 
	        end  -- if (requestAct == true
	        
	      end --if (sensor_main_ind == nil) and (sensor_res_ind == nil) both sensor are nil - skip processing
      end -- if isOT....
    end -- for ch_i, ch_s in ipairs(value['channelsSource']) do
    ----------------------------------------------------------------------------    

  end -- objects iterator
end 



-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
return table_controler
