local usr =  {}

--[[
 Этот файл содержит код пользователя. Только этот файл пользователь может менять по своему усмотрению.
Желательно, чтобы пользователь использовал только вызовы, описанные в файле api.lua.
]]--
utility = require("lua/utility")
get = require("lua/get")
api = require("lua/api")
table_controler = require("lua/table_controler")
config = require("lua/config")

tick_counter = 0 

-------------------------------------------------------------------------------
function usr.tick()
--[[ запускается, если второй аргумент бинарного файла Z710RPI отсутствует или равен 0 
Это главная функция, которая вызывается один раз в ХХ секунд (при отладке 2 секунды, потом уточним)
Перед ее вызовом обновляются данные температурных датчиков и данные об ошибках.]]--
  
  rtc_setup()
  if g_log_level >= 1 then  
--    local rtc = call_c_from_lua_rtc_seconds()
--    print ("RTC", rtc)
  end

--  internet_check_and_restart(config.internet_check_timeout, 0) -- staled function, remove it
  
  if g_log_level >= 3 then  -- maximum level
--    print("Infinite Counters ", infinite_counter(1), infinite_counter(2), infinite_counter(3))
  end
  
  if g_log_level >= 3 then  -- maximum level
--    print("----- Internet Status -----------: ", internet_status())
  end
  
--[[ 
  --time performance test - about 0.45sec for 2000 passes
  local elapsed_time =  os.clock()
  print(string.format(">>>>>>>>>>> time before: %.2f\n", os.clock()))
    for i=1,1000 do 
      table_controller(usr.channel_tab)  
    end
  print(string.format("<<<<<<<<<<<< time after: %.2f\n", os.clock()))
  print(string.format("<<<< elapsed time : %.2f\n", os.clock()-elapsed_time))
]]--

  table_controller(usr.channel_tab) --   
  events_controller(usr.events_tab) --
  
  tick_counter = tick_counter + 1
  
  if g_log_level >= 2 then  -- test level
--    print("<><> tick counter=", tick_counter)
  end
  
  --[[ отправка события на сервер 
EVENT_ID_MAIN_POWER_FAULT   10 - пропадание питания (sensor_num и msg_string не требуются)
EVENT_ID_MAIN_POWER_OK      11 - появление питания (sensor_num и msg_string не требуются)
EVENT_ID_SENSOR_ALARM_HIGH  71 - порог максимальной температуры датчика sensor_num
EVENT_ID_SENSOR_ALARM_LOW   72 - порог минимальной температуры датчика sensor_num
EVENT_ID_SENSOR_ALARM_FAULT 73 - датчик sensor_num неисправен
EVENT_ID_FAULT_START        74 (sensor_num и msg_string не требуются)
EVENT_ID_FAULT_END          75 (sensor_num и msg_string не требуются)
  ]]--
  
end
-------------------------------------------------------------------------------
function usr.factory_test_1()
  -- запускается, если второй агумент бинарного файла Z710RPI равен 1
  print("test-1")
  --[[  Программа тестирования выполняет следующее:
Периодически включает все 6 реле, их проверить надо мультиметром;
Периодически меняет состояние аналоговых выходов в диапазоне 0...10В, их надо проверить мультиметром;
Отображается температура датчика DS1820;
Отображает состояние трех дискретных входов;
Отображает состояние трех аналоговых входов;
Отображает состояние трех счетчиков импульсов на дискретных входах;
Периодически передает в порты K-Line и RS485 тестовые данные для их проверки и отображает принятые данные 
  Период равен 3 секунды   --]]
  local odd_tick = tick_counter %2 -- =0,1,0,1,0,1,..
  local even_tick = (tick_counter + 1) %2 -- =1,0,1,0,1,..
  local relay_index_1 = (tick_counter % 6) + 1        -- =1,2,3,4,5,6,1,2,3,...
  local relay_index_2 = ((tick_counter + 3) % 6) + 1  -- =4,5,6,1,2,3,4,5,6,...
  local inp_index = (tick_counter%3) + 1 -- =1,2,3,1,2,3,..
  local relay_index = (tick_counter % 6) + 1        -- =1,2,3,4,5,6,1,2,3,...
  
--  print("relay_index_1", relay_index_1, "odd_tick", odd_tick)
--  print("relay_index_1", relay_index_1, "even_tick", even_tick)
--  set_relay(relay_index_1, odd_tick)
--  set_relay(relay_index_2, even_tick)

  local r = get_relay(relay_index)
  if(r == 0)then
    set_relay(relay_index, 1)
  else
    set_relay(relay_index, 0)
  end
  print("relays", relay_index, th_tab.relay_status[1],th_tab.relay_status[2],th_tab.relay_status[3],th_tab.relay_status[4],th_tab.relay_status[5],th_tab.relay_status[6])
  
  local analog_out = 2000*(tick_counter %6)
  set_analog_output(1, analog_out) -- sets 0V, 2V, 4V, 6V, 8V, 10V, 0V, .. 
  set_analog_output(2, analog_out) -- sets 0V, 2V, 4V, 6V, 8V, 10V, 0V, .. 
  set_analog_output(3, analog_out) -- sets 0V, 2V, 4V, 6V, 8V, 10V, 0V, .. 
  set_analog_output(4, analog_out) -- sets 0V, 2V, 4V, 6V, 8V, 10V, 0V, .. 
  set_analog_output(5, analog_out) -- sets 0V, 2V, 4V, 6V, 8V, 10V, 0V, .. 
  set_analog_output(6, analog_out) -- sets 0V, 2V, 4V, 6V, 8V, 10V, 0V, .. 
  
  local digit_inp = get_digit_input(inp_index)
  local analog_inp = get_analog_input(inp_index)
  local counter = get_counter(inp_index) 
  local temperature = th_tab.tsens_arr_c[1]
  local rtc = call_c_from_lua_rtc_seconds()
  print("--------------CNTR-----INP3---DIGIT---ANALOG-COUNTER---0/10V------RTC----TEMPERATURE")
  print("-------TICK: ", tick_counter, inp_index, digit_inp, analog_inp, counter, analog_out, rtc, temperature)
  
  tick_counter = tick_counter + 1

  send_byte_tx1(tick_counter % 256)
  send_byte_tx2(tick_counter % 256)
--  wrapped2textpacket_unittest()
  
end
-------------------------------------------------------------------------------
usr.events_tab = 
--events_tab_start_line - do not change this line!
{
	events_table = {}
}--events_tab_end_line - do not change this line!

-------------------------------------------------------------------------------
usr.channel_tab = 
-- channel_tab_start_line - do not change this line!
{
	scriptPeriod = 3,
	objects_channels = {
			{
			deltaTempCascadMin = 20,
			sensorSerSystemIn = "",
			uuid = "24be8d3e-6d09-4f0a-9426-9487b1492af4",
			objectName = "new",
			reserveDelay = 240,
			sensorSerSystemOut = "",
			radioButtonReserve = false,
			rotateBoilersPeriod = 1,
			deltaTempCascadMax = 30,
			referenceConsumeChPZA = 0,
			channelsSource = {},
			pzaSensorSerial = "10-000802FE3E4C",
			delayCascad = 3600,
			channelsConsume = {},
			radioButtonCascade = false,
			minTempReserved = 3
		}
	}
}--channel_tab_end_line - do not change this line!
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- UNIT TEST ->>> comment it for release !!!!!!!!!!!
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
return usr--events_tab_start_line - do not change this line!--events_tab_start_line - do not change this line!
