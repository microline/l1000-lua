local utility =  {}
lip = require("lua/LIP")
local param_backup = require ("lua/param_backup")

-- Globals
param = {}


isRTCSetup = false
-------------------------------------------------------------------------------
function utility.get_unixtime()
	-- Output - 4 bytes table {low_byte, 2nd_byte, 3rd_byte, high_byte}
	local unixtime = os.time() -- "!t" -> UTC time;  "*t"-> local time
	--bit32.extract (n, field [, width])
	--Returns the unsigned number formed by the bits field to field + width - 1 from n. 
	--Bits are numbered from 0 (least significant) to 31 (most significant). 
	--All accessed bits must be in the range [0, 31].
	local ret1 = bit32.extract(unixtime, 0, 8)
	local ret2 = bit32.extract(unixtime, 8, 8)
	local ret3 = bit32.extract(unixtime, 16, 8)
	local ret4 = bit32.extract(unixtime, 24, 8)
	--print(unixtime)
	--utility.print_r ( ret )
	return string.char(ret1, ret2, ret3, ret4)
end
-------------------------------------------------------------------------------
function utility.get_date()
--[[
  struct tm* p_tm_date = gmtime(&timer);
  date_6bytes[0] = p_tm_date->tm_year-100;
  date_6bytes[1] = p_tm_date->tm_mon+1;
  date_6bytes[2] = p_tm_date->tm_mday;
  date_6bytes[3] = p_tm_date->tm_hour;
  date_6bytes[4] = p_tm_date->tm_min;
  date_6bytes[5] = p_tm_date->tm_sec;
]]--
  local temp = os.date("!*t", os.time()) -- "!t" -> UTC time;  "*t"-> local time
  --print(temp.year-2000,temp.month, temp.day, temp.hour-3, temp.min, temp.sec)
  local year = temp.year-2000
  if year < 0 then year = 0 end -- it may happened when date is wrong, may be 1970 Unix Age 
  return string.char(year,temp.month, temp.day, temp.hour, temp.min, temp.sec)
end
-------------------------------------------------------------------------------
--http://stackoverflow.com/questions/5303174/get-list-of-directory-in-a-lua
-- The main goal of this is find out all files like "param.XXX.ini 
-- Those are backup files. Need find out most highest XXX number and then backup param.ini file
-- as param.YYY.ini where YYY=XXX+1
function dirLookup_param_backup(dir) 
   --Open directory look for files, save data in p. 
   -- By giving '-type f' as parameter, it returns all files.
--   local p = io.popen('find "'..dir..'" -type f')
   local backup_file_name
   local start_i, end_i
   local max_ind = 0       
   local current_ind
   local p = io.popen('ls "'..dir..'"')
          
   for file in p:lines() do                         --Loop through all files
       start_i, end_i = file:find("param%p%d+%pini")
       if(start_i ~= nil)then
--         print("search param.xxx.ini", start_i, end_i, file, file:sub(start_i+6,end_i-4) ) 
         current_ind = tonumber(file:sub(start_i+6,end_i-4))
         if(current_ind > max_ind)then max_ind = current_ind end
       end
   end
   backup_file_name = "param/param." .. tostring(max_ind+1) .. ".ini"
   print("New Backup File param.xxx:  ", backup_file_name)
   local p = io.popen('cp -f '.. "param/param.ini ".. backup_file_name)
   -- This is very important line - popen() working in another tread and may not finished in time
   -- so this line wait for popen() goes to finish.
   -- may be the better to re-write this code w/o popen and use read file -> write file for backup coping.       
   p:read()  
   
   return max_ind
end
-------------------------------------------------------------------------------
function utility.update_param()
-- if some goes wrong then param.ini may be lost. So need backup. 
-- Need to rename every backup file with incremented number

--  dirLookup_param_backup("./param")

  lip.save("param/param.ini", utility.param)
  save_withmd5()
  
end
-------------------------------------------------------------------------------
function utility.hex_dump (str) -- https://gist.github.com/Elemecca/6361899
    local len = string.len( str )
    local dump = ""
    local hex = ""
    local asc = ""
    
    for i = 1, len do
        if 1 == i % 8 then
            dump = dump .. hex .. asc .. "\n"
            hex = string.format( "%04x: ", i - 1 )
            asc = ""
        end
        
        local ord = string.byte( str, i )
        hex = hex .. string.format( "%02x ", ord )
        if ord >= 32 and ord <= 126 then
            asc = asc .. string.char( ord )
        else
            asc = asc .. "."
        end
    end

    
    return dump .. hex
            .. string.rep( "   ", 8 - len % 8 ) .. asc
end
-------------------------------------------------------------------------------
-- https://coronalabs.com/blog/2014/09/02/tutorial-printing-table-contents/
-- Table Dump Utility
function utility.print_r ( t )  
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end
-------------------------------------------------------------------------------
function toBits(num,bits)
    -- returns a table of bits, most significant first.
    bits = bits or select(2,math.frexp(num))
    local t={} -- will contain the bits        
    for b=bits,1,-1 do
        t[b]=math.fmod(num,2)
        num=(num-t[b])/2
    end
    
    for i , data in ipairs(t) do
      io.write(data) 
    end
    print ()
    return t
end
-------------------------------------------------------------------------------
function utility.sign(x)
  return (x<0 and -1) or 1
end
-------------------------------------------------------------------------------
--[[ RTC feature
 * Let check "date", if it's about 1970 then we have had hardreset and need
 * to pickup RTC shift from STM32.
 * Let compare with "date +%s" -> Epoch time = 955709341 => Fri, 14 Apr 2000 10:49:01 GMT
 * If "date +%s" < 955709341 then device after hardreset.
 * In this case we use RTC shift and set as proper date
 * Shift catch in following condition:
 * If "date +%s" > 955709341 and rtc_shift = "date +%s" - rtc
 * it means that rtc_shift is Epoch time in the moment when RTC turn on
 * Let re-write rtc_shift at every boot, it help to eliminate RTC drift.
 * If "date +%s" < 955709341 then date = rtc_shift + rtc
 * Let processing all at Lua. C code push RTC current value only.
 
 isRTCSetup flag sets to false in initialization. Then it sets to true for one-time RTC setup
 */]]--
function rtc_setup()
  if isRTCSetup == true then return end
  if utility.param.rtc == nil then return end 

  local rtc = call_c_from_lua_rtc_seconds()
  if rtc == 0 then 
    return
  else 
    local unix = os.time()
    local rtc_shift = utility.param.rtc.rtc_shift
    if g_log_level >= 1 then  
      print (">>>RTC",rtc, "UNIX", unix, "rtc_shift", rtc_shift)
    end
    if rtc == 0 then
      return -- 
    end  
    if unix < 955709341 then -- about 1970, no NTP service
        unix = rtc + rtc_shift
        print ("RTC Shift applied new epoch=", unix, "rtc=", rtc)
        local bash_string = "date +%s -s @"..tostring(unix)
        os.execute(bash_string)
    else -- normal time with NTP. So let rewrite rtc_shift
        utility.param.rtc.rtc_shift = unix - rtc
        utility.update_param()
    end
    
    isRTCSetup = true
  end -- waing for proper rtc value
  
  
end
-------------------------------------------------------------------------------
function utility.test1()
  print ("_______________ test1() _____________________")
  local cascade_t = {}
  cascade_t[1] = 101
  cascade_t[2] = 102
  cascade_t[3] = 103
  
  local cascade_number = 1
  local cascade_max = 3
  local u_id
  local cascade_t_ind = 1
  local res = false
  utility.print_r ( cascade_t )  
  
  for j = 1, 3 do
      u_id = j+100
      res = false    
      for i=1, cascade_number do
            local cas_ind = ((cascade_t_ind + i-2) % cascade_max ) + 1
            if cascade_t[cas_ind] == u_id then
                  res = true
            end         
      end 
      print(j, i, u_id, cas_ind, cascade_t[cas_ind], res)
  end
  print ("_______________________________________________")
end
-------------------------------------------------------------------------------


-- unit test
-- print(utility.hex_dump("\x25ABC"))


return utility 
