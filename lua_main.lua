
k_event = require('lua/k_event') 
utility = require("lua/utility")
s_com = require("lua/s_com")
get = require("lua/get")
ot_uart = require("lua/ot_uart")
table_controler = require("lua/table_controler")
config = require("lua/config")
--usr = require("lua/usr")
local param_backup = require ("lua/param_backup")

-- GLOBALs:
g_log_level = nil -- see below function call_lua_from_c_test_log_levels() 
g_test_level = nil
g_release_version = "216-12-23   12.07"
g_uptime = {}
-----------
g_uptime.start = os.time()
-- use g_uptime:
--print (os.difftime( os.time(), g_uptime.start) )
-----------
local user_file
local message
-------------------------------------------------------------------------------
user_file, message = loadfile("lua/usr.lua")
if user_file == nil then
    print('***** Error: usr.lua file failed !\n', message)
else
    usr = user_file()
end
call_c_from_lua_uart_setup(config.uart_1_setup, config.uart_2_setup)
-------------------------------------------------------------------------------
---------------------------------------------------------------
-------------------------------------------------------------------------------
-- called every 60 seconds simultaneously with temperature update
function call_lua_from_c_k_event_proc() 
  local out_string = k_event.create_k_event()
  call_c_from_lua_packet(out_string)

  th_tab.f_sens_update()
  th_tab.f_fault_update()
  treshold_controller() --   
  antilegionella_check()
end
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- called every tick 1...60 seconds period
function call_lua_from_c_tick_proc()
  if usr == nil then                                                                                                                                                                                             
    print("****** ERROR: no user code")
  else
	if utility.param.p_setup == nil or utility.param.p_setup.p82_on == nil then
		-- sometimes p_setup is empty. Let going to default setup
		get.th_tab_ini()
		if g_log_level >= 1 then
			print("******* Error -> p_setup is empty. ")
		end
		return
	end
  	
	if utility.param.p_setup.p82_on == 1 then
    	ot_2boiler_next_tx()      
	end
    if (g_test_level == 0) then
      usr.tick()
    elseif (g_test_level == 1) then
      usr.tick()
    else 
      usr.factory_test_1()
    end
  end

end

function call_lua_from_c_s_comm(arg_string, dest_addr, source_addr)
--  print(">>>>><<<<< Lua argument is: ",arg_string)
  local s_comm2server = s_com.parser(arg_string)
--  print("From s_com.parser():", s_comm2server)
  if(s_comm2server ~= nil)then
    call_c_from_lua_s_command(s_comm2server, dest_addr, source_addr)
  end
end

-----------------------------
function call_lua_from_c_ini_param()
  local tick_period

  local res = isMD5ok(PARAM_NAME, PARAM_BACKUP1, PARAM_BACKUP2)
  print("\n\t ***** File param.ini Check Result:", res, "\n")
  if res == "FAILED" then
  	-- param.ini now EMPTY
  	utility.param = lip.load(PARAM_NAME)-- load empty table only
	get.th_tab_ini() -- load factory default values
	utility.update_param() -- save factory defaults at param.ini
	-- now first line has not md5 sum. Let add md5sum firstly
	update_md5_atfirstline(PARAM_NAME) 
	update_backup_check_before(PARAM_NAME, PARAM_BACKUP1, PARAM_BACKUP2)
  end

  utility.param = lip.load("param/param.ini")
  get.th_tab_ini()
  
  table_controler.obj_chann_update()-- add to param.ini objects and channels sections
  utility.update_param()
  print("\n----------------- LUA RELEASE VERSION: ", g_release_version, " -------------------\n")
  
  if usr.channel_tab['scriptPeriod'] ~= nil then
    tick_period = usr.channel_tab['scriptPeriod']
  else 
    tick_period = 60
  end
  
  antilegionella_ini()
  math.randomseed( os.time() )
 
--  utility.test1() 
 
  return tick_period
end

-----------------------------
--[[    /* IF second argument is absent then test_level=0, normal start
     * test_level = 0 - normal start for work
     * test_level = 1 - factory test - the same, as normal mode, but period tick decreased from 60 to 3 seconds.
     * test_level = 2 - factory test  - specific factory test - all input/output at special modes.
     *
     * log_level = 0 - minimum log, errors only.
     * log_level = 1 - errors/warnings
     * log_level = 2 - test level
     * log_level = 3 - maximum level
     */ --]]
function call_lua_from_c_test_log_levels(log_level, test_level)
  -- globals variables:
  g_log_level = log_level
  g_test_level = test_level
end
---------------------------------------------------------------
-- function moved from param_backup.lua due to "require" cross dependency issue
function param_backup_unit_test_1() 
	local fh,err = io.open(PARAM_NAME, "r")
	if err then
		fh, err = io.open(PARAM_NAME, "w")
		fh.close()
	end
	utility.param = lip.load(PARAM_NAME)
	th_tab.f_ini()
	local res = isMD5ok(PARAM_NAME, PARAM_BACKUP1, PARAM_BACKUP2)
	print("\nRESULT-1: ", res)

    local start_time = os.time()
	local fho, err, line
	local file = io.open(PARAM_NAME, "r") -- r read mode and b binary mode
	local content = file:read "*a" -- *a or *all reads the whole file
	local md5_res = sumhexa(lmd5(content))
	print (md5_res)
	
--	local md5_as_hex = md5.sumhexa(content)   -- returns a hex string
--	print (md5_as_hex)
--	os.exit(1)
	file:close()
--	for i=1, 1000000 do
	for i=1, 1000 do
--	md5_as_hex = md5.sumhexa(content)   -- returns a hex string
	md5_res = sumhexa(lmd5(content))
	end
	local end_time = os.time()
	local elapsed_time = os.difftime(end_time,start_time)
	
	print("\nRESULT-2: ", elapsed_time)


    utility.param.antilegionella.aaa=5
	utility.update_param()
    save_withmd5()
end
----------------------------------------------------------------------
---------------------------------------------------------------

-- unit test:
--call_lua_from_c_s_comm("#S2?", 15, 25)
--param_backup_unit_test_1()
--	param_backup_unit_test_1()

---------------------------------------------------------------
---------------------------------------------------------------

