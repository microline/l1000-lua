local k_event =  {}
utility = require("lua/utility")
get = require("lua/get")
ot_uart = require("lua/ot_uart")
ot = require("lua/ot")

-------------------------------------------------------------------------------
-- GLOBALs:
local ot_boiler_status = {}
ot_boiler_status["fault"] = 0
local k_event_unit_test_flag = false
local power_voltage_unit_test = 0
-------------------------------------------------------------------------------
--[[ make %k event for "kotel"
Пакет с данными о времени работы котла (Tset­>0,1гр). (1 раз в минуту)
Len = 13
%k ­ сигнатура (2 байта ‘%’ ‘k’ = 0x25 0x6B)
ymdhms ­ дата время в бинарном формате на каждое поле по 1 байту – 6 байт 
time ­ время работы котла 1 байт (сек)
mode ­ Режим работы 1 байт (0­выкл 1­comfort 2­ эконом 3 расписание)
Tset ­ Установочная температура 2 байт
(абсТ <500 дискрет 1 Кельвин, >=500 дискрет 0.1 Кельвин)
State ­ Состояние (бит 0 – питание, бит 1 – авария; 1­есть, 0­нет)
CRC
--]]

function k_event.create_k_event()
--[[ arguments:
  t_set
  k_mode
  k_time_at_min
  k_stat    
--]]    
  local state
  local datetime = utility.get_date()
  local temp_string
  local ot_n_bytes = 0 -- we do not know how many OT packets are, so let set ZERO and will update below
  local ot_packs_string = ""

--local ts = os.time()
--print(">>>", os.date('%Y-%m-%d %H:%M:%S', ts))

  if utility.param.p_setup.p82_on == 1 then
	  ot_2boiler_ini()
	  ot_boiler_comm_failed = ot_boiler_communication_check()  
	  -- OT data below:
	  -- OT_N		-	Количество следующих далее OT_param (1 байт)
	  -- OT_param1	-	4 байта
	  -- …
	  -- OT_paramN	-	4 байта
	  
	  -- Send only OT packets than has "1" at bit-mask at #P92, see ot_packets_tx table
	  local mask = utility.param.p_setup.p92_mask
	  local cnt = 0
	  local ot_id
	  local v
	--print("create_k_event-1", mask, ot_total_packs_num)  
	
	  while mask ~= 0 do
	  
	  	if cnt >= ot_total_packs_num then break end
	  	cnt = cnt + 1
		if bit32.band(mask, 1) == 1 then  
			ot_id = ot_packets_tx[cnt][2] -- extract ID
			v =  ot_packets_rx[ot_id]
	--print("create_k_event-2", cnt, ot_id, v)  
			if v ~= nil then 
			  	-- ot_packets_rx[ot_id] = {ot_type, ot_byte_h, ot_byte_l} => v[1]=ot_type; v[2]=ot_byte_h; v[3]=ot_byte_l
	--------------
			    if ot_id == 0 then 
	--[[
	LB: Slave status flag8 bit: description [ clear/0, set/1]
	0: fault indication [ no fault, fault ]
	1: CH mode [CH not active, CH active]
	2: DHW mode [ DHW not active, DHW active]
	3: Flame status [ flame off, flame on ]
	4: Cooling status [ cooling mode not active, cooling
	mode active ]
	5: CH2 mode [CH2 not active, CH2 active]
	6: diagnostic indication [no diagnostics, diagnostic event]
	7: reserved
	]]--
					ot_boiler_status["fault"] = bit32.extract (v[3], 0) 
	
					ot_boiler_status["ch"] =  bit32.extract (v[3], 1) 
					ot_boiler_status["dhw"] =  bit32.extract (v[3], 2) 
					ot_boiler_status["flame"] =  bit32.extract (v[3], 3)
			    end
	--------------
			    if ot_id == 3 then
	--[[
	HB: Slave configuration flag8 bit: description [ clear/0, set/1]
	0: DHW present [ dhw not present, dhw is present ]
	1: Control type [ modulating, on/off ]
	2: Cooling config [ cooling not supported,
	cooling supported]
	3: DHW config [instantaneous or not-specified,
	storage tank]
	4: Master low-off&pump control function [allowed,
	not allowed]
	5: CH2 present [CH2 not present, CH2 present]
	6: reserved
	7: reserved
	LB: Slave MemberID code u8 0..255 MemberID code of the slave
	]]--
					ot_boiler_configuration = v[2]		 
					ot_boiler_id = v[3]		 
			    end
	--------------
			    if ot_id == 5 then
	--[[
	HB: Application-specific
	fault flags
	flag8 bit: description [ clear/0, set/1]
	0: Service request [service not req’d, service required]
	1: Lockout-reset [ remote reset disabled, rr enabled]
	2: Low water press [no WP fault, water pressure fault]
	3: Gas/flame fault [ no G/F fault, gas/flame fault ]
	4: Air press fault [ no AP fault, air pressure fault ]
	5: Water over-temp[ no OvT fault, over-temperat. Fault]
	6: reserved
	7: reserved
	LB: OEM fault code u8 0..255 An OEM-specific fault/error code
	]]--

					-- since 2016-09-14 SVN>=202, Lua>=10.0 >> remove Global Fault Flag check:
					-- reset any flags if main FAULT flag is ZERO
--					if ot_boiler_status["fault"] == 0 then
--						v[2] = 0
--						v[3] = 0
--					else
						local ot_faults = {}
						local ot_fault_oem	
						ot_faults["service"] = bit32.extract(v[2], 0)
						ot_faults["lockout"] = bit32.extract(v[2], 1)
						ot_faults["low_press"] = bit32.extract(v[2], 2)
						ot_faults["flame"] = bit32.extract(v[2], 3)
						ot_faults["air_press"] = bit32.extract(v[2], 4)
						ot_faults["overtemp"] = bit32.extract(v[2], 5)
						ot_fault_oem = v[3]
	
						if ot_fault_oem > 0 then ot_faults["oem"] = 1 else ot_faults["oem"] = 0 end
						ot_add_fault(ot_faults, ot_fault_oem)
--					end	-- if fault...
			    end -- if ot_id == 5...
	--------------
			    ot_packs_string = table.concat({ot_packs_string, string.char(v[1], ot_id, v[2], v[3])})
			    ot_n_bytes = ot_n_bytes + 4
			end -- if v ~= nil..
		end -- if bit32.band(mask, 1) == 1...
		mask = bit32.rshift(mask, 1)
	  end -- while mask ~= 0 do...
  end -- if utility.param.p_setup.p82_on == 1 then
--------------

  -- Let do not add first byte - "LEN" - we will add it below
  temp_string = table.concat({string.char(0x25,0x6B), datetime,})
  temp_string = table.concat({temp_string, string.char(255)}) -- boiler uptime not used. But set 0xFF as OpenTherm option
  local temp_string_2 = string.char(0, 0, 0) -- all fields are dumb and not used for L1000.  
  temp_string = table.concat({temp_string, temp_string_2})
  --State ­ Состояние (бит 0 – питание, бит 1 – авария; 1­есть, 0­нет) ->> 
  local status
  local boiler_status = 0 
  if utility.param.p_setup.p82_on == 1 then
    boiler_status = bit32.lshift (ot_boiler_status["fault"], 1)
  end  
  local power_status = 1
  local power_voltage = 0
  if k_event_unit_test_flag == false then
  	power_voltage = get_analog_input(1)
  else
  	power_voltage = power_voltage_unit_test
  end
  if config.power_control_adc1_check == true and power_voltage < config.power_control_adc1_level then
  	power_status = 0
  end
  status = bit32.bor(boiler_status, power_status)
  temp_string = table.concat({temp_string, string.char(status)})

  temp_string = table.concat({temp_string, string.char(ot_n_bytes/4), ot_packs_string})

  local out_string = table.concat({string.char(14+ot_n_bytes), temp_string})
--print(">>>>> k-packet test dump-2:",utility.hex_dump(out_string), '\not_n_bytes', ot_n_bytes)
  --CRC вычисляется как сумма всех байт пакета вместе с длиной и сигнатурой
  summ = 0
  for i=1, 15+ot_n_bytes  do
    summ = (summ + string.byte(out_string,i) ) %256
--print(">>>>> k-packet test dump-0:", string.byte(out_string,i))
  end
  out_string = table.concat({out_string, string.char(summ)})
--utility.print_r ( ot_packets_rx )  
--print(">>>>> k-packet test dump-3:",utility.hex_dump(out_string), ot_n_bytes)
  return out_string
end
-------------------------------------------------------------------------------
local function unit_test_k_event()
  utility.param = lip.load("param/param.ini")
  th_tab.f_ini()

  k_event_unit_test_flag = true
  utility.param.p_setup.p82_on = false
  config.power_control_adc1_check=true-- вход analog-1 используется для контроля питания 
  config.power_control_adc1_level=10000
  power_voltage_unit_test = 10002
  print(">>>>> k-packet test dump:",utility.hex_dump(k_event.create_k_event()))
  print("---")
  power_voltage_unit_test = 0
  ot_boiler_status["fault"] = 1
  print(">>>>> k-packet test dump:",utility.hex_dump(k_event.create_k_event()))
--k_event.create_k_event()
end  
-------------------------------------------------------------------------------
-- unit test
-- unit_test_k_event()

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

return k_event
