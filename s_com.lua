
local s_com =  {}
utility = require("lua/utility")
get = require("lua/get")
usr = require("lua/usr")
ot_uart = require("lua/ot_uart")
ot = require("lua/ot")

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------
--[[0–TurnOFF;1–Comfort;2–ECONOM;3–Schedule; RETURN: #S0:V]]--
function s0_quest_parser(s_suff, uuid)
  local ret
  if uuid ~= nil then
    if utility.param[uuid] ~= nil then
      if utility.param[uuid].mode ~= nil then
        ret = "#S0:"..utility.param[uuid].mode.."&"..uuid
      else
        ret = "#S0:!"
      end
    else
      ret = "#S0:!"
    end
  else
    ret = "#S0:"..utility.param.term_control.mode
  end
  
  if g_log_level >= 1 then  -- low level
    print("S0? response:",ret)
  end
-- Additional format: #S0?&uuid
  return ret
end
--------------------------------------------------------
function s1_quest_parser(s_suff, uuid)
  --  T_off T_comfort T_eco #S1=280 294 288
  local ret
  if uuid ~= nil then

--  if g_log_level >= 1 then  -- low level
--    print("S1? input:", s_suff, uuid, utility.param[uuid], utility.param[uuid].mode)
--  end

    if utility.param[uuid] ~= nil then
        ret = "#S1:"..utility.param[uuid].tset_off.." "
          ..utility.param[uuid].tset_comf.." "..utility.param[uuid].tset_eco.."&"..uuid
    else
      ret = "#S1:!"
    end
  else
  ret = "#S1:"..utility.param.term_control.tset_off.." "
    ..utility.param.term_control.tset_comf.." "..utility.param.term_control.tset_eco
  end
  
  if g_log_level >= 1 then  -- low level
    print("S1? response:",ret)
  end

  return ret
end
--------------------------------------------------------
function s2_quest_parser(s_suff, uuid)
--[[#S2?0  или так #S2=0
#S2:0 191919192020212223232323232323232323232323212019
#S2:<INDEX> - INDEX:
 if 1...7 - недельное расписание целой части температуры. Температура в Цельсии, 
    две цифры на час. Всего 24 часа по две цифры  - 48 цифр;
if 9...15 - недельное расписание дробной части, одна цифра на час. 
    Всего 24 часа по одной цифре - 24 цифры
 tcalend_int_1=191919192020212223232323232323232323232323212019
 tcalend_fract_1=123456789012345678901234
 NOTE: param.ini must contains first letter "T" at each string. 
 It's because if string contains digits only then it convert to number at LIP.lua 
 ]]--
 
  local ret = "#S2:!"
  local sched
--  print("S2? Parser", s_suff)
  if((s_suff == nil) or(s_suff:len() == 0 ))then 
    print("******* WRONG S2? Format - no suffix")
    return ret
  end
  -- let extract first 3 fields w/o uuid
  local s_suff_str = (s_suff:match("^[%d']+"))
  
  if g_log_level >= 1 then  -- low level
    print("S2? input:",s_suff_str, s_suff)
  end
  
  if uuid ~= nil then
    if utility.param[uuid] ~= nil then
        if s_suff_str == "1" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_1.."&"..uuid
        elseif s_suff_str == "2" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_2.."&"..uuid
        elseif s_suff_str == "3" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_3.."&"..uuid
        elseif s_suff_str == "4" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_4.."&"..uuid
        elseif s_suff_str == "5" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_5.."&"..uuid
        elseif s_suff_str == "6" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_6.."&"..uuid
        elseif s_suff_str == "7" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_int_7.."&"..uuid
        elseif s_suff_str == "9" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_1.."&"..uuid
        elseif s_suff_str == "10" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_2.."&"..uuid
        elseif s_suff_str == "11" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_3.."&"..uuid
        elseif s_suff_str == "12" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_4.."&"..uuid
        elseif s_suff_str == "13" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_5.."&"..uuid
        elseif s_suff_str == "14" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_6.."&"..uuid
        elseif s_suff_str == "15" then
          ret = "#S2:"..s_suff_str.." "..utility.param[uuid].tcalend_fract_7.."&"..uuid
        else
          print("***** WRONG S2? Suffix (uuid)", s_suff)
          return ret
        end
    else
      ret = "#S0:!"
    end -- if utility[uuid] ~= nil
  else -- old fashion:
      if s_suff_str == "1" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_1
      elseif s_suff_str == "2" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_2
      elseif s_suff_str == "3" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_3
      elseif s_suff_str == "4" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_4
      elseif s_suff_str == "5" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_5
      elseif s_suff_str == "6" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_6
      elseif s_suff_str == "7" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_int_7
      elseif s_suff_str == "9" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_1
      elseif s_suff_str == "10" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_2
      elseif s_suff_str == "11" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_3
      elseif s_suff_str == "12" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_4
      elseif s_suff_str == "13" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_5
      elseif s_suff_str == "14" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_6
      elseif s_suff_str == "15" then
        ret = "#S2:"..s_suff_str.." "..utility.param.term_control.tcalend_fract_7
      else
        print("***** WRONG S2? Suffix", s_suff_str)
        return ret
      end
  end -- if uuid ~=nil

  ret = ret:gsub("T", "")
  if g_log_level >= 1 then  -- low level
    print("S2? response:",ret)
  end
  return ret
end
--------------------------------------------------------
function s3_quest_parser(s_suff)
--[[
Значения: Sched_type T_Alarm_Hi T_Alarm_Low Hist Error_inp_pol Al_inp_type

Sched_type – тип расписания (0-дневное 1-недельное)
T_Alarm_Hi     верхний температурный порог (абс)
 T_Alarm_Low   нижний температурный порог (абс)
Hist – гистерезис регулирования *0.1 гр
Error_inp_pol полярность  аварийного входа (0-замык на минус  1-размык минуса)
Al_inp_type – Тип тревожного входа (0-off, 3-датчик замикание на минус, 4-датчик размыкание минуса,  5-датчик движения,  7-дат.движения без задержки, 9-протечка воды, 10-пожарный датчик, 12-утечка газа,  13-тревожная кнопка )
]]--
  local ret
--  print("S3? Parser")
  ret = "#S3:"..utility.param.s3_setup.Sched_type.." "
    ..utility.param.s3_setup.T_Alarm_Hi.." "
    ..utility.param.s3_setup.T_Alarm_Low.." "
    ..utility.param.s3_setup.Hist.." "
    ..utility.param.s3_setup.Error_inp_pol.." "
    ..utility.param.s3_setup.Al_inp_type
    
  return ret
end
--------------------------------------------------------
function s6_quest_parser(s_suff)
-- Значение: напряжение питания (х0,1в)
-- Let input "1" connected to power supply line 
  local ret
  ret = "#S6:"..tostring(math.floor(get_analog_input(1)/100)) -- floor -> returns x rounded down to the nearest integer
  return ret
end
--------------------------------------------------------
function s9_quest_parser(s_suff)
--  телефонные номера через запятую, Пример: #S9? -> #S9=8912345678, 8912345679, 8912345678  
-- stored at param.ini, section "PHONES" s9=xxxx s49=xxxxx
	local ret = "#S9:!"
  	if utility.param.phones ~= nil and utility.param.phones.s9 ~= nil then 
		ret = "#S9="..utility.param.phones.s9
	end
	return ret 
end
--------------------------------------------------------
--[[
#S14?
#S14:сост1, Назн1 … состN, НазнN
#S14= Назн1…НазнN
Сост: 
0 - неподключен (значение не присутствует)
1 - неисправен (значение не присутствует)
2 - исправен
Назн:
0 не назначено
1 Регулирование
2 Регулирование резерв
3 температура снаружи
Пример:
#S14?
#S14:2 3 2 2 2 1 0 0 0  0 0 0
#S14=1 2 3   --]]
 --[[ NEW Design for control_setup
  Zont has control_setup register, not field at the s14_s17_setup line
  Lets utiliti.param.term_control.control_setup contains slot (index) number, started from 1: 1..
  Code below has contro_setup local variable ==> Please discard this value, it still stay as STALE !!

  10-000802b44efc=2 2 0 278 273 {Name}
  example: <serial>=<index> <status> <control_setup> <max(Kelvin)> <min(Kelvin)> {NAME}
  --]]
    
function s14_quest_parser(s_suff)
  local s14_response = "#S14:"
  local param_line
  local start_i, end_i
  local control_setup
  
--  print("S14? Parser")
  -- Lets read file with serial numbers ds1820_raw_info.txt and check if new sensor is appeared? 
  -- If Yes, Then create new entry for param.ini and then report to server with full S14 report.
  local file = assert(io.open("param/ds1820_raw_info.txt", 'r'), 'Error loading file : ' .. "ds1820_raw_info.txt");
  local index = 1 -- Zero means empty record - stale sensor
  
  -- DO NOT FORGET TO ADD INDEX to param !!!!!!!!!!
  if(utility.param.s14_s17_setup ~= nil) then 
    for serial in file:lines() do
      param_line = utility.param.s14_s17_setup[serial]
      if (param_line == nil)then
        --[[ new serial number => new sensor is appeared
            Add to param.ini new entry. Format:
            Parameter=Index Sensor_Status Control_Status Maximum_Trsh Minimum_Trsh {NAME} >> use default:
            <i> 2 0 278 303 {}          ]]--
        param_line = "0 2 0 303 278 {}"
      end -- if(param_line == nil)
      
      start_i, end_i = param_line:find(" ")
      utility.param.s14_s17_setup[serial] = tostring(index)..param_line:sub(start_i) 
      
      control_setup = 0
      if (index == utility.param.term_control.control_setup) then
        control_setup = 1
      end
      if (index == utility.param.term_control.second_control_setup) then
        control_setup = 2
      end
      if (index == utility.param.term_control.outdoor_setup) then
        control_setup = 3
      end
      
      start_i, end_i = param_line:find(" ")
      -- apend: <blank><status><blank><control_setup>
      s14_response = s14_response .. param_line:sub(start_i+1, start_i+2)..tostring(control_setup).." "
      index = index + 1
    end -- "for" for all ds1820_raw_info.txt lines
  else
    s14_response = "#S14" -- sensors are not presented
  end -- if(... ~nil)
  file:close();
  utility.update_param()
--  print("S14 response: ", s14_response)
  return s14_response
end
--------------------------------------------------------
--[[ #S17:Num Thr_h Thr_l Name Ser
#S17= Num Thr_h Thr_l Name
Num – номер термометра (0…)
Thr_h – верхний порог (Кельвины)
Thr_l – нижний порог (Кельвины)
Name – название термометра (строка макс 20 символов ограниченная символами { })
Ser -  серийный номер (строка до пробела) (длина не ограничена, можно добавить первые три символа -” Family Code -”)
Если порог == 0 то он не контролируется
#S17?N - сначала запрашивает у Зонта о датчике N
#S17=0 ????
#S17:0 320 273 {в помещении} 000CA6A56201
#S17=0 321 274 {на улице}
#S17:0 321 274 {на улице} 000CA6A56201  ]]--
------
function s17_quest_parser(s_suff) -- s_suff -> Sensor Number (from ZERO: 0...)
  local s17_response = "#S17:!"
  local sensor_name
  local start_i, end_i, key, value
  local index = 0

  if g_log_level >= 3 then  -- maximum level
    print("S17? Parser","number", s_suff)
  end

  if((s_suff == nil) or(s_suff:len() == 0 ))then 
    print("******* WRONG S17 Format - no suffix")
    return "#S17!"
  end
  
  -- NOTE #S17 uses index starts from ZERO: 0,1,... But we use from ONE: 1,2, ..
  if(utility.param.s14_s17_setup ~= nil) then  
    for key,value in pairs(utility.param.s14_s17_setup)do
    
      -- Solution example: for w in s:gmatch("%S+") do print(w) end
      local words = {}
      for word in value:gmatch("%S+") do --%S represents all non-space characters
        table.insert(words, word)
      end
      
      -- The Problem: NAME may include SPACE and splits for many words[7], words[8] and so on
      -- Let concantinate them again to one NAME
      for i, v in ipairs(words) do
        if(i > 6)then
          words[6] = words[6].." "..v
        end
      end
      
      --[s14_s17_setup]format:
      -- <serial>=<index> <status> <control_setup> <max(Kelvin)> <min(Kelvin)> {NAME}
--      print("words= ", words[1],words[2],words[3],words[4],words[5],words[6])
      
      index = words[1]
 --     print("s17_response-1  ", index, s_suff)
      if( tonumber(index) == (tonumber(s_suff)+1) )then
        s17_response = "#S17:"..tostring(index-1).." "..words[4].." "..words[5].." "..words[6].." "..key
      end
    end -- for key,..
  end --if(utility.param.s17_s17...
--  print("s17_response-2  ", s17_response)
  return s17_response
end
--------------------------------------------------------
function s20_quest_parser(s_suff)
-- #S20? #S20:N N – количество поддерживаемых параметров
	local s20_response = "#S20:!"
	local ot_num = tostring(ot_total_packs_num)
	if ot_boiler_comm_failed == false then
		s20_response = "#S20:"..ot_num
	end
--print("#S20", s20_response, ot_total_packs_num, ot_boiler_comm_failed)	
	return s20_response
end
--------------------------------------------------------
function s21_quest_parser(s_suff)
-- #S21?n  #S21:n x1 x2 x3 x4
-- Data – Данные для отправки в опен терм (LB+256*HB)
-- x1 x2 x3 x4 – байты по протоколу open therm
	local s21_response = "#S21:!"
	local ind = tonumber(s_suff)
	if ot_boiler_comm_failed == true or ind>=ot_total_packs_num then 
		return "#S21!"
	end
	if ind == nil or ind<0 then 
		print("******* WRONG S21 Format - no suffix")
		return "#S21!"
	end
	-- There are 2 tables: 
	-- ot_packets_tx >> {ot_type, ot_id, ot_hb, ot_lb} - simple list 
	-- ot_packets_rx >> [ot_id]={ot_type, ot_hb, ot_lb} - Key/Value associative array
	-- #S21?n request for simple list index 0...25 but send response from ot_packet_rx
	-- Let find out: Requested ot_id is:
	local ot_id = ot_packets_tx[ind+1][2]
	if ot_id ~= nill then
		if ot_packets_rx[ot_id] ~= nil then
			local ot_type = tostring(ot_packets_rx[ot_id][1])
			local ot_hb = tostring(ot_packets_rx[ot_id][2])
			local ot_lb = tostring(ot_packets_rx[ot_id][3])
			s21_response = "#S21:"..s_suff.." "..ot_type.." "..ot_id.." "..ot_hb.." "..ot_lb
		end
	end
--print("#S21-10", s21_response)	
	return s21_response
end
--------------------------------------------------------
function s31_quest_parser(s_suff)
--[[ Format: 
#S31=PZA_N
#S31:PZA_N  PZA_A  PZA_B  PZA_C
PZA_N – номер кривой
PZA_A  PZA_B  PZA_C – коэффициенты *10000 ]]--
  local ret = "#S31:"..utility.param.pza.curve_num
  print("S31? response:",ret)
  return ret
end
--------------------------------------------------------
function s36_quest_parser(s_suff)
  local ret = "#S36:"..utility.param.term_control.timezone
--  print("S36? response:",ret)
  return ret
end
--------------------------------------------------------
function s49_quest_parser(s_suff)
--  телефонные номера через запятую, Пример: #S9? -> #S9=8912345678, 8912345679, 8912345678  
-- stored at param.ini, section "PHONES" s9=xxxx s49=xxxxx
	local ret = "#S49:!"
  	if utility.param.phones ~= nil and utility.param.phones.s49 ~= nil then 
		ret = "#S49="..utility.param.phones.s9
	end
	return ret 
end
--------------------------------------------------------
function p_82_quest_parser(s_suff)
  local ret = "#P82:"..utility.param.p_setup.p82_on
--  print("P82? response:",ret)
  return ret
end
--------------------------------------------------------
function p_85_quest_parser(s_suff)
  local ret = "#P85:"..utility.param.p_setup.p85_max_w
  return ret
end
--------------------------------------------------------
function p_86_quest_parser(s_suff)
  local ret = "#P86:"..utility.param.p_setup.p86_min_w
  return ret
end
--------------------------------------------------------
function p_87_quest_parser(s_suff)
  local ret = "#P87:"..utility.param.p_setup.p87_log
  return ret
end
--------------------------------------------------------
function p_88_quest_parser(s_suff)
  local ret = "#P88:"..utility.param.p_setup.p88_max_m
  return ret
end
--------------------------------------------------------
function p_89_quest_parser(s_suff)
  local ret = "#P89:"..utility.param.p_setup.p89_stat
  return ret
end
--------------------------------------------------------
function p_90_quest_parser(s_suff)
  local ret = "#P90:"..utility.param.p_setup.p90_dhw
  return ret
end
--------------------------------------------------------
function p_91_quest_parser(s_suff)
  local ret = "#P91:"..utility.param.p_setup.p91_min_p
  return ret
end
--------------------------------------------------------
function p_92_quest_parser(s_suff)
  local ret = "#P92:"..utility.param.p_setup.p92_mask
  return ret
end
--------------------------------------------------------
function p_109_quest_parser(s_suff)
  local ret = "#P109:"..utility.param.p_setup.p109_relay
  return ret
end
--------------------------------------------------------

--------------------------------------------------------
--------------------------------------------------------
--  EQUAL PARSER --
--------------------------------------------------------
--------------------------------------------------------
function s0_equal_parser(s_suff, uuid)
--  if(s_suff:find('[0123]') == nil)then 
--      print ("***** WRONG Format: #S0=XX XX-argument= "..s_suff)
--      return "#S0:!" 
--  end
  if uuid ~= nil then
    if utility.param[uuid] ~= nil then
      utility.param[uuid].mode =  tonumber(s_suff:sub(1,1))-- get fist digit
      if g_log_level >= 1 then  -- low level
        print("S0= (2):", utility.param[uuid].mode, uuid)
        print (">>4>",s_suff, s_suff:sub(1,1), s_suff:sub(1,3), tonumber(s_suff.sub(1,1)))
      end
    end  
  else
    utility.param.term_control.mode = tonumber(s_suff)
  end
--  print("#S0=... ", s_suff)
  return s0_quest_parser(s_suff, uuid)
end
--------------------------------------------------------
--[[Значение: T_off T_comfort T_eco Задание установочных температур для всех режимов (абс)
Пример
#S1=280 294 288 
Tset    - Установочная температура 2 байт (абсТ <500 дискрет 1 Кельвин, >=500 дискрет 0.1 Кельвин)
Те параметры, значение которых не должно меняться заменяются символом *.
]]--
----------------------
function s1_equal_parser(s_suff, uuid)
  local t_off_num, t_comfort_num, t_eco_num
  local i=1
  local num, word
--  print(">#S1-1 ", s_suff)
  -- let extract first 3 fields w/o uuid
  local s_suff_temper = s_suff:match("[%d ']+")

  local uuid_flag = false
  if uuid ~= nil then
    if utility.param[uuid] ~= nil then
      uuid_flag = true
    end  
  end

  for word in string.gmatch(s_suff_temper, "%S+") do
      num = tonumber(word)
      if((num ==nil) and (word ~= "*"))then
        print("***** WRONG #S1= Format: not number", s_suff)
        return ""
      end
      if((word ~="*"))then
        if(i==1)then
          if uuid_flag == true then
            utility.param[uuid].tset_off = num
          else          
            utility.param.term_control.tset_off = num
          end
        elseif (i==2)then
          if uuid_flag == true then
            utility.param[uuid].tset_comf = num
          else          
            utility.param.term_control.tset_comf = num
          end
        elseif (i==3)then
          if uuid_flag == true then
            utility.param[uuid].tset_eco = num
          else          
            utility.param.term_control.tset_eco = num
          end
        else
          print ("***** WRONG Format: #S1=XXX XXX XXX Must has 3 words *", s_suff)
        end  -- switch i
      end -- "*"  - still as is
    i = i + 1
  end -- for do
  return s1_quest_parser(s_suff, uuid)
end
--------------------------------------------------------
--[[#S2?0  или так #S2=0
#S2:0 191919192020212223232323232323232323232323212019
#S2:<INDEX> - INDEX:
 if 1...7 - недельное расписание целой части температуры. Температура в Цельсии, 
    две цифры на час. Всего 24 часа по две цифры  - 48 цифр;
if 9...15 - недельное расписание дробной части, одна цифра на час. 
    Всего 24 часа по одной цифре - 24 цифры
 tcalend_int_1=191919192020212223232323232323232323232323212019
 tcalend_fract_1=123456789012345678901234
 NOTE: param.ini must contains first letter "T" at each string. 
 It's because if string contains digits only then it convert to number at LIP.lua  ]]--
 --------------------
function s2_equal_parser(s_suff, uuid)
  local ret = ""
  -- let extract first 3 fields w/o uuid
  local s_suff_temper = s_suff:match("[%d ']+")
  local ind_str = s_suff_temper:match('^%d%d? ')
  local  param_str = s_suff_temper:match('%d+$')
  
  local uuid_flag = false
  if uuid ~= nil then
    if utility.param[uuid] ~= nil then
      uuid_flag = true
    end  
  end

  if((s_suff == nil)or(s_suff:len() == 0))then 
    print("******* WRONG S2= Format - no suffix")
    return ret
  end

  if g_log_level >= 1 then  -- low level
    print("S2? input:",ind_str, s_suff, s_suff_temper)
  end

  ind_str = ind_str:match('%d%d?') -- remove last space

  if uuid_flag == false then
      if ind_str == "1" then
        utility.param.term_control.tcalend_int_1 = "T"..param_str
      elseif ind_str == "2" then
        utility.param.term_control.tcalend_int_2 = "T"..param_str
      elseif ind_str == "3" then
        utility.param.term_control.tcalend_int_3 = "T"..param_str
      elseif ind_str == "4" then
        utility.param.term_control.tcalend_int_4 = "T"..param_str
      elseif ind_str == "5" then
        utility.param.term_control.tcalend_int_5 = "T"..param_str
      elseif ind_str == "6" then
        utility.param.term_control.tcalend_int_6 = "T"..param_str
      elseif ind_str == "7" then
        utility.param.term_control.tcalend_int_7 = "T"..param_str
      elseif ind_str == "9" then
        utility.param.term_control.tcalend_fract_1 = "T"..param_str
      elseif ind_str == "10" then
        utility.param.term_control.tcalend_fract_2 = "T"..param_str
      elseif ind_str == "11" then
        utility.param.term_control.tcalend_fract_3 = "T"..param_str
      elseif ind_str == "12" then
        utility.param.term_control.tcalend_fract_4 = "T"..param_str
      elseif ind_str == "13" then
        utility.param.term_control.tcalend_fract_5 = "T"..param_str
      elseif ind_str == "14" then
        utility.param.term_control.tcalend_fract_6 = "T"..param_str
      elseif ind_str == "15" then
        utility.param.term_control.tcalend_fract_7 = "T"..param_str
      else
        print("***** WRONG S2= Suffix", s_suff)
      end
  else -- if uuid_flag == false 
      if ind_str == "1" then
        utility.param[uuid].tcalend_int_1 = "T"..param_str
      elseif ind_str == "2" then
        utility.param[uuid].tcalend_int_2 = "T"..param_str
      elseif ind_str == "3" then
        utility.param[uuid].tcalend_int_3 = "T"..param_str
      elseif ind_str == "4" then
        utility.param[uuid].tcalend_int_4 = "T"..param_str
      elseif ind_str == "5" then
        utility.param[uuid].tcalend_int_5 = "T"..param_str
      elseif ind_str == "6" then
        utility.param[uuid].tcalend_int_6 = "T"..param_str
      elseif ind_str == "7" then
        utility.param[uuid].tcalend_int_7 = "T"..param_str
      elseif ind_str == "9" then
        utility.param[uuid].tcalend_fract_1 = "T"..param_str
      elseif ind_str == "10" then
        utility.param[uuid].tcalend_fract_2 = "T"..param_str
      elseif ind_str == "11" then
        utility.param[uuid].tcalend_fract_3 = "T"..param_str
      elseif ind_str == "12" then
        utility.param[uuid].tcalend_fract_4 = "T"..param_str
      elseif ind_str == "13" then
        utility.param[uuid].tcalend_fract_5 = "T"..param_str
      elseif ind_str == "14" then
        utility.param[uuid].tcalend_fract_6 = "T"..param_str
      elseif ind_str == "15" then
        utility.param[uuid].tcalend_fract_7 = "T"..param_str
      else
        print("***** WRONG S2= Suffix (uuid)", s_suff)
      end
  end -- if uuid_flag == false 
  return s2_quest_parser(ind_str, uuid)
end
--------------------------------------------------------
--Значения: Sched_type T_Alarm_Hi T_Alarm_Low Hist Error_inp_pol Al_inp_type
--[[ Sched_type – тип расписания (0-дневное 1-недельное)
T_Alarm_Hi     верхний температурный порог (абс)
 T_Alarm_Low   нижний температурный порог (абс)
Hist – гистерезис регулирования *0.1 гр
Error_inp_pol полярность  аварийного входа (0-замык на минус  1-размык минуса)
Al_inp_type – Тип тревожного входа (0-off, 3-датчик замикание на минус, 4-датчик размыкание минуса,  5-датчик движения,  7-дат.движения без задержки, 9-протечка воды, 10-пожарный датчик, 12-утечка газа,  13-тревожная кнопка )]]--
-----------------
function s3_equal_parser(s_suff)
  local ret = ""
  local i=1
  local num, word
--  print("S3=X Parser", s_suff)

  for word in string.gmatch(s_suff, "[^ ]+") do
    if(word:find('[^0-9*.]') ~= nil)then
      print ("***** WRONG Format: #S3=XXX XXX XXX Must be digits OR *", s_suff)
      return s3_quest_parser(s_suff)
    else
      num = tonumber(word)
--      print(">>>>#S3-4: ", word, num)
      if((word ~="*")and(num ~=nil))then
        if(i==1)then
          utility.param.s3_setup.Sched_type = num
        elseif (i==2)then
          utility.param.s3_setup.T_Alarm_Hi = num
        elseif (i==3)then
          utility.param.s3_setup.T_Alarm_Low = num
        elseif (i==4)then
          utility.param.s3_setup.Hist = num
        elseif (i==5)then
          utility.param.s3_setup.Error_inp_pol = num
        elseif (i==6)then
          utility.param.s3_setup.Al_inp_type = num
        else
          print ("***** WRONG Format: #S3=XXX XXX XXX Must has 6 words *", s_suff)
        end  -- switch i
      end -- "*"  - still as is
    end -- if(word:find('[^0-9*.]') ... else
    i = i + 1
  end -- for do
  return s3_quest_parser(s_suff)  
end

--------------------------------------------------------
function s9_equal_parser(s_suff)
--  телефонные номера через запятую, Пример: #S9? -> #S9=8912345678, 8912345679, 8912345678  
  	if utility.param.phones == nil then utility.param.phones = {} end
    utility.param.phones.s9 = s_suff 
	return s9_quest_parser(s_suff)
end
--------------------------------------------------------
-- #S14= Назн1…НазнN
function s14_equal_parser(s_suff)
  local ret = ""
  local word
  local sensors_value = {}
  local sensors_key = {}
  local start_i, end_i, key, value
  local setup
  local i = 1 -- 
  local sensor_max = 16

--[[  This is PROBLEM. If Server sends like #S14=0 1 2 then we need find out that "3" status is absend.
There are many many many such scenarios!!
Let #S18 Resets all setup valuet to Zero!! It's not good but I do not know hot to fix it.   --]]  
  utility.param.term_control.control_setup = 0
  utility.param.term_control.second_control_setup = 0
  utility.param.term_control.outdoor_setup = 0
  utility.update_param()
  
  for word in string.gmatch(s_suff, "[^ ]+") do -- word -> setup1, setup2, ..
    if(word:find('[^0-9*]') ~= nil)then
      print ("***** WRONG Format: #S14=X X X Must be digits OR *", s_suff)
      return s14_quest_parser(s_suff)
    else
      if((word ~="*")and(word:find('[0-3]')))then
        local arg = tonumber(word)
--[[ NOTE: s14_s17_setup has parameter "control_setup". Actually it is not used here. Instead used
param.term_control.control_setup and so on, see below. "control_setup" still Zero in any cases. --]]
        if arg == 1 then
          utility.param.term_control.control_setup = i
        elseif arg == 2 then
          utility.param.term_control.second_control_setup = i
        elseif arg == 3 then
          utility.param.term_control.outdoor_setup = i
        end
      end -- "*"  - still as is
    end -- if(word:find('[^0-9*.]') ... else
    i = i + 1
    if(i>sensor_max + 1)then
      print ("***** WRONG Format: #S14= Must has maximum sensors=", sensor_max, "S14", s_suff, "i=", i)
      break
    end
  end -- for word in string.

  return s14_quest_parser(s_suff)  
end
--------------------------------------------------------
function s15_equal_parser(s_suff)
  -- config.zip should be updated too
  os.execute("zip -q config.zip param/ds1820_raw_info.txt param/param.ini lua/*.lua")
  utility.update_param()
  return "#S15:1" 
  end
--------------------------------------------------------
-- #S17= Num Thr_h Thr_l Name
function s17_equal_parser(s_suff)
  -- let extract Num (slot number) to digits. Then find out what is serial (key) then change data for this serial (slot)
  
  -- NOTE #S17 uses index starts from ZERO: 0,1,... But we use from ONE: 1,2, ..
  local words = {}
  for word in s_suff:gmatch("%S+") do --%S represents all non-space characters
    table.insert(words, word)
  end
  -- The Problem: NAME may include SPACE and splits for many words[5], words[6] and so on
  -- Let concantinate them again to one NAME
  for i, v in ipairs(words) do
    if(i > 4)then
      words[4] = words[4].." "..v
    end
  end      
--  print("s_suff line:",words[1],words[2],words[3],words[4],words[5])
  
  local num_str = words[1]
--  local num_str = s_suff:match('[0-9]?[0-9]')
  if(num_str ~= nil)then 
--    num_str = num_str:match('[0-9]?[0-9]')-- remove last space
    -- Let read all sensors and find out the same number as num_str
    for key,value in pairs(utility.param.s14_s17_setup)do
--      print(">>>>>#S17-0", key, value)
      --------------------------------------------------------------------------------------
      -- Solution example: for w in s:gmatch("%S+") do print(w) end
      local vals = {}
      for word in value:gmatch("%S+") do --%S represents all non-space characters
        table.insert(vals, word)
      end
      
--      local index_str = value:gmatch("%S+")      
      local index_str = vals[1]      
--      print(">>>>#S17-1", s_suff, index_str)
      if(index_str ~= nil)then
        if(tonumber(num_str)+1 == tonumber(index_str))then 
          -- param.ini line format is following:
          -- 10-000802b44efc=2 2 1 303 278 {Датчик 3}
          -- serial=<number> <status> <setup> <max> <min> {NAME}
--          print(">>>>#S17-2",index_str, words[1],words[2],words[3],words[4] )
--          print(">>>>#S17-4", vals[1],vals[2],vals[3],vals[4],vals[5], vals[6] )
          utility.param.s14_s17_setup[key] = vals[1].." "..vals[2].." "..vals[3]
            .." "..words[2].." "..words[3].." "..words[4]
        end
      end
    end -- for key,value in pairs(
  else
    print("***** WRONG Format #S17= Num not number", s_suff)
    return "#S17:!"
  end 
  return s17_quest_parser(num_str)
end
--------------------------------------------------------
function s18_equal_parser(s_suff)
  local index
  local start_i, end_i, key, value
  local temp_string
--  print("S18? Parser")
  --[[ <s14_s17_setup> format example: 10-000802b44efc=2 2 0 278 273 {Name}
  <serial>=<index> <status> <control_setup> <max(Kelvin)> <min(Kelvin)> {NAME}
  
  #S18 task is to set "-1" for all "index" fiels and set "0" to all "control_setup" fields   --]]
  
  if(utility.param.s14_s17_setup ~= nil) then 
    for key,value in pairs(utility.param.s14_s17_setup)do
  --    print (key, value)

      -- Solution example: for w in s:gmatch("%S+") do print(w) end
      local words = {}
      for word in value:gmatch("%S+") do --%S represents all non-space characters
        table.insert(words, word)
      end
      -- words[1] - <index>
      -- words[2] - <status>
      -- words[3] - <control_setup>
      -- words[4] - <max>
      -- words[5] - <min>
      -- words[6],7,8,.. - <{Name}>
      -- NOTE; {NAME1stWORD NAME2ndWORD} was broken to parts!
      -- The Problem: NAME may include SPACE and splits for many words[7], words[8] and so on
      -- Let concantinate them again to one NAME
      for i, v in ipairs(words) do
        if(i > 6)then
          words[6] = words[6].." "..v
        end
      end
      -- now words[6] = <{Name}>

      -- Let set to zero another fields as there:
      utility.param.term_control.control_setup = 0
      utility.param.term_control.second_control_setup = 0
      utility.param.term_control.outdoor_setup = 0
    
      -- NOTE: <control_setup> sets to Zero, but it does no used more !!!
      if(words[6] ~= nil) then
        temp_string ="-1 "..words[2].." 0 "..words[4].." "..words[5].." "..words[6] 
        utility.param.s14_s17_setup[key] = temp_string
--[[  start_i, end_i = value:find(" ")
      if(end_i > 1)then
        index = value:sub(1, end_i-1)
        temp_string = value:gsub("[%d-]* ", "-1 ", 1)
        utility.param.s14_s17_setup[key] = temp_string --]]
      else
        print("Wrong S14_S17 param.ini format - SPACE at the first symbol ")
        return "#S18:!"
      end
    end -- for(key,value...
  end -- if(...~=nil)  
  return "#S18:1"
end
--------------------------------------------------------
function s21_equal_parser(s_suff)
	return s21_quest_parser(s_suff)
end
--------------------------------------------------------
function s31_equal_parser(s_suff)
--[[ Format: 
#S31=PZA_N
#S31:PZA_N  PZA_A  PZA_B  PZA_C
PZA_N – номер кривой
PZA_A  PZA_B  PZA_C – коэффициенты *10000 ]]--	
	utility.param.pza.curve_num = tonumber(s_suff)
	return s31_quest_parser(s_suff)
end
--------------------------------------------------------
function s36_equal_parser(s_suff)
--  print("S36 Parser")
  utility.param.term_control.timezone = tonumber(s_suff)
  return s36_quest_parser(s_suff)
end
--------------------------------------------------------
function s49_equal_parser(s_suff)
--  телефонные номера через запятую, Пример: #S49? -> #S49=8912345678, 8912345679, 8912345678  
  	if utility.param.phones == nil then utility.param.phones = {} end
    utility.param.phones.s49 = s_suff 
	return s49_quest_parser(s_suff)
end
--------------------------------------------------------
function s99_equal_parser(s_suff)
  local temp_string = "#S99:"
  local handle = io.popen(s_suff)
  local result = handle:read("*a")
  handle:close()
  local res_truncate = string.sub (result, 1, 4000)--truncate XX symbols
--  print(temp_string..res_truncate)
  return temp_string..res_truncate
end
--------------------------------------------------------
function p_82_equal_parser(s_suff) -- 1 => ON; 0 => OFF
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p82_on = res end
	return p_82_quest_parser(s_suff)
end
--------------------------------------------------------
function p_85_equal_parser(s_suff) -- f8.8 3530 => (3530-2730)/10=80; 
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p85_max_w = res end  
	return p_85_quest_parser(s_suff)
end
--------------------------------------------------------
function p_86_equal_parser(s_suff) -- f8.8 3530 => (3530-2730)/10=80; 
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p86_min_w = res end  
	return p_86_quest_parser(s_suff)
end
--------------------------------------------------------
function p_87_equal_parser(s_suff) -- 0-no; 1-OT; 2-OT & radio 
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p87_log = res end  
	return p_87_quest_parser(s_suff)
end
--------------------------------------------------------
function p_88_equal_parser(s_suff) -- Format: 100% => 25600; 50% => 12800; 10% => 25600
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p88_max_m = res end   
	return p_88_quest_parser(s_suff)
end
--------------------------------------------------------
function p_89_equal_parser(s_suff) --#P89=38400	//уст LB=0 HB=15 => 15*256=3840; 768=3*256
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p89_stat = res end   
	return p_89_quest_parser(s_suff)
end
--------------------------------------------------------
function p_90_equal_parser(s_suff) -- #P90=7680	//уст 30 гр => 30*256=7680; 35*256=8960
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p90_dhw = res end   
	return p_90_quest_parser(s_suff)
end
--------------------------------------------------------
function p_91_equal_parser(s_suff) -- #P91=2560	//уст 10 бар = 1bar=>256
	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p91_min_p = res end   
	return p_91_quest_parser(s_suff)
end
--------------------------------------------------------
function p_92_equal_parser(s_suff) -- b16 b15 b14 b13 ... ; 126483 => (High) 1.1110.1110.0001.0011 (Low)
  	local res = tonumber(s_suff)
	if res ~= nil then utility.param.p_setup.p92_mask = res end 
	return p_92_quest_parser(s_suff)
end
--------------------------------------------------------
function p_109_equal_parser(s_suff) -- 0 => relay OT mode; 1 => analog OT mode
	local res = tonumber(s_suff)
print("p_109_equal_parser-1", s_suff, res, utility.param.p_setup.p109_relay)	
	if res ~= nil then utility.param.p_setup.p109_relay = res end   
print("p_109_equal_parser-2", s_suff, res, utility.param.p_setup.p109_relay)	
	return p_109_quest_parser(s_suff)
end
--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------
s_func_question_table = {
S0=s0_quest_parser,
S1=s1_quest_parser,
S2=s2_quest_parser,
S3=s3_quest_parser,
S6=s6_quest_parser,
S9=s9_quest_parser,
S14=s14_quest_parser,
S17=s17_quest_parser,
S20=s20_quest_parser,
S21=s21_quest_parser,
S31=s31_quest_parser,
S36=s36_quest_parser,
S49=s49_quest_parser,
P82=p_82_quest_parser,
P85=p_85_quest_parser,
P86=p_86_quest_parser,
P87=p_87_quest_parser,
P88=p_88_quest_parser,
P89=p_89_quest_parser,
P90=p_90_quest_parser,
P91=p_91_quest_parser,
P92=p_92_quest_parser,
P109=p_109_quest_parser,
}
--------------------------------------------------------
s_func_equal_table = {
S0=s0_equal_parser,
S1=s1_equal_parser,
S2=s2_equal_parser,
S3=s3_equal_parser,
S9=s9_equal_parser,
S14=s14_equal_parser,
S15=s15_equal_parser,
S17=s17_equal_parser,
S18=s18_equal_parser,
S21=s21_equal_parser,
S31=s31_equal_parser,
S36=s36_equal_parser,
S49=s49_equal_parser,
P82=p_82_equal_parser,
P85=p_85_equal_parser,
P86=p_86_equal_parser,
P87=p_87_equal_parser,
P88=p_88_equal_parser,
P89=p_89_equal_parser,
P90=p_90_equal_parser,
P91=p_91_equal_parser,
P92=p_92_equal_parser,
S99=s99_equal_parser,
P109=p_109_equal_parser,
}
--------------------------------------------------------
function s_com.parser(string)
  -- It may be that more then one S Command presented. So need parser each command separatelly.
  -- Possible: #S1?2#S3=55#S10? (not real but for illustration only)
  local start_i=1
  local end_i
  local s_command
  local res = ""
  local ret
  local uuid = nil
  
  local ind = string:find("&")

  local sss = ""
  if ind ~= nil then
    uuid = string:sub(ind+1)
--    print("uuid", uuid)
    for key,value in pairs(usr.channel_tab['objects_channels']) do
      if value['uuid'] == uuid  then
          break
      end
    end
  end

-- Add #P commands support. Assume, that #Sxx and #Pyy has different xx and yy areas. 
-- So let ignore "S" and "P" and looking for xx/yy only  
  start_i, end_i = string:find("#S.*", start_i)
  if start_i == nil then
	  start_i, end_i = string:find("#P.*", start_i)
  end
--  print(">>>-1",string, start_i, end_i)
  while (start_i ~= nil)do
    res = ""
    s_command = string:sub(start_i, end_i)
--  print(">>>-2",string, s_command, start_i, end_i)
    if(s_command:find("%?")~=nil) then 
--    print(" #SXX?Y command found", s_command)
    res =s_question_format_parser(s_command, uuid)
--    print("RETURN:", res)
    return res
    else
      if(s_command:find("=")==nil)then
        print("****** Wrong S Commad, without ? and =", s_command)
      else 
--        print(" #SXX command found", s_command)
        res = s_equal_format_parser(s_command, uuid)
--        print("RETURN:", res)
        return res
      end  
    end --
    start_i, end_i = string:find("#S.*", end_i+1) -- actually > 1 command does not used, so this line does not matter
--    print(">>>-3",string, start_i, end_i)
  end -- while
end
--------------------------------------------------------
function s_question_format_parser(string, uuid)
  local start_i, end_i
  local ok, res
  local err
  start_i, end_i = string:find("S%d*%?")
  if start_i == nil then
	  start_i, end_i = string:find("P%d*%?")
  end
  local s_comm_number = string:sub(start_i, end_i-1)
  local s_comm_suff = string:sub(end_i+1)
--  print("s_comm_number", s_comm_number, "s_comm_suff", s_comm_suff, start_i, end_i)
  local p_func = s_func_question_table[s_comm_number]

  if(p_func ~= nill)then
    ok, res = pcall(p_func, s_comm_suff, uuid)
    if(ok==false)then print("***** PCALL ERROR:", res, debug.traceback()) res = "" end
  else
    res = "#"..s_comm_number..":!"
--    print("****** WRONG S Format: <input_string> <number> <suffix>", string, s_comm_number, s_comm_suff)  
  end
  return res
end
----------------------------------------------------------
function s_equal_format_parser(string, uuid)
  local start_i, end_i
  local ok, res

  start_i, end_i = string:find("S%d*=")
  if start_i == nil then
	  start_i, end_i = string:find("P%d*=")
  end
  local s_comm_number = string:sub(start_i, end_i-1)
  local s_comm_suff = string:sub(end_i+1)
--  print(string, "s_comm_number", s_comm_number, "s_comm_suff", s_comm_suff, start_i, end_i)
  local p_func = s_func_equal_table[s_comm_number]
  if(p_func ~= nill)then
    ok, res = pcall(p_func, s_comm_suff, uuid)
    if(ok==false)then print("***** PCALL ERROR:", res, debug.traceback()) res = "" end
  else
    res = "#"..s_comm_number..":!"
--    print("****** WRONG S Format: <input_string> <number> <suffix>", string, s_comm_number, s_comm_suff)  
  end

  th_tab.f_ini()

  utility.update_param()

  return res
end
--------------------------------------------------------
--------------------------------------------------------

-- main
function unit_test()
  utility.param = lip.load("param/param.ini")
  for key,value in pairs(utility.param.s14_s17_setup)do
    print(key, value)
  end 
  g_log_level = 3
  --[[
  s_com.parser("#S77?") print("----")
  s_com.parser("#S0?") print("----")
  s_com.parser("#S1?") print("----")
  s_com.parser("#S2?") print("----")
  s_com.parser("#S2?3") print("----")
  s_com.parser("#S2?13") print("----")
  s_com.parser("#S2?16") print("----")
  s_com.parser("#S3?") print("----")
  s_com.parser("#S14?") print("----")
  s_com.parser("#S17?0") print("----")
  s_com.parser("#S17?2") print("----")
  s_com.parser("#S17?10") print("----\n\n")
--  s_com.parser("#S17?10#S0?#S1?#S2?#S17?2") print("----")
  s_com.parser("#S18=1") print("----")
  s_com.parser("#S14?") print("----")
  s_com.parser("#S0=2") print("----")
  s_com.parser("#S0=4") print("----")
  print(">0",utility.param.term_control.tset_off, utility.param.term_control.tset_comf,utility.param.term_control.tset_eco)
  s_com.parser("#S1=300 301 302") print("----")
  print(">1",utility.param.term_control.tset_off, utility.param.term_control.tset_comf,utility.param.term_control.tset_eco)
  s_com.parser("#S1=350 222 aaa") print("----")
  print(">2",utility.param.term_control.tset_off, utility.param.term_control.tset_comf,utility.param.term_control.tset_eco)
  s_com.parser("#S1=* * *") print("----")
  print(">3",utility.param.term_control.tset_off, utility.param.term_control.tset_comf,utility.param.term_control.tset_eco)
  s_com.parser("#S1=299 * 333") print("----")
  print(">4",utility.param.term_control.tset_off, utility.param.term_control.tset_comf,utility.param.term_control.tset_eco)
  s_com.parser("#S1=370 444 *") print("----")
  print(">5",utility.param.term_control.tset_off, utility.param.term_control.tset_comf,utility.param.term_control.tset_eco)
  s_com.parser("#S2=1 191919192020212223232323232323232323232323212019")
  s_com.parser("#S2=0 191919192020212223232323232323232323232323212019")
  s_com.parser("#S2=7 191919192020212223232323232323232323232323212019")
  s_com.parser("#S2=9 T123456789012345678901234")
  s_com.parser("#S2=11 T123456789012345678901234")
  s_com.parser("#S2=15 T123456789012345678901234")
  s_com.parser("#S2=16 T123456789012345678901234")
--Значения: Sched_type T_Alarm_Hi T_Alarm_Low Hist Error_inp_pol Al_inp_type
  s_com.parser("#S3=0 363 278 0.1 0 0")
  s_com.parser("#S3=0 363 279 0.3 0 0")
  s_com.parser("#S3=0 363 280 * a 0")
  s_com.parser("#S3=0 363 280 . 0 0")
  s_com.parser("#S14=0 0 0")
  s_com.parser("#S14=1 2 2")
  s_com.parser("#S14=2 3 1")
  s_com.parser("#S14=3 1 4")
  s_com.parser("#S16=3 1 4")
--  s_com.parser("#S17=1 400 401 {NAME 1}")
--  s_com.parser("#S17=3 400 401 {NAME 2}")
--  s_com.parser("#S17=1 400 401 {NAME 3}")
--  s_com.parser("#S17=0 400 401 {}")
  s_com.parser("#S0?") print("----")
  s_com.parser("#S17?0")
  s_com.parser("#S17=9 300 290 {SA}")
  s_com.parser("#S17=9 300 290 {SA АБВ SC SD}")
  for key,value in pairs(utility.param.s14_s17_setup)do
    print(key, value)
  end 
]]--
  s_com.parser("#S9=+7910111111")
--  s_com.parser("#S9=")

  utility.update_param()
  utility.param = lip.load("param/param.ini")
  utility.update_param()
  
  print("FINISH")
  os.exit()
end
--------------------------------------------------------
-- unit test start: .../Z701RPI$ lua lua/s_com.lua
-- unit_test()
return s_com
