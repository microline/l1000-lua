local get =  {}
utility = require("lua/utility")

--[[We need to create usfull structure for all current values for thermostat. It includes mode, sensors, setup temperature, algorithm parameters and so on
  Let combine all values at one table. It may be usfull to combain it. Because it placed all here and controlled here   ]]--
  
-------------------------------------------------------------------------------
function th_tab_ini()
  local key, value
  
  ----------------------------
  -- If some new fields at param.ini added need add here its default value
  if utility.param.antilegionella == nil then 
    utility.param.antilegionella = {}
    print "Antilegionella added  to param !!"
  end

  if utility.param.rtc == nil then 
    utility.param.rtc = {}
    utility.param.rtc.rtc_shift = 0
    print "RTC added  to param !!"
  end

  if utility.param.phones == nil then 
    utility.param.phones = {}
    print "PHONES added  to param !!"
  end

  -- SVN >= 200 Lua Release >= 9.00
  if utility.param.s14_s17_setup == nil then 
    utility.param.s14_s17_setup = {}
    print "s14_s17_setup added  to param !!"
  end

  -- SVN >= 199 Lua Release >= 8.25
  if utility.param.s3_setup == nil then 
    utility.param.s3_setup = {}
    utility.param.s3_setup.Al_inp_type = 0
    utility.param.s3_setup.Error_inp_pol = 0
    utility.param.s3_setup.Hist = 0.3
    utility.param.s3_setup.Sched_type = 0
    utility.param.s3_setup.T_Alarm_Hi = 0
    utility.param.s3_setup.T_Alarm_Low = 0
    print "s3_setup added  to param !!"
  end

  if utility.param.term_control == nil then 
    utility.param.term_control = {}
    utility.param.term_control.control_setup = 0
    utility.param.term_control.mode = 4
    utility.param.term_control.outdoor_setup = 0
    utility.param.term_control.second_control_setup = 0
    utility.param.term_control.tcalend_fract_1 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_2 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_3 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_4 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_5 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_6 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_7 = "T000000000000000000000000"
    utility.param.term_control.tcalend_int_1 = "T191919192020212223232323232323232323232323212019"
    utility.param.term_control.tcalend_int_2 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_3 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_4 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_5 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_6 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_7 = "T191919192020212223232323232323232323232323212019"
    utility.param.term_control.timezone = 3
    utility.param.term_control.tset_comf = 444
    utility.param.term_control.tset_eco = 302
    utility.param.term_control.tset_off = 370
    print "term_control added  to param !!"
  end
  
  if utility.param.term_control.tcalend_fract_1 == nil then
    utility.param.term_control.tcalend_fract_1 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_2 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_3 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_4 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_5 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_6 = "T000000000000000000000000"
    utility.param.term_control.tcalend_fract_7 = "T000000000000000000000000"
    print "term_control fract... added  to param !!"
  end  	
  
  if utility.param.term_control.tcalend_int_1 == nil then
    utility.param.term_control.tcalend_int_1 = "T191919192020212223232323232323232323232323212019"
    utility.param.term_control.tcalend_int_2 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_3 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_4 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_5 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_6 = "T202020202020202020202020202020202020202020202020"
    utility.param.term_control.tcalend_int_7 = "T191919192020212223232323232323232323232323212019"
    print "term_control tcalend_int... added  to param !!"
  end  	

  -- SNV<190 has not p_setup fields
  if utility.param.p_setup == nil then 
    utility.param.p_setup = {}
    utility.param.p_setup.p109_relay=0
	utility.param.p_setup.p82_on=0
	utility.param.p_setup.p85_max_w=3040
	utility.param.p_setup.p86_min_w=3280
	utility.param.p_setup.p87_log=0
	utility.param.p_setup.p88_max_m=25600
	utility.param.p_setup.p89_stat=768
	utility.param.p_setup.p90_dhw=11520
	utility.param.p_setup.p91_min_p=256
	utility.param.p_setup.p92_mask=126483
    print "p_setup added  to param !!"
  end

  -- since 2016-09-14 SVN>=202, Lua>=10.0
  if utility.param.p_setup.p92_mask == 0 then 	utility.param.p_setup.p92_mask=126483 end 


  -- SNV-190 has all p_setup fields but has not p87_log field. Need add this one:
  if utility.param.p_setup.p87_log == nil then 
	utility.param.p_setup.p87_log=0
    print "p87_log added  to param !!"
  end
  
  -- SVN>223 Add PZA #S31
  if utility.param.pza == nil then 
	utility.param.pza = {} 
	utility.param.pza.curve_num = 10 
	utility.param.pza.coeff_a = 10 
	utility.param.pza.coeff_b = 10 
	utility.param.pza.coeff_c = 10 
    print "pza added  to param !!"
  end
  ----------------------------
  
  -- Let delete table and re-create it from scratch
--  th_tab = {}
  th_tab_fault_update()
  -- let we iterate all relays: 1, 2, 3, ..6. Let we use all 6 relays. 
  -- If we will use more relays that need change this code:
  th_tab.relay_status = {0, 0, 0, 0, 0, 0,}
  th_tab.digit_input = {0, 0, 0}
  th_tab.analog_input = {0, 0, 0}
  th_tab.analog_output = {0, 0, 0, 0, 0, 0,}
--  th_tab.mode = utility.param.term_control.mode
  th_tab.sens_status = {}
  th_tab.tset_arr_c = {}
  th_tab.ttresh_arr_c = {}
  th_tab.hyster_c = {}
  -- let make one-channel Thermostat yet. In future need to change this code: 
  -- Format:{ {contr_state=0,status=0, name="SENSOR NAME"} }
  -- status: 0 - неподключен (значение не присутствует) 1 - неисправен (значение не присутствует) 2 - исправен
  -- control_setup: 0-не назначено;1-Регулирование; 2-Регулирование резерв; 3-температура снаружи
  -- let counts all sensors:
  local index = 1
  local status, control_setup
  local temp
  local name  
  --[[ NEW Design for control_setup
  Zont has control_setup register, not field at the s14_s17_setup line
  Lets utiliti.param.term_control.control_setup contains slot (index) number, started from 1: 1..
  Code below has contro_setup local variable ==> Please discard this value, it still stay as STALE !!
  --]]
  if(utility.param.s14_s17_setup ~= nil) then
    for key,value in pairs(utility.param.s14_s17_setup)do
      -- 10-000802b44efc=2 2 0 278 273 {Name}
      -- example: <serial>=<index> <status> <control_setup> <max(Kelvin)> <min(Kelvin)> {NAME}
      -- index=0 means that this sensor is not presented currently
      --------------------------------------------------------------------------------------
      -- Solution example: for w in s:gmatch("%S+") do print(w) end
      local words = {}
      for word in value:gmatch("%S+") do --%S represents all non-space characters
        table.insert(words, word)
      end

      -- The Problem: NAME may include SPACE and splits for many words[7], words[8] and so on
      -- Let concantinate them again to one NAME
      for i, v in ipairs(words) do
        if(i > 6)then
          words[6] = words[6].." "..v
        end
      end
      
      index = assert(tonumber(words[1]), "S14-S17 invalid index not a number", index_str)
      if index == 0 then -- do not interesting stale's record
        break
      end

      th_tab.sens_status[index] = {}
      th_tab.ttresh_arr_c[index] = {}
      th_tab.tset_arr_c[index] = {}
      th_tab.tsens_arr_ser[index]=key

      status = tonumber(words[2])
      control_setup = tonumber(words[3])
      local max_treshold = tonumber(words[4])     
      local min_treshold = tonumber(words[5])
      name = words[6]

      if( (max_treshold~=nil) and (min_treshold~=nil)  )then
        th_tab.ttresh_arr_c[index]["max"] = max_treshold - 273 
        th_tab.ttresh_arr_c[index]["min"] = min_treshold - 273
      else
        print("****** ERROR: max min values not founded at param.ini", value)
        print("param.ini line:",words[1],words[2],words[3],words[4],words[5],words[6],words[7])
      end
      
      if((name:find("}") == nil) or (name:find("{") == nil)) then
        -- it was bug at early code version - no "}" at the name field. Let fix it
        print("****** ERROR: wrong sensor name, no } or {. Let fix it, name=", name)
        name = "{ }"
        
        utility.param.s14_s17_setup[key] = words[1].." "..words[2].." "..words[3].." "..words[4].." "..words[5].." "..name
        
        utility.update_param()
      end
      th_tab.sens_status[index]["name"] = name:match('[^{].*[^$}]') -- remove brackets {}
    
      if((control_setup~=nil) and (status~=nil))then
        th_tab.sens_status[index]["status"] = status
        th_tab.sens_status[index]["control_setup"] = control_setup
          
        if(utility.param.term_control.control_setup == index)then
          -- this is main sensor for One_Channel Thermostat
          -- let copy Hysteresis value for this sensor
          th_tab.hyster_c[index] = utility.param.s3_setup.Hist/10
          th_tab.main_channel_ind = index  -- range 1...max
  
          --  mode -- 0/1/2/3 - off/comfort/econom/shedule  
          if(th_tab.mode == 0)then
              temp = convert_tset2c(utility.param.term_control.tset_off)
              th_tab.tset_c = temp
              th_tab.tset_arr_c[index]["off"] = temp 
            elseif(th_tab.mode == 1)then
              temp = convert_tset2c(utility.param.term_control.tset_comf)
              th_tab.tset_c = temp
              th_tab.tset_arr_c[index]["comfort"] = temp 
            elseif(th_tab.mode == 2)then
              temp = convert_tset2c(utility.param.term_control.tset_eco)
              th_tab.tset_c = temp     
              th_tab.tset_arr_c[index]["econom"] = temp 
            elseif(th_tab.mode == 3)then
              temp = get_schedule_c()
              th_tab.tset_c = temp  
              th_tab.tset_arr_c[index]["schedule"] = temp 
            else
              -- wrong mode, let still as is
              print("***** WARNING wrong mode", th_tab.mode)
          end -- if(th_tab.mode == 0)then
        end -- if(utility.param.term_control.control_setup == index)then
      else
        print("****** ERROR: status or control_setup not found ", key, value)
        status = 1
        control_setup = 0
      end -- if((control_setup~=nil) and (status~=nil))then

      --------------------------------------------------------------------------------------
    end -- for key,value in pairs(
  end -- if(utility.param.s14_s17_setup ~= nil)..  
  -- See New Design above:
  --th_tab.main_channel_ind = utility.param.term_control.control_setup
  --th_tab.second_channel_ind = utility.param.term_control.second_control_setup

--  utility.print_r(th_tab) -- for debug oly !! Comment this line !!!!!!!!!!!!!  
 
end
-------------------------------------------------------------------------------
function th_tab_sens_update()
-- index for tempearture may start 0 and may start 1. Be Carrefully !!
-- at param.ini -> s14_s17_setup -> starts from 0
-- at th_tab.tsens_arr[i] - i starts from 1 >>> DUMB !!!
--  print("********* th_tab_sens_update ********")
  if(utility.param.s14_s17_setup ~= nil) then
    for key,value in pairs(utility.param.s14_s17_setup)do
      local space_ind = value:find(" ")
      local index = assert(tonumber(value:sub(1,space_ind)),
          "S14-S17 invalid first parameter " .. value:sub(1,space_ind) .. " is not a number")
      if(index > 0) then
    --    print("th_tab_sens_update: ", "key=", key, " value=", value, " index=", index)
        local temp = call_c_from_lua_get_temp(index-1)/16
        if temp > 1000 then -- sensor got error 
          temp = nil
        end
        th_tab.tsens_arr_c[index] = temp
      end
    end -- for key,value...
  end -- if(... ~= nil)
end
  
-------------------------------------------------------------------------------
function th_tab_fault_update()
--  power_state=1,        -- 1 - ok; 0 - failed
--  boiler_fault_state=0, -- 1 -fault; 0 - no fault
  th_tab.power_state = 1
  th_tab.boiler_fault_state = 0
end
-------------------------------------------------------------------------------
function get_schedule_c()
--[[#S2:<INDEX> - INDEX:
 if 1...7 - недельное расписание целой части температуры. Температура в Цельсии, две цифры на час. Всего 24 часа по две цифры  - 48 цифр;
if 9...15 - недельное расписание дробной части, одна цифра на час. Всего 24 часа по одной цифре - 24 цифры.
tcalend_fract_1=T000000000000000000000000
tcalend_fract_2=T000000000000000000000000
tcalend_fract_3=T000000000000000000000000
tcalend_fract_4=T000000000000000000000000
tcalend_fract_5=T000000000000000000000000
tcalend_fract_6=T000000000000000000000000
tcalend_fract_7=T000000000000000000000000
tcalend_int_1=T202020202020202020202020202020202020202020202020
tcalend_int_2=T202020202020202020202020202020202020202020202020
tcalend_int_3=T202020202020202020202020202020202020202020202020
tcalend_int_4=T202020202020202020202020202020202020202020202020
tcalend_int_5=T202020202020202020202020202020202020202020202020
tcalend_int_6=T202020202020202020202020202020202020202020202020
tcalend_int_7=T202020202020202020202020202020202020202020202020
--]]
  local weekday = os.date("%w") -- 1...7
  local hour = os.date("%H") -- 0...23
  
  -- os.date from Raspberry Pi gets time with GMT+0 timezone, so
  -- Let take into consideration TimeZone utility.param.term_control.timezone
  local timezone = utility.param.term_control.timezone
  if (hour + timezone) > 23 then
    if weekday == 7 then
      weekday = 1
    else
      weekday = weekday + 1  
    end
  end
  hour = (hour + timezone) % 24

  local t_int = utility.param.term_control["tcalend_int_"..tostring(weekday)]
  local t_fract = utility.param.term_control["tcalend_fract_"..tostring(weekday)]

  local temp = t_int:sub(2*hour+2, 2*hour+2+1) + (t_fract:sub(hour+2, hour+2))/10
--  print (weekday, hour, temp, tonumber(temp))  
  return tonumber(temp)
end  
-------------------------------------------------------------------------------
function convert_tset2c(tset)
  local tset_c
  if(tset<500)then 
    tset_c = tset-273 
  else
    tset_c = ((tset)/10) - 273
  end
  return tset_c
end
-------------------------------------------------------------------------------
--[[ Эта таблица хранит все переменные для доступа программы пользователя
Комментарии к каждому полю поясняют его назначение. 
Пример доступа к полю таблицы из кода Lua:
th_tab.tset_c - поле заданной температуры - то самое, которое берется из страницы веб-сервера, режима Комфорт/Эконом/... 
Многие поля таблицы представляют собой массив, т.е. еще одну таблицу и индексом. Для обращения к такому полю используют
квадратные скобки после имени, например:
relay_status[1] - состояние реле с номером 1;
Сразу заметим, что здесь все индексы в Lua начинаются с единицы, т.е. реле имеют номера 1,2,...,6
Описание поля, которое на самом деле массив задается фигурными скобками. Так ниже мы видим, что поле состояния реле имеет вид:
relay_status={}

Есть более сложные поля, которые задаются т.н. ассоциативным массивом. Это, например, поле sens_status={}. Это двумерный массив. 
Первый индекс - номер датчика от 1 до числа датчиков. Второй индекс - специальные зарезервированные слова: 
control_setup
status
name
Для доступа к его полям применяется такая запись:
 sens_status[<индекс от 1 до числа датчиков>][control_setup] - назначение датчика, формат ниже; 
 sens_status[<индекс от 1 до числа датчиков>][status] - статус датчика, формат ниже;
 sens_status[<индекс от 1 до числа датчиков>][name] - Имя датчика, текстовая строка длиной до 20 символов
Примечание, запись <индекс от 1 до числа датчиков> означает число от 1 до числа датчиков, помещенное в первые квадратные скобки (первый индекс)

Возможно, эта таблица будет расширяться, когда веб-интерфейс будет поддерживать новые параметры. Или же будет создана вторая таблица
с новыми параметрами.
--]]    
-- NOTE: there are two different sets: Sensors and Channels. 
-- Each Channel has own sensor But not Each Sesnor has own Channel
-- Let define for all Sensor some Hysteresis, some Trhesholds, some Setup Temperature. It's not needed 
--   for each sensor, but let it be for easy. In this case we may not use "Channel" in this table.
-- We will create one more table which deal with Channels...   
th_tab = {
  relay_status={},      -- Состояние Реле, массив с индексами 1-6. Состояние 1 -реле включено; 0 - реле выключено
  
  digit_input={},        -- Состояние дискретного входа с индексами 1-3. Состояние 1 - высокий входной уровень, 0 - низкий входной уровень
  
  analog_input={},       -- Состояние аналогового входа в милливольтах (пока там еще не милливольты, нужна правка в api.lua, калибровка)
  
  analog_output={},      -- Состояние аналогового выхода, милливольты
  
  mode=1,               -- Режим веб-интерфейса 0-Выкл.; 1-Комфорт; 2-Эконом; 3-Расписание
  
  main_channel_ind=nil,      -- Номер главного датчика, который задан в веб-интерфейсе. Является индексом от 1 до 16. If for some Sensor control_setup==1, Then this is Main channel index 
  
  second_channel_ind=nil,     -- Номер резервного датчика, который задан в веб-интерфейсе. Является индексом от 1 до 16. If for some Sensor control_setup==2, Then this is Reserve channel index
  
  hyster_c={}, -- Гистерезис, массив с индексами 1 до числа датчиков. Измеряется в градусах Цельсия. Пока нет поддержки веб-интерфейса, поле не поддерживается.          
  
  tset_c=19.9,            -- Заданная температура из веб-сервера. Величина в градусах Цельсия. For Main Channel. Setup temperature in Celsius, 0.1 degree discrete
  
  -- Формат: {control_setup=0,status=0, name="SENSOR NAME"}, смотри пример использования выше 
  -- control_setup: 0-не назначено;1-Регулирование; 2-Регулирование резерв; 3-температура снаружи
  -- status: 0 - неподключен (значение не присутствует) 1 - неисправен (значение не присутствует) 2 - исправен
  -- name - имя датчика
  sens_status={},-- Состояние датчика.            

  -- array (table) for setup temperatures for all sensors (index from 1, so main channel has index 1)
  -- Format:{ {"comfort"=XXX, "econom"=YYY, "off"=ZZZ},{}... }
  tset_arr_c={},     -- массив заданных температур, пока нет поддержки веб-интерфейса поле не поддерживается   

  -- array (table) sensors values in Celsius, 0.1 degree discrete. 
  -- Index of this table the same as "slot" for S17 Command
  tsens_arr_c={},  -- массив данных от датчиков в градусах Цельсия

  -- Index of this table the same as "slot" for S17 Command
  tsens_arr_ser={}, -- массив серийных номеров датчиков температуры. Индекс тот же, что и у массива с температурами и макс/мин темп.
  -- array (table) for max/min treshoulds for each sensor. Format:{ {min=xxx, max=yyy},{....}...}
  ttresh_arr_c={}, -- массив максимальных/минимальных значений датчиков, при выходе за которые посылается событие аварии. Берется из веб-интерфейса
  
  power_state=1,        -- Состояние питания: 1-ОК; 0-авария;

  boiler_fault_state=0, -- Состояние котла: 1-авария, 0-ОК

  -- function pointers
  f_ini=th_tab_ini,  
  f_sens_update=th_tab_sens_update,
  f_fault_update=th_tab_fault_update,

}
-------------------------------------------------------------------------------
function get.th_tab_ini()
  --------------------------------------------------------------------------------------------
  -- WARNING - this function must be called AFTER lip.load("param/param.ini") initializatioin !!
  --------------------------------------------------------------------------------------------
  
  -- let check how many sensors, relay and so on we have. What parameters we store at flash.
  -- Actually th_tab_ini() and f_ini() looks like the same. May be in future they are will be different?
  th_tab.f_ini()
end
-------------------------------------------------------------------------------
function unit_test()
  utility.param = lip.load("param/param.ini")
  th_tab.tsens_arr_c[1] = 20
  
  th_tab.f_ini()
  th_tab.tsens_arr_c[1] = 22.9


  th_tab.sens_status[1] = {}
  th_tab.sens_status[1]["name"] = "AAA" 


  print(th_tab.mode, th_tab.tsens_arr_c[2])
  print(th_tab.sens_status[1]["name"])
  
  
  print(get_schedule_c())
  
end
-------------------------------------------------------------------------------
-- !!!! this is for debug only!!! Comment it for full code !!!!
--function call_c_from_lua_get_temp(sensor_ind)
--  return 25.5
--end
-------------------------------------------------------------------------------
-- unit test
--unit_test()
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

return get
