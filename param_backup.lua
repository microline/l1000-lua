--[[
We have rare issue: file param.ini failed, some times its content deleted.
Solution - create param2.ini, param3.ini backup copies.
Add md5sum string as first line at param.ini file. This line does not touch LIP Lua library.

Algorithm:
READ:
- read param.ini md5sum and check;
- check param2.ini, param3.ini and check;
- update backup files param2.ini, param3.ini if it needed
- if param.ini md5 check failed then take backup from param2.ini, param3.ini
WRITE:
- write via LIP.save()
- read param.ini again, md5sum is updated and write again
- update all backup files - param2.ini, param3.ini


Performance info. md5.lua md5sum implementation has not good performance. 
Let use C/Lua library -- https://github.com/keplerproject/md5
Let check performance at RasPi, 
but PC looks like very good- 1000000 passes for 12 seconds
Do not use LuaRocks but "C" fragment code ported - function lmd5()
  
]]--

local lip = require("lua/LIP")
--local utility = require("lua/utility")
--local md5 = require( "lua/md5")
--local get = require("lua/get")

--local md5 = require"md5"--https://github.com/keplerproject/md5

----------------------------------------------------------------------
PARAM_NAME = "param/param.ini"
PARAM_BACKUP1 = "param/param2.ini"
PARAM_BACKUP2 = "param/param3.ini"
local TIMEOUTBEFOREBACKUP = 0.1 -- 100 ms timeout before write any backup file
----------------------------------------------------------------------
-- ported from https://github.com/keplerproject/md5
---- @param k String with original message.
-- @return String with the md5 hash value converted to hexadecimal digits
local function sumhexa (k)
--  k = core.sum(k)
  return (string.gsub(k, ".", function (c)
           return string.format("%02x", string.byte(c))
         end))
end
----------------------------------------------------------------------
local function read_file_linebyline(filename)
-- RETURN:
-- 	true/fail -> does file exist?
--	first line of file ("" if does not exist)
--	another lines of file ("" if does not exist
	local fh, err, res = false
	local first_line = ""
	local another_lines = ""

	fh,err = io.open(filename)
	if err then 
		print("\n******** Error when open file (1) ", filename); 
		return res, "", "" 
		end
	res = true
	
	local line
	local first_line_flag = true
	while true do
		line = fh:read()
		if line == nil then break end
		if first_line_flag == true  then
			first_line = line
			first_line_flag = false
		else
			another_lines = another_lines..line.."\n"
		end
	end
	fh:close()
	return res, first_line, another_lines
end
----------------------------------------------------------------------
function update_md5_atfirstline(filename)
	local fho, err, line

	local file = io.open(filename, "r+") -- r read mode and b binary mode
	if not file then return nil end
	local content = file:read "*a" -- *a or *all reads the whole file
	
	-- new version: md5sum calculates on "C", good performance:
	local md5_as_hex = sumhexa(lmd5(content))
	
	-- old version - md5sum calculates on Lua -very slow, discarded:
	--local md5_as_hex = md5.sumhexa(content)   -- returns a hex string
	
	file:seek("set", 0)
	file:write(md5_as_hex)
	file:write("\n")
	file:write(content)
	file:close()
end
----------------------------------------------------------------------
local function md5check(first_line, another_lines)
	-- RETURN: if md5sum passed -> true
	local res = true

	if(first_line == nil) or (another_lines == nil) then
		return false
	end
	
	-- new version: md5sum calculates on "C", good performance:
	local md5_as_hex = sumhexa(lmd5(another_lines))
	
	-- old version - md5sum calculates on Lua -very slow, discarded:
	--local md5_as_hex = md5.sumhexa(another_lines)   -- returns a hex string
	if first_line == md5_as_hex then
		res = true
	else
		res = false
	end
	return res, md5_as_hex
end
----------------------------------------------------------------------
local function md5file(filename)
	local md5_as_hex = ""
	local res, first_line, another_lines = read_file_linebyline(filename)
	if res == true then
		res, md5_as_hex = md5check(first_line, another_lines)
	end
	return res, md5_as_hex
end
----------------------------------------------------------------------
local function copy2backup(filename, backupname)
	local file = io.open(filename, "rb") -- r read mode and b binary mode
	if not file then return nil end
	local content = file:read "*a" -- *a or *all reads the whole file
	file:close()
	file = io.open(backupname, "wb") -- recreate backup from scratch
	file:write(content)
	file:close()
end
----------------------------------------------------------------------
function	update_backup_check_before(filename, backup1name, backup2name)
	-- Assume that main param.ini is good. Let update all backups
	-- Assume we have not any info about backup, let check backup before
	-- NOTE: if backup files need to update then need to make pause between param.ini writing and param2/3.ini writing.
	-- 	Because it may cause that Power failed now and we may destroy all 3 files simultaneously.
	-- 	Let make delay between writing procedures

	local res, md5_p = md5file(filename)
	local res_b, md5_b = md5file(backup1name)

	if res ~= true then
		print("***** ERROR update_backup_check_before() fail-1 ")-- it's may not happened !
		return
	end

	if (res_b == true) and (md5_p == md5_b) then
	-- backup1 is OK
	else
		-- backup1 failed !!
		os.execute("sleep " .. tonumber(TIMEOUTBEFOREBACKUP))
		copy2backup(filename, backup1name)
	end

	res_b, md5_b = md5file(backup2name)
	if (res_b == true) and (md5_p == md5_b) then
	-- backup2 is OK. Do nothing
	else
		-- backup2 failed !!
		os.execute("sleep " .. tonumber(TIMEOUTBEFOREBACKUP))
		copy2backup(filename, backup2name)
	end

end
----------------------------------------------------------------------
function isMD5ok(filename, backup1name, backup2name)
	-- this is pre-fix before LIP.load(). Before call it check md5sum and
	-- uses backup if this check failed
	-- RETURN:
	--				"OK" md5 is OK;
	--				"NEEDFIXEND" means md5 failed and need to take backup;
	--				"FAILED" means md5 failed and backup failed too. param.ini reset to factory default values
-----------------------
	-- Special Case: new Lua version with md5 and old param.ini file
	-- In this case backup files are not preset yet. Let check is backup files exist??	
	
	local status = "FAILED" -- possible: "OK", "FAILED", "FIXED"
	local res, md5_p = md5file(filename)

	if res == true then
		update_backup_check_before(filename, backup1name, backup2name)
		status = "OK"
	else
		res, md5_p = md5file(backup1name)
		if (res == true) then
			-- backup1 is good. Let take it and copy to main
			print("***** WARNING param.ini MD5SUM failed, backup-1  is used !!!!")
			update_backup_check_before(backup1name, filename, backup2name)
			status = "FIXED"
		else
			-- backup1 failed, let take backup2:
			res, md5_p = md5file(backup2name)
			if (res == true) then
				-- backup2 is good. Let take it and copy to main
				print("***** WARNING param.ini MD5SUM failed, backup-2 is used !!!!")
				update_backup_check_before(backup2name, filename, backup1name)
				status = "FIXED"
			else
				-- all 3 files failed. Catastrophe !! Reset main to zero and report ERROR !!
				print("\n***** ERROR param.ini MD5SUM failed, backup files failed too. RESET param.ini to factory values !!!!\n")
				local fho,err = io.open(filename, "w")
				if err then print("******** Error when open file (2) ", filename); return status; end
				fho:close() -- param.ini now EMPTY
				-- here was code to recreate new utility.param. BUT problem is cross-"require" issue 
				-- >> Lua code can not be loaded if "require" cross called !!
				-- Let remove code below to lua_main.lua				 
--				utility.param = lip.load(PARAM_NAME)
--				get.th_tab_ini()
--				utility.update_param()
				-- now first line has not md5 sum. Let add md5sum firstly
--				update_md5_atfirstline(PARAM_NAME)
--				update_backup_check_before(filename, backup1name, backup2name)
				status = "FAILED"
			end -- check 2nd backup
		end -- check 1st backup
	end -- check main file
	return status
end
----------------------------------------------------------------------
local function	update_backup_wo_check(filename, backup1name, backup2name)
	-- Assume that main param.ini is good. Let update all backups
	-- Assume param.ini changed, let recreate backup from scratch
	-- NOTE: if backup files need to update then need to make pause between param.ini writing and param2/3.ini writing.
	-- 	Because it may cause that Power failed now and we may destroy all 3 files simultaneously.
	-- 	Let make delay between writing procedures
	os.execute("sleep " .. tonumber(TIMEOUTBEFOREBACKUP))
	copy2backup(filename, backup1name)
	os.execute("sleep " .. tonumber(TIMEOUTBEFOREBACKUP))
	copy2backup(filename, backup2name)
end
----------------------------------------------------------------------
function save_withmd5()
-- this is post-fix after LIP.save().
-- re-write param.ini and backup files with new content with new md5sum
-- RETURN: NONE
-- Assume that LIP.save() passed. After that md5sum removed by LIP.save().Let recreate it again
	update_md5_atfirstline(PARAM_NAME)
	update_backup_check_before(PARAM_NAME, PARAM_BACKUP1, PARAM_BACKUP2)
end
----------------------------------------------------------------------

-- unit test

----------------------------------------------------------------------
--				print (">>>>-21\n")
--				utility.print_r ( utility.param )
--				print(debug.traceback(), filename)
----------------------------------------------------------------------
----------------------------------------------------------------------
 