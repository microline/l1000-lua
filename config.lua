--[[
*******************************************************************************
В этом файле содержатся настройки, которые не вошли в утилиту настройки 
и задаются сервисным инженером путем редактирования этого файла.

Так же здесь настройки, которые изменяются с помощью утилиты настройки, но специальным образом.
Это делается клавишей "Настройки" и сразу перезаписывают этот файл.

Все переменные в этом файле глобальные. Поэтому, желательно давать им уникальные имена, 
удобные для понимания.
*******************************************************************************
]]--
local config = {} 

config.antilegionella_temperature = 65 -- температура нагрева для функции антилегионелла
config.antilegionella_period_days = 14 -- период нагрева фукции антилегионелла
config.antilegionella_time_minutes = 3 -- время прогрева фукции антилегионелла
config.antilegionella_start_time_hour = 01 -- в какое время стартует фукция - время в диапазоне 0...24 час.
config.internet_check_timeout = 200 -- какой интервал проверки наличя Интернета
config.adc_shift = 0 -- компенсания сдвига измерения АЦП из-за апаратных проблем 
config.adc_vref = 3300 -- in millivolts
config.main_server_url = "s1.zont-online.ru" -- адрес главного сервера
config.test_server_url = "d1.zont.microline.ru" -- адрес тестового сервера - без необходимости не применять
--[[          Control Register Format:
  bit7  bit6  bit5  bit4  bit3  bit2  bit1  bit0
  <bit2, bit1, bit0> - BaudRate, format:
  0 - 1200
  1 - 2400
  2 - 4800
  3 - 9600
  4 - 19200
  5 - 36400
  6 - 72800
  7 - 115200
  bit3 - StopBit, format: 0 - One Stop Bit; 1 - Two Stop Bits;
  <bit5, bit4> - Parity, format: 0 - NoParity; 1 - ParityEven, 2 - ParityOdd ]]--
config.uart_1_setup = 7 --(115200) k-line настройка, формат см. выше 
config.uart_2_setup = 0 --(1200) rs485 настройка, формат см. выше
config.ot_faults_timeout = 180 -- задержка перед отправкой повторной ошибки OT 
config.ot_comm_timeout = 120 -- задержка перед отправкой ошибки потери связи OT 
config.ot_fault_setpoint=53-- температура теплоносителя, если ошибка датчика регулирования 
-------------------
-- patch_thread.lua configuration:
config.isPatchTrheadOn = true
config.enable_eth=true-- Ethernet включен
config.enable_wifi=false-- WiFi выключен
config.enable_3g=false-- 3G выключен
config.check_highest_level_period=7200-- reset algorithm periodically in seconds
config.usb_id_modem="12d1:1001"-- USB 3G Modem's ID
config.provider_apn="internet"-- Operator's APN
-------------------
config.fault_cntr_max = 10
config.timeout4reboot = 3600*12 -- reboot if too many faults at this period in seconds

config.start_timeout = 30
config.check_timeout = 15

config.timeouts_eth = {10, 30, 5} -- eth0 {check_period, check_max, turn_on } 
config.timeouts_wifi = {10, 120, 30} -- wlan0 {check_period, check_max, turn_on }
config.timeouts_3g = {10, 240, 60} -- 3G {check_period, check_max, turn_on }

config.power_control_adc1_check=false-- вход analog-1 используется для контроля питания 
config.power_control_adc1_level=10000-- порог питания, ниже которого отображается авария, милливольт 
-------------------


return config
