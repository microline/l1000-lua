--[[ 	This is special script to check and switch Internet Providers. 
	Started at new thread and do not communicate with main thread ]]--

local config = require("lua/config")

local version = 50

-----------------------
local isPatchTrheadOn	= config.isPatchTrheadOn
local enable_eth 		= config.enable_eth
local enable_wifi		= config.enable_wifi
local enable_3g			= config.enable_3g

local fault_cntr_max = 10
local timeout4reboot = 3600*12 -- reboot if too many faults at this period
local check_highest_level_period = config.check_highest_level_period

local start_timeout = 30
local check_timeout = 15

local timeouts_eth = {10, 30, 5} -- eth0 {check_period, check_max, turn_on } 
local timeouts_wifi = {10, 120, 30} -- wlan0 {check_period, check_max, turn_on }
local timeouts_3g = {10, 240, 60} -- 3G {check_period, check_max, turn_on }
-------------------

-- there are 3 levels: 0-print none; 1-print errors; 2-print our msg; 3-print system msg
local print_debug_level = 0

local cntr_t = {}
local del_suff = " 1>&- 2>&-"
------------------------------------------------------------------
------------------------------------------------------------------
local function pread(cmd)
    local f = io.popen(cmd)
    if not f then return end
    local output = f:read("*a")
    f:close()
    return output
end
------------------------------------------------------------------
local function print_debug(...)
    if print_debug_level >= 2 then
		  local printResult = ""
		  local arguments = {...}  -- pack the arguments in a table
		  for i, v in pairs(arguments) do
	        printResult = printResult .. tostring(v) .. "\t"
		  end
	      printResult = printResult .. "\n"
	      print(">>> patch_thread >>> "..tostring(os.date("%X")).." "..printResult)
	end	      
end
------------------------------------------------------------------
local function apn_change(file, apn_template, apn)
	-- if apn == nil let apn qual to empty string
	if apn == nil then apn = "" end
	-- Warning: function does proper work Only If APN at the END OF Line
	-- do not re-write file if apn_new == apn_old
	-- sed command: > sudo sed -i 's/original/new/g' file.txt
	local com = "grep "..apn_template.." "..file
	local apn_current = pread(com)
	-- issue: need 'trim' apn_current line. Let use following: s:match "^%s*(.-)%s*$"
	apn_current = apn_current:match("^%s*(.-)%s*$")
	-- issue: need to add quotes for APN. Argument has not quotes!
	if apn_current == apn_template.."\""..apn.."\"" then
		-- do nothing because APN the same
		return false
	end
	com = "sudo sed -i 's/"..apn_current.."/"..apn_template.."\""..apn.."\"".."/' "..file
	local res = os.execute(com)
	return true
end
------------------------------------------------------------------
local function sdcard_check()
	local cmd = "sudo tune2fs -l /dev/mmcblk0p2 |grep -A 1 state"
	-- good response:
	-- Filesystem state:         clean
	-- Errors behavior:          Continue
	os.execute(cmd)
end
------------------------------------------------------------------
local function ping(interface)
	local string = string.format("ping -I %s %s %s 2>/dev/null", interface, "-W 10 -c 2", "yandex.ru")
    local out = pread(string)
    return tonumber(out:match("/(%d+%.%d+)/"))
end
------------------------------------------------------------------
------------------------------------------------------------------
local function kill_name(name)
	local com = string.format("ps -ef|grep %s| grep -v grep | awk '{ print $2 }'", name)
	local find_str = pread(com)
	for id_str in string.gmatch(find_str, "%d+") do 
		print_debug(">>>-5 ", id_str) 
		com = string.format("sudo kill -9 %s", id_str)
			os.execute(com)
		end
end
------------------------------------------------------------------
local function SplitMeIntolines(str)
local t = {}
local function helper(line) table.insert(t, line) return "" end
helper((str:gsub("(.-)r?n", helper)))
return t
end
------------------------------------------------------------------
local function route_t_parser()
	-- table with all lines of route table is created
	local com = "sudo ip route"
	local out = pread(com)
	local route_t = {}

	for line in out:gmatch("[^\r\n]+") do 
      table.insert(route_t, line)			
	end
	return route_t
end
------------------------------------------------------------------
local function print_route_t(force) -- for test only 
    if (print_debug_level >= 2) or (force == true) then
		local res = route_t_parser()
		for i, v in pairs(res) do
			print(i, v)
		end
	end
end
------------------------------------------------------------------
local function remove_all_providers()
	if enable_3g == true then
		local com = "sudo /home/pi/3g/sakis3g disconnect"
		if print_debug_level < 3 then 
			com = com..del_suff		
		end
		os.execute(com)
	end		
	
	local out = route_t_parser()
	for i, v in pairs(out) do 
		local com = "sudo ip route del "..v
		if print_debug_level < 3 then 
			com = com..del_suff		
		end
		os.execute(com)
	end
end
------------------------------------------------------------------
local function turn_on_eth(v)
	if v["enable"] == true then
		print_debug("turn_on_eth-1")
		remove_all_providers()
		local com = "sudo ifdown eth0" -- sudo ifdown eth0; sudo ifup eth0 
		if print_debug_level < 3 then 
			com = com..del_suff		
		end
		os.execute(com)
		print_debug("turn_on_eth-2")
		com = "sudo ifup eth0"  -- sudo /etc/init.d/networking restart
		if print_debug_level < 3 then 
			com = com..del_suff		
		end
		os.execute(com)
		print_debug("turn_on_eth-3")
		os.execute("sleep " .. tostring(v["timeout"][3]))
	end
	return v["enable"]
end  
------------------------------------------------------------------
local function turn_on_wifi(v)
	if v["enable"] == true then
	print_debug("turn_on_wifi")
		remove_all_providers()
		local com = "sudo /sbin/wpa_supplicant -P /var/run/wpa_supplicant.wlan0.pid -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf "
		if print_debug_level < 3 then 
			com = com..del_suff.." &"		
		end
		print_debug("turn_on_wlan-2", com)		
		os.execute(com)
		os.execute("sleep " .. tostring(v["timeout"][3]))
		print_debug("turn_on_wlan-3")
	end
	return v["enable"]
end  
------------------------------------------------------------------
local function turn_on_3g(v)
	if v["enable"] == true then
		print_debug("turn_on_3g")
		remove_all_providers()
		local com = "sudo /home/pi/3g/sakis3g connect"		
		if print_debug_level < 3 then 
			com = com..del_suff		
		end
		os.execute(com)

		os.execute("sleep " .. tostring(v["timeout"][3]))
	end
	return v["enable"]
end  
------------------------------------------------------------------
local function check_provider(v)
	local res
	local prvd = v["name"]
	local start_time = os.time() -- -- current time
	local max_time = start_time + v["timeout"][2]
	
	if v["enable"] == true then
	
		-- let wait for first check
		res = ping(prvd)
		print_debug("first ping", prvd, res)
		if res == nil then 
			print_debug("pause", prvd, res)
			os.execute("sleep " .. tostring(v["timeout"][1]))
		else
			return res, os.time()-start_time --res, time4check
		end
		
		while (res == nil) and (os.time() < max_time) do
			print_debug("pause", prvd, res)
			os.execute("sleep " .. tostring(v["timeout"][1]))
			res = ping(prvd)
			print_debug("ping:", prvd, res)
		end
	end
	return res, os.time()-start_time --res, time4check
end
------------------------------------------------------------------
local function quick_check_provider(v)
	-- The difference between check_provider() and quick_check_provider() is timeout;
	-- Quick Check return immediately if ping failed.
	local res
	local prvd = v["name"]
	
	if v["enable"] == true then
		res = ping(prvd)
		print_debug("quick first ping", prvd, res)
	end
	return res
end
------------------------------------------------------------------
local function isHighestLevel(ind)
-- argument:  current index (level) 1/2/3
-- output: true -> need reset to highest level; false - skip 
	local res = false
	for i=1, #cntr_t do
		if (cntr_t[i]["enable"] == true) and (i < ind) then res = true end
	end
	return res
end
------------------------------------------------------------------
cntr_t[1] = {} -- High Priority (Ethernet)
cntr_t[2] = {} -- Middle Priority (WiFi)
cntr_t[3] = {} -- Low Priority (3G)
cntr_t[1]["name"] = "eth0"
cntr_t[2]["name"] = "wlan0"
cntr_t[3]["name"] = "ppp0"

cntr_t[1]["enable"] = enable_eth
cntr_t[2]["enable"] = enable_wifi
cntr_t[3]["enable"] = enable_3g

cntr_t[1]["f_on"] = turn_on_eth  
cntr_t[2]["f_on"] = turn_on_wifi
cntr_t[3]["f_on"] = turn_on_3g

cntr_t[1]["timeout"] = timeouts_eth -- eth0 {check_period, check_max, turn_on } 
cntr_t[2]["timeout"] = timeouts_wifi -- wlan0
cntr_t[3]["timeout"] = timeouts_3g -- 3G
------------------------------------------------------------------
--[[
-- TODO:
-- 		goes to Hard Reset (Wathch Dog) after some threshold of faults 
-------
--Rules for eth/wifi/3g switching:
-- 	starts for eth
-- 	if failed then switch to wifi
-- 	if failed then switch to 3G
-- 	after large timeout renew to eth (may be in 1...2 hours)
-- 	if needs send SMS then switch to 3G (let check firstly, may be works w/o switch??? )
----------
--Notes: 
--	remove all non-used route lines
--	remove all non-used default route lines
--	
--	Test Cases:
--		- check print nothing for level 0
--		- setup wifi only
--		- setup 3G only
--		- setup Eth+Wifi
--		- setup Eth+3G
--		- setup WiFi+3G
--		- setup Eth+WiFI+3G but disconnect some of interfaces (eth/wifi/3G)
--		- test with server => looking for server connection when providers switching
--
-- Question - how to make WatchDog instead of softreset? => need stop RTC seconds, because RTC loaded to STM32 for WD control
]]--
local com
local res
local time2check
local res_curr_level
local reset 

print("[patch_thread.lua setup]:", tostring(os.date("%c")), version, isPatchTrheadOn, enable_eth, enable_wifi, enable_3g, timeout4reboot, check_highest_level_period, fault_cntr_max, start_timeout, check_timeout)

apn_change("/etc/sakis3g.conf", "APN=", config.provider_apn)
apn_change("/etc/sakis3g.conf", "USBMODEM=", config.usb_id_modem)
--os.exit(0)
-----------------------
sdcard_check()

if isPatchTrheadOn == true then
	-- Let pause at the start
	os.execute("sleep " .. tostring(start_timeout))
	      
	local fault_cntr = 0
	local reboot_timeout = os.time() + timeout4reboot
	
	while(true) do -- main infinite loop 
		-- NOTE, special case: - no NTP at this time, so may be time is wrong and reset_time wrong too.
		--then NTP has started and cycle has break. May be it may happened sometimes, not too big issue.
		local reset_time = os.time() + check_highest_level_period -- next time to reset
		reset = false
	
		for i, v in ipairs(cntr_t) do -- ipairs catch simultaneously 1,2,3,1,2,3
			if v["enable"] == true then
				res_curr_level = quick_check_provider(v)
				if res_curr_level == nil then 
					local status, err = pcall(v["f_on"], v) 
				end
				res_curr_level = check_provider(v)
				print("******", tostring(os.date("%X")), "new provider", v["name"], "ping", res_curr_level)
				while(res_curr_level ~=nil) do
					-- periodically check high priority provider and if it True then re-configure again
					res_curr_level = check_provider(v) 
					os.execute("sleep " .. tostring(check_timeout))
					-- after large timeout reset current level and start from high level
	
					if os.time() > reset_time then 
						if isHighestLevel(i) == true then reset = true break end
					end 
				end -- while(...
				if reset == true then break end
			end -- if v["enable"] == true 
	
		end -- for i, v in ipairs
	
		if os.time() > reboot_timeout then
			if (fault_cntr < fault_cntr_max) then
			fault_cntr = 0
			reboot_timeout = os.time() + timeout4reboot
			else
print("DEBUG: patch_thread: too many network provider faults for timeout, so goes to reboot", 
					timeout4reboot, fault_cntr, fault_cntr_max, reboot_timeout)
--				os.execute("sudo reboot")
			end
		end
		fault_cntr = fault_cntr + 1
	
	end -- while(true)
end -- if isPatchTrheadOn...

------------------------------------------------------------------

--[[
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ETHERNET", os.time())
print_route_t(true)
local ind = 1
res = pcall(cntr_t[ind]["f_on"], cntr_t[ind])
print_route_t(true)
print("ping result", cntr_t[ind]["name"], ping(cntr_t[ind]["name"]) )
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 3G", os.time())
local ind = 3
res = pcall(cntr_t[ind]["f_on"], cntr_t[ind])
print_route_t(true)
print("ping result", cntr_t[ind]["name"], ping(cntr_t[ind]["name"]) )

print("check_provider()",  check_provider(cntr_t[ind]) ) 


--print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 3G", os.time())
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ETHERNET", os.time())
local ind = 1
res = pcall(cntr_t[ind]["f_on"], cntr_t[ind])
print_route_t(true)
print("ping result", cntr_t[ind]["name"], ping(cntr_t[ind]["name"]) )

print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FINISH", os.time())
os.exit(0)
]]--
------------------------------------------------------------------


